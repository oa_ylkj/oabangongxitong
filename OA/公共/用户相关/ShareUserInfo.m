//
//  LoginUserInfo.m
//  投票
//
//  Created by yy on 17/3/3.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "ShareUserInfo.h"

@implementation ShareUserInfo
MJCodingImplementation //归档

+ (ShareUserInfo *)sharedUserInfo {
    static id instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}
+ (CLLocationManager *)shaCLLocationManager
{
    
    
    
    static CLLocationManager *instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        
        
        //定位管理器
        instance=[[CLLocationManager alloc]init];
        
        if (![CLLocationManager locationServicesEnabled]) {
            [MBProgressHUD showError:@"定位服务当前可能尚未打开，请设置打开"];
            return;
        }
        //如果没有授权则请求用户授权
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusNotDetermined){
            
            if(([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0))
            {
                [instance requestWhenInUseAuthorization];
            }
            
        }else if([CLLocationManager authorizationStatus]==
                 kCLAuthorizationStatusAuthorizedWhenInUse){
        }
        
        
        
        
        
    });
    
    return instance;
    
    
}
@end
