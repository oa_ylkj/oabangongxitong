//
//  LoginUserInfo.h
//  投票
//
//  Created by yy on 17/3/3.
//  Copyright © 2017年 yy. All rights reserved.
//
#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>
#import "UserModel.h"

@interface ShareUserInfo : NSObject
//用户
@property (nonatomic, strong) UserModel *userModel;//用户模型
@property (nonatomic, strong) NSString * token;//用户token

//部门相关
@property (nonatomic ,strong) NSMutableArray *lxrArr; // 写日志选择人

@property (nonatomic ,strong) NSMutableArray *csArr; // 抄送和审核人

@property (nonatomic, strong) dispatch_source_t timer; // 考勤
+ (CLLocationManager *)shaCLLocationManager;
+ (ShareUserInfo *)sharedUserInfo;

@end
