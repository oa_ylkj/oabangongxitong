//
//  UserModel.h
//  投票
//
//  Created by yy on 17/3/3.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface UserModel : NSObject


@property (nonatomic ,strong) NSString *id;
@property (nonatomic ,strong) NSString *userid;
@property (nonatomic ,strong) NSString *name;
@property (nonatomic ,strong) NSString *sex;
@property (nonatomic ,strong) NSString *tel;
@property (nonatomic ,strong) NSString *userface;
@property (nonatomic ,strong) NSString *status;
@property (nonatomic ,strong) NSString *department; // 部门
@property (nonatomic ,strong) NSString *positionName; // 职务
@property (nonatomic ,strong) NSString *myUnitName; // 公司
@property (nonatomic ,strong) NSString *rolenm; // 角色名称

@end
