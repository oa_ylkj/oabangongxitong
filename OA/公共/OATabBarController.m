//
//  OATabBarController.m
//  OA
//
//  Created by yy on 17/6/5.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "OATabBarController.h"
#import "MessageViewController.h"

@interface OATabBarController ()

@end

@implementation OATabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeUserInterface];
}
////公共
//#import "OATabBarController.h"
//#import "CompanyViewController.h"
//#import "AddressBookViewController.h"
//#import "MyViewController.h"
//
- (void)initializeUserInterface{
    
    MessageViewController * messageViewController = Y_storyBoard_id(@"MessageViewController");
//    CompanyViewController *CompanyViewController = Y_storyBoard_id(@"CompanyViewController");
    MoreViewController *CompanyViewController = VCInSB(@"MoreViewController",@"CompanyViewController");
    AddressBookViewController *addressBookViewController = Y_storyBoard_id(@"AddressBookViewController");
    MyViewController *myViewController = Y_storyBoard_id(@"MyViewController");
    
    NSArray * vcs = @[messageViewController,CompanyViewController,addressBookViewController,myViewController];
    NSArray * vcTitles  = @[@"消息",@"亚亮科技",@"通讯录",@"我的"];
    NSArray * vcNavTitles = @[@"消息",@"亚亮科技",@"通讯录",@"我的"];
    NSArray * imageNames = @[@"message-icon",@"menu-iconn",@"address-book",@"my"];
    NSArray * imageSelectedNames = @[@"click-message-icon",@"click-menu-cion",@"click-address-book-icon",@"click-my-icon"];
    NSMutableArray * navs = [@[]mutableCopy];
    
    //视图控制器的遍历
    [vcs enumerateObjectsUsingBlock:^(UIViewController *  _Nonnull vc, NSUInteger idx, BOOL * _Nonnull stop) {
        /* 统一配置视图控制器 */
        //设置视图控制器在导航栏上的标题
        vc.title = vcNavTitles[idx];
        //设置视图背景颜色(随机色)
        vc.view.backgroundColor = [UIColor getColorWithHexString:@"F9F9F9"];
        //设置视图控制器在标签栏上的按钮元素
        vc.tabBarItem = [[UITabBarItem alloc]initWithTitle:vcTitles[idx] image:[UIImage imageNamed:imageNames[idx]] selectedImage:[UIImage imageNamed:imageSelectedNames[idx]]];
        
        /* 统一配置导航控制器 */
        //创建导航控制器(这里相当于多个导航控制器,每个自己控制一个视图控制器)
        UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:vc];
        //配置导航控制器背景颜色
        nav.navigationBar.barTintColor= BlueColor;
        //配置导航控制器文字颜色
        nav.navigationBar.tintColor = [UIColor whiteColor];//
//        //配置导航控制器标题颜色
        nav.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:20]};
        
        //将导航控制器添加到数组
        [navs addObject:nav];
        
    }];
    // 配置标签控制器管理的视图控制器
    self.viewControllers = navs;//导航控制器关联上标签控制器
    self.selectedIndex = 0;//选定的序号为0即主页
//    self.tabBar.tintColor = [UIColor whiteColor];//点击后的tabBar文字颜色
    self.tabBar.barTintColor = [UIColor whiteColor];
    [[UINavigationBar appearance] setBackgroundColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setBackIndicatorImage:Y_IMAGE_ORIGINAL(@"返回icon")];
    [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:Y_IMAGE_ORIGINAL(@"返回icon")];
    [UIBarButtonItem.appearance setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -64) forBarMetrics:UIBarMetricsDefault];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
