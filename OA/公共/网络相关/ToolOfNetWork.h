//
//  ToolOfNetWork.h
//  投票
//
//  Created by yy on 17/3/3.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
typedef enum : NSUInteger {
    GET,
    POST
} requestMethod;

@interface ToolOfNetWork : AFHTTPSessionManager
+ (ToolOfNetWork *)sharedTools;

-(void)YrequestURL:(NSString *)url withParams:(NSMutableDictionary *)params finished:(void (^)(id responsObject,NSError *error))finished;

-(void)YrequestURL:(NSString *)url withParams:(NSMutableDictionary *)params fileData:(NSMutableArray *)fileArr finished:(void (^)(id responsObject,NSError *error))finished;
-(void)YrequestURL:(NSString *)url withParams:(NSMutableDictionary *)params fileData:(NSMutableArray *)fileArr  fileVoiceData:(NSMutableArray *)fileVoiceArr finished:(void (^)(id  responsObject,NSError *error))finished;

@end
