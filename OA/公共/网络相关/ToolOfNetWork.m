//
//  ToolOfNetWork.m
//  投票
//
//  Created by yy on 17/3/3.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "ToolOfNetWork.h"
static ToolOfNetWork * netWorkTools = nil;

@implementation ToolOfNetWork

+ (ToolOfNetWork *)sharedTools {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        netWorkTools = [[self alloc] initWithBaseURL:nil];
        netWorkTools.requestSerializer = [AFJSONRequestSerializer serializer];
        netWorkTools.responseSerializer = [AFJSONResponseSerializer serializer];
        netWorkTools.requestSerializer.timeoutInterval = 20;
        netWorkTools.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
        netWorkTools.requestSerializer = [AFHTTPRequestSerializer serializer];

    });
    return netWorkTools;
}

-(void)YrequestURL:(NSString *)url withParams:(NSMutableDictionary *)params finished:(void (^)(id responsObject,NSError *error))finished{
    if([ShareUserInfo sharedUserInfo].token!=nil){
        NSLog(@"当前用户token%@",[ShareUserInfo sharedUserInfo].token);
        [netWorkTools.requestSerializer setValue: [ShareUserInfo sharedUserInfo].token forHTTPHeaderField:@"tokenId"];
        
    }
    NSLog(@"%@________%@",params,Y_BASEURL(url));
     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),^{
     
          [self POST:Y_BASEURL(url) parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
               dispatch_async(dispatch_get_main_queue(), ^{
              NSLog(@"_______%@",[[responseObject description]kdtk_stringByReplaceingUnicode]);
              finished(responseObject,nil);
              });
          } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
               dispatch_async(dispatch_get_main_queue(), ^{
              NSLog(@"_______%@",[[error.localizedDescription description]kdtk_stringByReplaceingUnicode]);
              finished(nil,error);
              });
          }];
      });
    
}

-(void)YrequestURL:(NSString *)url withParams:(NSMutableDictionary *)params fileData:(NSMutableArray *)fileArr finished:(void (^)(id  responsObject,NSError *error))finished{
    if([ShareUserInfo sharedUserInfo].token!=nil){
        NSLog(@"当前用户token%@",[ShareUserInfo sharedUserInfo].token);
        [netWorkTools.requestSerializer setValue: [ShareUserInfo sharedUserInfo].token forHTTPHeaderField:@"tokenId"];
        
    }
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self POST:Y_BASEURL(url) parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            
            for (UIImage *photo in fileArr) {
                NSString *filename = [NSString stringWithFormat:@"%@.png", [ToolOfBasic nowTime]];
                NSData *data = UIImageJPEGRepresentation(photo, 0.2);
                [formData appendPartWithFileData:data name:@"file" fileName:filename mimeType:@"image/png"];
            }
            
        } progress:^(NSProgress * _Nonnull uploadProgress) {
            NSLog(@"%@",uploadProgress);
            NSLog(@"进度%lf",1.0 *uploadProgress.completedUnitCount / uploadProgress.totalUnitCount);
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"___success____%@",[[responseObject description]kdtk_stringByReplaceingUnicode]);
                finished(responseObject,nil);

            });
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"___failure____%@",[[error.localizedDescription description]kdtk_stringByReplaceingUnicode]);
                finished(nil,error);

            });
            
        }];
    });
    
    
}


#pragma mark ——————  Voice

-(void)YrequestURL:(NSString *)url withParams:(NSMutableDictionary *)params fileData:(NSMutableArray *)fileArr  fileVoiceData:(NSMutableArray *)fileVoiceArr finished:(void (^)(id  responsObject,NSError *error))finished{
    if([ShareUserInfo sharedUserInfo].token!=nil){
        NSLog(@"当前用户token%@",[ShareUserInfo sharedUserInfo].token);
        [netWorkTools.requestSerializer setValue: [ShareUserInfo sharedUserInfo].token forHTTPHeaderField:@"tokenId"];
        
    }
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self POST:Y_BASEURL(url) parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            
             for (NSString *str in fileVoiceArr) {
                 NSURL *u = [NSURL URLWithString:str];
                NSData *data = [NSData dataWithContentsOfURL:u];//dataWithContentsOfURL dataWithContentsOfFile
                [formData appendPartWithFileData:data name:@"list" fileName:[NSString stringWithFormat:@"sound.amr"] mimeType:@"audio/mp3/caf/amr"];
            }
//            for (NSData *data in fileVoiceArr) {
//                
//                
//                [formData appendPartWithFileData:data name:@"list" fileName:[NSString stringWithFormat:@"sound.amr"] mimeType:@"audio/mp3/caf/amr"];
//            }
            
            for (UIImage *photo in fileArr) {
                NSString *filename = [NSString stringWithFormat:@"%@.png", [ToolOfBasic nowTime]];
                NSData *data = UIImageJPEGRepresentation(photo, 0.2);
                [formData appendPartWithFileData:data name:@"file[%d]" fileName:filename mimeType:@"image/png"];
            }
            
        } progress:^(NSProgress * _Nonnull uploadProgress) {
            NSLog(@"%@",uploadProgress);
            NSLog(@"进度%lf",1.0 *uploadProgress.completedUnitCount / uploadProgress.totalUnitCount);
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"___success____%@",[[responseObject description]kdtk_stringByReplaceingUnicode]);
                finished(responseObject,nil);
                
            });
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"___failure____%@",[[error.localizedDescription description]kdtk_stringByReplaceingUnicode]);
                finished(nil,error);
                
            });
            
        }];
    });
    
    
}
@end
