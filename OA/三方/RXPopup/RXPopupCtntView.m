//
//  RXPopupCtntView.m
//  BMKTest
//
//  Created by iXcoder on 2016/10/19.
//  Copyright © 2016年 YIJI.COM. All rights reserved.
//

#import "RXPopupCtntView.h"

@interface RXPopupCtntView ()

@property (nonatomic, strong) UILabel *titleLbl;
@property (nonatomic, strong) UIView *headView;


@property (nonatomic, strong) UIButton *actBtn;


@end

@implementation RXPopupCtntView

- (void)dealloc
{
    NSLog(@"=== %s dealloc===", object_getClassName(self));
}

+ (instancetype)contentViewWithHeight:(CGFloat)height
{
    RXPopupCtntView *content = [[self alloc] initWithFrame:CGRectMake(0, 0, RX_SCREEN_SIZE.width - 40, height)];
    
    return content;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        self.clipsToBounds = YES;
        self.layer.cornerRadius = 4.f;
    }
    return self;
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    [super willMoveToSuperview:newSuperview];
    [self headView];
    [self actBtn];
}

- (void)onCancelAction:(id)sender
{
    if (self.cancel) {
        self.cancel();
    }
}

- (void)onConfirmAction:(id)sender
{
    if (self.confirm) {
        self.confirm(self.rtnValue);
    }
}

#pragma mark - getter & setter methods
- (UIView *)headView
{
    if (!_headView) {
        _headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 30)];
        _headView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        
        [_headView addSubview:self.titleLbl];
        
        UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        closeBtn.frame = CGRectMake(CGRectGetWidth(_headView.bounds) - 50, 0, 50, CGRectGetHeight(_headView.bounds));
        [closeBtn setImage:Y_IMAGE(@"关闭") forState:UIControlStateNormal];
        closeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        closeBtn.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        closeBtn.tintColor = [UIColor darkGrayColor];
        [closeBtn addTarget:self action:@selector(onCancelAction:) forControlEvents:UIControlEventTouchUpInside];
        [_headView addSubview:closeBtn];
        
        UIView *seperator = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(_headView.bounds) - 1, CGRectGetWidth(_headView.bounds), 0.5)];
        seperator.backgroundColor = [UIColor lightGrayColor];
        seperator.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [_headView addSubview:seperator];
        
        
        [self addSubview:_headView];
    }
    
    return _headView;
}

- (UILabel *)titleLbl
{
    if (!_titleLbl) {
        _titleLbl = [[UILabel alloc] initWithFrame:CGRectInset(self.headView.bounds, 12, 0)];
        _titleLbl.textAlignment = NSTextAlignmentCenter;
        _titleLbl.font = [UIFont systemFontOfSize:14];
        _titleLbl.textColor = [UIColor darkGrayColor];
        
        [self.headView addSubview:_titleLbl];
    }
    return _titleLbl;
}

- (UIButton *)actBtn
{
    if (!_actBtn) {
        _actBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        _actBtn.frame = CGRectMake(12, CGRectGetHeight(self.bounds) - 12 - 35, CGRectGetWidth(self.bounds) - 12 * 2, 35);
        _actBtn.backgroundColor = [UIColor whiteColor];
        _actBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_actBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_actBtn setTitle:@"确定" forState:UIControlStateNormal];
        [_actBtn setBackgroundImage:Y_IMAGE(@"圆角矩形-2") forState:UIControlStateNormal];
        _actBtn.layer.cornerRadius = 4.f;
        [_actBtn addTarget:self action:@selector(onConfirmAction:) forControlEvents:UIControlEventTouchUpInside];
        _actBtn.clipsToBounds = YES;
        
        [self addSubview:_actBtn];
    }
    return _actBtn;
}

- (void)setTitle:(NSString *)title
{
    
    self.titleLbl.text = title;
}

- (void)setActTitle:(NSString *)actTitle
{
    _actTitle = actTitle;
    [self.actBtn setTitle:_actTitle forState:UIControlStateNormal];
}

@end
