//
//  RXPopupGenderView.h
//  DeFeng
//
//  Created by iXcoder on 2016/10/19.
//  Copyright © 2016年 iXcoder. All rights reserved.
//

#import "RXPopupCtntView.h"

@interface RXPopupGenderView : RXPopupCtntView

+ (instancetype)genderView;

@end
