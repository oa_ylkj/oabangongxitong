//
//  RXPopupView.h
//  BMKTest
//
//  Created by iXcoder on 2016/10/19.
//  Copyright © 2016年 YIJI.COM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RXPopupCtntView.h"

@interface RXPopupView : UIView

+ (void)showContent:(RXPopupCtntView *)content completion:(void(^)())completion;

+ (void)hidePopup:(void(^)())completion;

@end
