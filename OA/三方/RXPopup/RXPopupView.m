//
//  RXPopupView.m
//  BMKTest
//
//  Created by iXcoder on 2016/10/19.
//  Copyright © 2016年 YIJI.COM. All rights reserved.
//

#import "RXPopupView.h"

@implementation RXPopupView

+ (instancetype)popupContainer
{
    static RXPopupView *container = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        container = [[self alloc] initWithFrame:CGRectMake(0, 0, RX_SCREEN_SIZE.width, RX_SCREEN_SIZE.height)];
    });
    return container;
}

+ (void)showContent:(RXPopupCtntView *)content completion:(void(^)())completion
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    
    RXPopupView *popup = [self popupContainer];
    NSArray *subs = popup.subviews;
    [subs makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [popup addSubview:content];
    content.center = popup.center;
    [window addSubview:popup];
    
    content.alpha = 0.0;
    content.transform = CGAffineTransformMakeScale(0.1, 0.1);
    
    [UIView animateWithDuration:0.25 animations:^{
        popup.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
        content.alpha = 1.0;
        content.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        if (completion) {
            completion();
        }
    }];
}

+ (void)hidePopup:(void(^)())completion
{
    RXPopupView *popup = [self popupContainer];
    NSArray *subviews = popup.subviews;
    [UIView animateWithDuration:0.25 animations:^{
        popup.backgroundColor = [UIColor clearColor];
        for (UIView *sub in subviews) {
            sub.transform = CGAffineTransformMakeScale(0.1, 0.1);
            sub.alpha = 0.0;
        }
    } completion:^(BOOL finished) {
        [subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [popup removeFromSuperview];
        if (completion) {
            completion();
        }
    }];
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    self.backgroundColor = [UIColor clearColor];
    [super willMoveToSuperview:newSuperview];
}


@end
