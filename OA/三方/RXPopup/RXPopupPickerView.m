//
//  RXPopupPickerView.m
//  BMKTest
//
//  Created by iXcoder on 2016/10/19.
//  Copyright © 2016年 YIJI.COM. All rights reserved.
//

#import "RXPopupPickerView.h"
//#import "DFAreaManager.h"

@interface RXPopupPickerView ()<UIPickerViewDataSource, UIPickerViewDelegate>
{
    
}

@property (nonatomic, strong) UIPickerView *picker;
@property (nonatomic, strong) UIView *seperator;

@property (nonatomic, readonly) NSArray *rows;

@end

@implementation RXPopupPickerView

+ (instancetype)pickerView
{
    RXPopupPickerView *picker = [RXPopupPickerView contentViewWithHeight:250];
    
    return picker;
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    [super willMoveToSuperview:newSuperview];
    if (newSuperview) {
        [self.picker reloadAllComponents];
        [self adjustSeperator];
    }
}

- (void)adjustSeperator
{
    NSArray *subviews = self.seperator.subviews;
    [subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    NSUInteger secCount = self.secCount();
    CGFloat width = (CGRectGetWidth(self.seperator.frame) - 12 * (secCount - 1))/secCount;
    for (int i = 0; i < secCount; i++) {
        UIView *sep = [[UIView alloc] initWithFrame:CGRectMake(i * (width + 12) , 0, width, CGRectGetHeight(self.seperator.frame))];
        sep.backgroundColor = [UIColor whiteColor];
        sep.opaque = YES;
        [self.seperator addSubview:sep];
    }
}

#pragma mark - UIPickerViewDataSource methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (self.secCount) {
        return self.secCount();
    }
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.rowCount(component);
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    ((UIView *)[pickerView.subviews objectAtIndex:1]).backgroundColor = [UIColor clearColor];
    ((UIView *)[pickerView.subviews objectAtIndex:2]).backgroundColor = [UIColor clearColor];
//    CBAutoScrollLabel *label = (CBAutoScrollLabel *)view;
//    if (!label) {
//        label = [[CBAutoScrollLabel alloc] init];
//        
//    }
//    label.labelSpacing = 30; // 开始和结束标签之间的距离
//    label.pauseInterval = 0; // 一秒的停顿之后再开始滚动
//    label.scrollSpeed = 30; // 每秒像素
//    label.textAlignment = NSTextAlignmentCenter; // 不使用自动滚动时的中心文本
//    label.fadeLength = 12.f;
//    label.backgroundColor = [UIColor yellowColor];
//    label.scrollDirection = CBAutoScrollDirectionLeft;
//    if (_mainW < 330) {
//        label.font = [UIFont systemFontOfSize:13];
//    } else {
//        label.font = [UIFont systemFontOfSize:15];
//    }
//    label.text = self.cfgCell(component, row);
//    NSLog(@"%f---%f",label.frame.size.width,label.frame.size.height);
    UILabel *label = (UILabel *)view;
    if (!label) {
        label = [[UILabel alloc] init];
        label.textColor = [UIColor darkGrayColor];
        label.font = [UIFont boldSystemFontOfSize:15];
        label.textAlignment = NSTextAlignmentCenter;
    }
    label.text = self.cfgCell(component, row);
    return label;
}

#pragma mark - UIPickerViewDelegate methods
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSUInteger secCount = 1;
    if (self.secCount) {
        secCount = self.secCount();
    }
    if (secCount < 2) {
        return ;
    }
    for (NSUInteger i = component + 1; i < secCount; i++) {
        [pickerView reloadComponent:i];
        [pickerView selectRow:0 inComponent:i animated:YES];
    }
    
    
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    NSUInteger secCount = 1;
    if (self.secCount) {
        secCount = self.secCount();
    }
    CGFloat width = (CGRectGetWidth(self.seperator.frame) - 12 * (secCount - 1))/secCount;
    return width;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 40;
}

#pragma mark - getter & setter methods
- (UIPickerView *)picker
{
    if (!_picker) {
        _picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.bounds) - 12 * 2, 170)];
        _picker.dataSource = self;
        _picker.delegate = self;
        _picker.center = CGPointMake(self.center.x, self.center.y - 8);
        [self addSubview:_picker];
    }
    
    return _picker;
}

- (UIView *)seperator
{
    if (!_seperator) {
        UIView *sep = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.bounds) - 12 * 2, 2)];
        sep.backgroundColor = [UIColor whiteColor];
        sep.opaque = YES;
        sep.center = CGPointMake(self.center.x, self.center.y + 12);
        [self addSubview:sep];
        _seperator = sep;
    }
    return _seperator;
}

- (id)rtnValue
{
    NSMutableArray *idxes = [NSMutableArray array];
    NSUInteger secCount = self.secCount();
    for (int i = 0; i < secCount; i++) {
        NSUInteger row = [self.picker selectedRowInComponent:i];
        [idxes addObject:@(row)];
    }
    return @{@"rows":idxes};
}

- (NSArray *)rows
{
    NSMutableArray *selectedRows = [NSMutableArray array];
    
    
    return [NSArray arrayWithArray:selectedRows];
}


@end


//@interface RXPopupAddrView ()
//{
//    
//}
//@property (nonatomic) NSInteger pvrow;
//@property (nonatomic) NSInteger ctrow;
//@property (nonatomic) NSInteger dsrow;
//@property (nonatomic, strong) NSArray *pvList;  // 省
//@property (nonatomic, strong) NSArray *ctList;  // 市
//@property (nonatomic, strong) NSArray *dsList;  // 区
//
//@end
//
//@implementation RXPopupAddrView
//
//+ (instancetype)addrView
//{
//    RXPopupAddrView *addrView = [RXPopupAddrView pickerView];
//    addrView.secCount = ^NSInteger{ return 3; };
//    __weak typeof(addrView) weak = addrView;
//    addrView.rowCount = ^NSInteger(NSInteger sec) {
//        if (sec == 0) {
//            return weak.pvList.count;
//        } else if (sec == 1) {
//            return weak.ctList.count;
//        } else if (sec == 2) {
//            return weak.dsList.count;
//        }
//        return 0;
//    };
//    addrView.cfgCell = ^NSString *(NSInteger sec, NSInteger row) {
//        switch (sec) {
//            case 0:
//                return weak.pvList[row][@"name"];
//            case 1:
//                return weak.ctList[row][@"name"];
//            case 2:
//                return weak.dsList[row][@"name"];
//            default:
//                return @"";
//        }
//    };
//    
//    return addrView;
//}
//
//- (instancetype)initWithFrame:(CGRect)frame
//{
//    if (self = [super initWithFrame:frame]) {
//        self.pvList = [DFAreaManager provinces];
//        NSNumber *pvid = self.pvList.firstObject[@"id"];
//        self.ctList = [DFAreaManager citiesInProvince:pvid];
//        NSNumber *ctid = self.ctList.firstObject[@"id"];
//        self.dsList = [DFAreaManager districtsInCity:ctid InProvince:pvid];
//    }
//    return self;
//}
//
//- (void)updatePickerDataSource
//{
//    NSNumber *pvid = self.pvList[self.pvrow][@"id"];
//    self.ctList = [DFAreaManager citiesInProvince:pvid];
//    NSNumber *ctid = self.ctList[self.ctrow][@"id"];
//    self.dsList = [DFAreaManager districtsInCity:ctid InProvince:pvid];
//}
//
//#pragma mark - getter & setter methods
//- (id)rtnValue
//{
//    return nil;
//}
//
//- (NSArray *)pvList
//{
//    if (!_pvList) {
//        _pvList = [DFAreaManager provinces];
//    }
//    return _pvList;
//}
//
//- (void)setPvrow:(NSInteger)pvrow
//{
//    if (_pvrow == pvrow) {
//        return;
//    }
//    _pvrow = pvrow;
//    self.ctrow = 0;
//}
//
//- (void)setCtrow:(NSInteger)ctrow
//{
//    if (_ctrow == ctrow) {
//        return;
//    }
//    _ctrow = ctrow;
//    self.dsrow = 0;
//}
//
//- (void)setDsrow:(NSInteger)dsrow
//{
//    if (_dsrow == dsrow) {
//        return ;
//    }
//    _dsrow = dsrow;
//    
//}
//
//@end
//
//
//@interface RXPopupDateView ()
//
//@property (nonatomic, strong) UIDatePicker *datePicker;
//
//@end
//
//@implementation RXPopupDateView
//
//+ (instancetype)dateView
//{
//    RXPopupDateView *dateView = [RXPopupDateView contentViewWithHeight:250];
//    
//    
//    return dateView;
//}
//
//- (void)willMoveToSuperview:(UIView *)newSuperview
//{
//    [super willMoveToSuperview:newSuperview];
//    self.datePicker.date = [NSDate date];
//}
//
//#pragma mark - getter & setter methods
//- (UIDatePicker *)datePicker
//{
//    if (!_datePicker) {
//        _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.bounds) - 30, 150)];
//        _datePicker.datePickerMode = UIDatePickerModeDate;
////        _datePicker.maximumDate = [NSDate date];
//        _datePicker.center = CGPointMake(self.center.x, self.center.y - 8);
//        NSLocale *locale = [[NSLocale alloc]initWithLocaleIdentifier:@"zh_CN"];
//        _datePicker.locale = locale;
//        [self addSubview:_datePicker];
//        
//        UIView *sep = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.bounds), 2)];
//        sep.backgroundColor = DF_BASE_COLOR;
//        sep.center = CGPointMake(self.center.x, self.center.y + 8);
//        [self addSubview:sep];
//    }
//    return _datePicker;
//}
//
//- (id)rtnValue
//{
//    NSDateFormatter *df = [[NSDateFormatter alloc] init];
//    df.dateFormat = @"yyyy-MM-dd";
//    NSDictionary *value = @{@"date": [df stringFromDate:self.datePicker.date]};
//    return value;
//}
//
//@end
