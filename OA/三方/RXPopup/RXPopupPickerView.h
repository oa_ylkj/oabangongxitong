//
//  RXPopupPickerView.h
//  BMKTest
//
//  Created by iXcoder on 2016/10/19.
//  Copyright © 2016年 YIJI.COM. All rights reserved.
//

#import "RXPopupCtntView.h"

@interface RXPopupPickerView : RXPopupCtntView

+ (instancetype)pickerView;

@property (nonatomic, copy) NSInteger(^secCount)(void);
@property (nonatomic, copy) NSInteger(^rowCount)(NSInteger sec);
@property (nonatomic, copy) NSString *(^cfgCell)(NSInteger sec, NSInteger row);

@end

@interface RXPopupAddrView : RXPopupPickerView

+ (instancetype)addrView;

@end


//@interface RXPopupDateView : RXPopupCtntView
//
//+ (instancetype)dateView;

//@end
