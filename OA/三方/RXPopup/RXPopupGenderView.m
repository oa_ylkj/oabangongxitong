//
//  RXPopupGenderView.m
//  DeFeng
//
//  Created by iXcoder on 2016/10/19.
//  Copyright © 2016年 iXcoder. All rights reserved.
//

#import "RXPopupGenderView.h"

@interface RXPopupGenderView ()

@property (nonatomic, strong) UIButton *maleBtn;
@property (nonatomic, strong) UIButton *femaleBtn;

@end

@implementation RXPopupGenderView

+ (instancetype)genderView
{
    RXPopupGenderView *genderView = [RXPopupGenderView contentViewWithHeight:180];
    
    return genderView;
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    [super willMoveToSuperview:newSuperview];
    [self maleBtn];
    [self femaleBtn];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        
    }
    return self;
}

- (UIButton *)sexBtnWithSex:(BOOL)isMale
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    NSString *title = isMale ? @"  一般" : @"  紧急";
    [button setTitle:title forState:UIControlStateNormal];
    [button setImage:Y_IMAGE(@"选择") forState:UIControlStateNormal];
    [button setImage:Y_IMAGE(@"选中") forState:UIControlStateSelected];
    [button addTarget:self action:@selector(onSexBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

- (void)onSexBtnAction:(UIButton *)sender
{
    self.maleBtn.selected = self.maleBtn == sender;
    self.femaleBtn.selected = self.femaleBtn == sender;
}

#pragma mark - getter methods
- (UIButton *)maleBtn
{
    if (!_maleBtn) {
        UIButton *mbtn = [self sexBtnWithSex:YES];
        mbtn.frame = CGRectMake(12, 38, 64, 35);
        [self addSubview:mbtn];
        mbtn.selected = YES;
        _maleBtn = mbtn;
    }
    return _maleBtn;
}

- (UIButton *)femaleBtn
{
    if (!_femaleBtn) {
        UIButton *fbtn = [self sexBtnWithSex:NO];
        fbtn.frame = CGRectMake(12, 75, 64, 35);
        [self addSubview:fbtn];
        fbtn.selected = NO;
        _femaleBtn = fbtn;
    }
    return _femaleBtn;
}

- (id)rtnValue
{
    BOOL male = self.maleBtn.selected;
    return @(male);
}

@end
