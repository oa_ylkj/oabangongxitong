//
//  RXPopupCtntView.h
//  BMKTest
//
//  Created by iXcoder on 2016/10/19.
//  Copyright © 2016年 YIJI.COM. All rights reserved.
//

#import <UIKit/UIKit.h>

#define RX_SCREEN_SIZE  [UIScreen mainScreen].bounds.size

@interface RXPopupCtntView : UIView

+ (instancetype)contentViewWithHeight:(CGFloat)height;
@property (nonatomic, strong) id rtnValue;
@property (nonatomic, copy) void(^cancel)();
@property (nonatomic, copy) void(^confirm)(id resp);

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *actTitle;

- (void)onCancelAction:(id)sender;
- (void)onConfirmAction:(id)sender;

@end
