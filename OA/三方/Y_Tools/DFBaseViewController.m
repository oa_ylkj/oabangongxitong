//
//  RXBaseViewController.m
//  DeFeng
//
//  Created by iXcoder on 16/9/13.
//  Copyright © 2016年 iXcoder. All rights reserved.
//

#import "DFBaseViewController.h"

@interface DFBaseViewController ()

@end

@implementation DFBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSArray *vcs = self.navigationController.viewControllers;
    if (vcs.count > 1) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"返回"] style:UIBarButtonItemStylePlain target:self action:@selector(onGoBack:)];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (void)onGoBack:(id)sender
{
    if (self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - MBProgressHUD
- (void)showWaitingHUD {
    _elfhud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _elfhud.removeFromSuperViewOnHide = YES;
    _elfhud.mode = MBProgressHUDModeIndeterminate;
    _elfhud.opacity = .5;
}

- (void)showWaitingHUDWithMessage:(NSString*)msg {
    _elfhud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _elfhud.mode = MBProgressHUDModeIndeterminate;
    _elfhud.removeFromSuperViewOnHide = YES;
    _elfhud.labelText = msg;
    _elfhud.opacity = .5;
}

- (void)showWaitingHUDWithMessageAutoDismiss:(NSString*)msg {
    _elfhud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _elfhud.mode = MBProgressHUDModeIndeterminate;
    _elfhud.removeFromSuperViewOnHide = YES;
    _elfhud.labelText = msg;
    _elfhud.opacity = .5;
    [_elfhud hide:YES afterDelay:1.0];
}

- (void)showTextHUDWithText:(NSString*)text forTime:(NSTimeInterval)duration {
    _elfhud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _elfhud.labelText = text;
    _elfhud.removeFromSuperViewOnHide = YES;
    _elfhud.mode = MBProgressHUDModeText;
    _elfhud.margin = 10.f;
    _elfhud.opacity = .5;
    NSTimeInterval time = duration > 0 ? duration : 1.0;
    [_elfhud hide:YES afterDelay:time];
}

- (void)showDetailsTextHUDWithText:(NSString*)text forTime:(NSTimeInterval)duration {
    _elfhud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _elfhud.detailsLabelText = text;
    _elfhud.removeFromSuperViewOnHide = YES;
    _elfhud.mode = MBProgressHUDModeText;
    _elfhud.margin = 10.f;
    _elfhud.opacity = .5;
    _elfhud.detailsLabelFont = [UIFont systemFontOfSize:15.f];
    NSTimeInterval time = duration > 0 ? duration : 1.0;
    [_elfhud hide:YES afterDelay:time];
}

- (void)showSuccessHUD:(NSString*)sucMsg {
    if (_elfhud) {
        [_elfhud removeFromSuperview];
        _elfhud = nil;
    }
    _elfhud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _elfhud.removeFromSuperViewOnHide = YES;
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"back"]];//
    _elfhud.customView = imgView;
    imgView = nil;
    _elfhud.opacity = .5;
    _elfhud.mode = MBProgressHUDModeCustomView;
    if (sucMsg.length > 0) {
        _elfhud.labelText = sucMsg;
    }
    [_elfhud hide:YES afterDelay:1.0];
}

- (void)showFailedHUD:(NSString*)failedMsg {
    if (_elfhud) {
        [_elfhud removeFromSuperview];
        _elfhud = nil;
    }
    _elfhud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _elfhud.removeFromSuperViewOnHide = YES;
    _elfhud.opacity = 1;
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_pig"]];//
    _elfhud.customView = imgView;
    imgView = nil;
    _elfhud.mode = MBProgressHUDModeCustomView;
    if (failedMsg.length > 0) {
        _elfhud.labelText = failedMsg;
    }
    
    [_elfhud hide:YES afterDelay:1.0];
}

- (void)showProgressHUDWithType:(MBProgressHUDMode)type {
    _elfhud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    _elfhud.mode = MBProgressHUDModeAnnularDeterminate;
    _elfhud.mode = type == MBProgressHUDModeDeterminateHorizontalBar ? MBProgressHUDModeDeterminateHorizontalBar : MBProgressHUDModeAnnularDeterminate;
    _elfhud.opacity = .5;
    
    [_elfhud showAnimated:YES whileExecutingBlock:^{
        float progress = 0.0f;
        while (progress < 1.0f) {
            progress += 0.01f;
            _elfhud.progress = progress;
            usleep(90000);
        }
    }];
}

- (void)dismissHUD {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}


@end
