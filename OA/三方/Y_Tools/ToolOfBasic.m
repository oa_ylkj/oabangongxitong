//
//  ToolOfBasic.m
//  shusheng
//
//  Created by rimi on 16/7/9.
//  Copyright © 2016年 yuying. All rights reserved.
//

#import "ToolOfBasic.h"
#import "GTMBase64.h" //base64加密
@implementation ToolOfBasic
#pragma mark —————— 时间
+(NSString *)nowTime{
    NSDate * senddate=[NSDate date];
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"YYYY-MM-dd"];
    NSString * morelocationString=[dateformatter stringFromDate:senddate];
//    NSLog(@"%@",morelocationString);
    
    return morelocationString;
}

+(NSDate *)nowTimeOfDate{
    NSDate * senddate=[NSDate date];
    NSLog(@"%@",senddate);
//    NSDateFormatter *dateformatter=[[NSDateFormatter alloc] init];
//    [dateformatter setDateFormat:@"YYYY-MM-dd~HH:mm:ss"];
//    NSString * morelocationString=[dateformatter stringFromDate:senddate];
//    NSLog(@"%@",morelocationString);
    
    return senddate;
}

+(NSString *)nowTimeOfLong{
    NSDate * senddate=[NSDate date];
    NSLog(@"%@",senddate);
        NSDateFormatter *dateformatter=[[NSDateFormatter alloc] init];
//        [dateformatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
        dateformatter.dateFormat = @"YYYY-MM-dd HH:mm";
        NSString * morelocationString=[dateformatter stringFromDate:senddate];
        NSLog(@"%@",morelocationString);
    
    return morelocationString;
}

#pragma mark —————— 上月下月

+ (NSDate *)dayInThePreviousMonth:(NSDate*)date
{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    dateComponents.month = -1;
    return [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:date options:0];
}

+ (NSDate *)dayInTheFollowingMonth:(NSDate*)date
{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    dateComponents.month = 1;
    return [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:date options:0];
}

#pragma mark —————— 上下月str
//上月
+ (NSString *)dayInThePreviousMonthStr:(NSString *)dateS
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM"];
    NSDate *oldDate = [formatter dateFromString:dateS];

    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *lastMonthComps = [[NSDateComponents alloc] init];
    //    [lastMonthComps setYear:1]; // year = 1表示1年后的时间 year = -1为1年前的日期，month day 类推
    [lastMonthComps setMonth:-1];
    NSDate *newdate = [calendar dateByAddingComponents:lastMonthComps toDate:oldDate options:0];
    NSString *newDateStr = [formatter stringFromDate:newdate];
    return newDateStr;
}

//下月
+ (NSString *)dayInTheFollowingMonthStr:(NSString*)dateS
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM"];
    NSDate *oldDate = [formatter dateFromString:dateS];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *lastMonthComps = [[NSDateComponents alloc] init];
    //    [lastMonthComps setYear:1]; // year = 1表示1年后的时间 year = -1为1年前的日期，month day 类推
    [lastMonthComps setMonth:1];
    NSDate *newdate = [calendar dateByAddingComponents:lastMonthComps toDate:oldDate options:0];
    NSString *newDateStr = [formatter stringFromDate:newdate];
    return newDateStr;
}
#pragma mark —————— 上下几天
+ (NSDate *)dayInThePreviousDayNum:(NSInteger)dayNum  beginDate:(NSDate*)date
{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    dateComponents.day = -dayNum;
    return [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:date options:0];
}

+ (NSDate *)dayInTheFollowingDayNum:(NSInteger)dayNum  beginDate:(NSDate*)date
{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    dateComponents.day = dayNum;
    return [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:date options:0];
}

#pragma mark —————— 秒转小时分钟
+ (NSString *)timeFormatted:(int)totalSeconds
{
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
//    return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
    if (hours==0&&minutes==0) {
        return @"00:01";
    }else{
        return [NSString stringWithFormat:@"%02d:%02d",hours, minutes];
    }

}


#pragma mark —————— NSString 转换为 NSDate
+(NSDate *)strLongBecomeDate:(NSString *)longDateStr{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString:longDateStr];
    return date;
}

+(NSDate *)strShortBecomeDate:(NSString *)shortDateStr{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:shortDateStr];
    return date;
}

#pragma mark —————— NSDate 转换为 NSString
+ (NSString *)dateBecomeLongStr:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *strDate = [dateFormatter stringFromDate:date];
    
    return strDate;
}
+ (NSString *)dateBecomeShortStr:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *strDate = [dateFormatter stringFromDate:date];
    return strDate;
}


#pragma mark —————— 前后时间串进行比较
+ (int)compareOneDay:(NSString *)oneDayStr withAnotherDay:(NSString *)anotherDayStr
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    
    NSDate *dateA = [dateFormatter dateFromString:oneDayStr];
    NSDate *dateB = [dateFormatter dateFromString:anotherDayStr];
    NSComparisonResult result = [dateA compare:dateB];
    NSLog(@"date1 : %@, date2 : %@", oneDayStr, anotherDayStr);
    if (result == NSOrderedDescending) {
        return 1;
    }
    else if (result == NSOrderedAscending){
        //NSLog(@"Date1 is in the past");
        return -1;
    }
    //NSLog(@"Both dates are the same");
    return 0;
    
}

#pragma mark —————— 表情过滤
+(BOOL)stringContainsEmoji:(NSString *)string
{
    __block BOOL returnValue = NO;
    
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length])
                               options:NSStringEnumerationByComposedCharacterSequences
                            usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                                const unichar high = [substring characterAtIndex: 0];
                                
                                // Surrogate pair (U+1D000-1F9FF)
                                if (0xD800 <= high && high <= 0xDBFF) {
                                    const unichar low = [substring characterAtIndex: 1];
                                    const int codepoint = ((high - 0xD800) * 0x400) + (low - 0xDC00) + 0x10000;
                                    
                                    if (0x1D000 <= codepoint && codepoint <= 0x1F9FF){
                                        returnValue = YES;
                                    }
                                    
                                    // Not surrogate pair (U+2100-27BF)
                                } else {
                                    if (0x2100 <= high && high <= 0x27BF){
                                        returnValue = YES;
                                    }
                                }
                            }];
    
    return returnValue;
}

#pragma mark —————— 加密解密

/***
 将字符串Base64加密
 @param input 需要加密的字符串
 */
+ (NSString*)encodeBase64String:(NSString*)input{
    
    NSData *data = [input dataUsingEncoding:NSUTF8StringEncoding];
    data = [GTMBase64 encodeData:data];
    NSString *base64String = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    return base64String;
}


/***
 解密Base64字符串
 @para m input 需要解密的字符串
 */
+ (NSString*)decodeBase64String:(NSString*)input{
    
    NSData *data = [input dataUsingEncoding:NSUTF8StringEncoding];
    data = [GTMBase64 decodeData:data];
    NSString *base64String = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    return  base64String;
}


/***
 将 data Bese64加密
 @para m  data 需要加密的data数据
 */
+ (NSString*)encodeBase64Data:(NSData*)data{
    
    
    data = [GTMBase64 encodeData:data];
    NSString *base64String = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    return base64String;
}


/***
 解密Base64 data
 @para m 要解密的data数据
 */
+ (NSString*)decodeBase64Data:(NSData*)data{
    
    
    data = [GTMBase64 decodeData:data];
    NSString *base64String = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    return base64String;
    
    
    
}

+ (CLLocation *)AMapLocationFromBaiduLocation:(CLLocation *)BaiduLocation{
    const double x_pi = M_PI * 3000.0 / 180.0;
    double x = BaiduLocation.coordinate.longitude - 0.0065, y = BaiduLocation.coordinate.latitude - 0.006;
    double z = sqrt(x * x + y * y) - 0.00002 * sin(y * x_pi);
    double theta = atan2(y, x) - 0.000003 * cos(x * x_pi);
    double AMapLongitude = z * cos(theta);
    double AMapLatitude = z * sin(theta);
    CLLocation *AMapLocation = [[CLLocation alloc] initWithLatitude:AMapLatitude longitude:AMapLongitude];
    return AMapLocation;
}


@end
