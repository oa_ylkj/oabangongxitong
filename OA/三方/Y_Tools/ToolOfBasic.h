//
//  ToolOfBasic.h
//  shusheng
//
//  Created by rimi on 16/7/9.
//  Copyright © 2016年 yuying. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ToolOfBasic : NSObject

//时间
+(NSString *)nowTime;
+(NSDate *)nowTimeOfDate;
+(NSString *)nowTimeOfLong;

//上月下月
+ (NSDate *)dayInThePreviousMonth:(NSDate*)date;
+ (NSDate *)dayInTheFollowingMonth:(NSDate*)date;

+ (NSString *)dayInTheFollowingMonthStr:(NSString*)dateS;
+ (NSString *)dayInThePreviousMonthStr:(NSString *)dateS;

#pragma mark —————— 上下几天
+ (NSDate *)dayInThePreviousDayNum:(NSInteger)dayNum  beginDate:(NSDate*)date;
+ (NSDate *)dayInTheFollowingDayNum:(NSInteger)dayNum  beginDate:(NSDate*)date;

//秒转小时分钟
+ (NSString *)timeFormatted:(int)totalSeconds;//

//nsstring与date互转
#pragma mark —————— NSString 转换为 NSDate
+(NSDate *)strLongBecomeDate:(NSString *)longDateStr;
+(NSDate *)strShortBecomeDate:(NSString *)shortDateStr;
#pragma mark —————— NSDate 转换为 NSString
+ (NSString *)dateBecomeLongStr:(NSDate *)date;
+ (NSString *)dateBecomeShortStr:(NSDate *)date;

#pragma mark —————— 前后时间串进行比较
+ (int)compareOneDay:(NSString *)oneDayStr withAnotherDay:(NSString *)anotherDayStr;

//表情过滤
+(BOOL)stringContainsEmoji:(NSString *)string;

//加密解密
+ (NSString*)encodeBase64String:(NSString*)input;
+ (NSString*)decodeBase64String:(NSString*)input;
+ (NSString*)encodeBase64Data:(NSData*)data;
+ (NSString*)decodeBase64Data:(NSData*)data;

+ (CLLocation *)AMapLocationFromBaiduLocation:(CLLocation *)BaiduLocation;
@end
