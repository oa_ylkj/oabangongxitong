//
//  RXBaseViewController.h
//  DeFeng
//
//  Created by iXcoder on 16/9/13.
//  Copyright © 2016年 iXcoder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DFBaseViewController : UIViewController
@property (nonatomic, readonly) MBProgressHUD *elfhud;


- (void)onGoBack:(id)sender;


#pragma mark - MBProgressHUD
- (void)showWaitingHUD;
- (void)showWaitingHUDWithMessage:(NSString*)msg;
- (void)showWaitingHUDWithMessageAutoDismiss:(NSString*)msg;

- (void)showTextHUDWithText:(NSString*)text forTime:(NSTimeInterval)duration;
- (void)showDetailsTextHUDWithText:(NSString*)text forTime:(NSTimeInterval)duration;

- (void)showSuccessHUD:(NSString*)sucMsg;
- (void)showFailedHUD:(NSString*)failedMsg;

- (void)showProgressHUDWithType:(MBProgressHUDMode)type;

- (void)dismissHUD;

@end
