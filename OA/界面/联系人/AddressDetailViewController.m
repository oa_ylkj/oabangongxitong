//
//  AddressDetailViewController.m
//  OA
//
//  Created by yy on 17/7/7.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "AddressDetailViewController.h"
#import "MobileManModel.h"
@interface AddressDetailViewController ()
@property (weak, nonatomic) IBOutlet UIButton *sendSmsBtn;
@property (weak, nonatomic) IBOutlet UIButton *callPhoneBtn;
@property (nonatomic,copy)NSString *phone;
@property (weak, nonatomic) IBOutlet UILabel *nameL;
@property (weak, nonatomic) IBOutlet UILabel *phoneL;
@property (weak, nonatomic) IBOutlet UILabel *nameImgL;
@end

@implementation AddressDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeUserInterface];
    [self initOfDataSouce];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)sendSmsBtnAction:(UIButton *)sender {
    [self sendSmsAction:_phone];
}
- (void)initOfDataSouce{
    MobileManModel *model = [MobileManModel objectWithKeyValues:_infomationDic error:nil];
  
    _nameImgL.text = model.name.length>2?[model.name substringWithRange:NSMakeRange(model.name.length-2, 2)]:model.name;
    _nameL.text = model.name;
    _phoneL.text = model.family_phone1;
    _phone = model.family_phone1;
    
//    _phone = _infomationDic[@"phone"];
//    NSMutableDictionary *parm = [NSMutableDictionary dictionaryWithObject:_idStr forKey:@"id"];
//    [[ToolOfNetWork sharedTools]YrequestURL:oa_getadressDetail withParams:parm finished:^(id responsObject, NSError *error) {
//        if (_Success) {
//             NSDictionary *dic = responsObject[@"data"];
//            Y_MBP_Success
//        }else{
//            Y_MBP_Error
//        }
//    }];
}
- (void)initializeUserInterface{
    _sendSmsBtn.imageEdgeInsets = UIEdgeInsetsMake(-_sendSmsBtn.titleLabel.intrinsicContentSize.height, 0, 0, -_sendSmsBtn.titleLabel.intrinsicContentSize.width);
    _sendSmsBtn.titleEdgeInsets = UIEdgeInsetsMake(_sendSmsBtn.currentImage.size.height, -_sendSmsBtn.currentImage.size.width, 0, 0);
    
    _callPhoneBtn.imageEdgeInsets = UIEdgeInsetsMake(-_callPhoneBtn.titleLabel.intrinsicContentSize.height, 0, 0, -_callPhoneBtn.titleLabel.intrinsicContentSize.width);
    _callPhoneBtn.titleEdgeInsets = UIEdgeInsetsMake(_callPhoneBtn.currentImage.size.height, -_callPhoneBtn.currentImage.size.width, 0, 0);
    
}

- (IBAction)callPhoneAction:(UIButton *)sender {
    [self phoneAction:_phone];
    
}
#pragma mark ——————
- (void)sendSmsAction:(NSString *)phone{
    if (phone.length == 0) {
        [MBProgressHUD showError:@"电话为空！"];
    }
    
    NSString *phoneNumber = phone;
    NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"sms://%@", phoneNumber]];
    //         NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", @"10086"]];
    
    UIWebView *mCallWebview = [[UIWebView alloc] init];
    [self.view addSubview:mCallWebview];
    [mCallWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
    
    
}
- (void)phoneAction:(NSString *)phone{
    if (phone.length == 0) {
        [MBProgressHUD showError:@"电话为空！"];
        return;
    }
    
    NSString *phoneNumber = phone;
    NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", phoneNumber]];
    //     NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", @"10086"]];
    
    UIWebView *mCallWebview = [[UIWebView alloc] init] ;
    [self.view addSubview:mCallWebview];
    [mCallWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
    
    
}
@end
