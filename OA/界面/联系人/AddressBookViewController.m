//
//  AddressBookViewController.m
//  OA
//
//  Created by yy on 17/6/5.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "AddressBookViewController.h"
#import "AddressBookTableViewCell.h"
#import "MobileAddressBookViewController.h"
#import "AddressBookListHeaderView.h"
#import "MobileManModel.h"
@interface AddressBookViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong)NSMutableArray *arrOfCompany;
@property (nonatomic,assign)BOOL isExpansion;
@property (nonatomic,copy) NSString *unit;
@end

@implementation AddressBookViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Y_tableViewMj_header([weakSelf initOfDataSouce];)
    Y_tableViewMj_footer([weakSelf initOfDataSouce]; )
    [self initOfDataSouce];
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)initOfDataSouce{
    _arrOfCompany = [NSMutableArray array];
    _isExpansion = YES;
//    NSMutableDictionary *parm = [NSMutableDictionary dictionaryWithObject:@"00000001" forKey:@"myUnitId"];
    NSMutableDictionary *parm = [NSMutableDictionary dictionary];
    [[ToolOfNetWork sharedTools]YrequestURL:oa_getAddresslist withParams:parm finished:^(id responsObject, NSError *error) {
        Y_headerEndRefreshing
        Y_footerEndRefreshing
        if (_Success) {
//            NSDictionary *dic = [NSDictionary dictionaryWithDictionary:responsObject[@"data"]];
//            _arrOfCompany = [NSMutableArray arrayWithArray:[dic objectForKey:@"list"]];
//            _unit = [dic objectForKey:@"unit"];
//            _tableView.mj_footer.hidden = (_arrOfCompany.count == 0);
            
            NSMutableArray *arr = [NSMutableArray arrayWithArray:responsObject[@"data"]];
            _arrOfCompany = arr;
            if (arr.count>0) {
                 _unit = [arr[0]  objectForKey:@"unit"];
            }
             [self initializeUserInterface];
            [_tableView reloadData];
        }else{
            Y_MBP_Error
        }
         _tableView.mj_footer.hidden = YES;
    }];
    
}
 

- (void)initializeUserInterface{
    //header
    UIView *tableViewHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 8, Y_mainW, 70)];
    AddressBookListHeaderView *headerView = [self headerViewIsInternalTableViewSectionisZero:2];
    headerView.frame = CGRectMake(0, 0, Y_mainW, 70);
    [tableViewHeaderView addSubview:headerView];
    _tableView.tableHeaderView = tableViewHeaderView;
    _tableView.tableHeaderView.frame = CGRectMake(0, 8, Y_mainW, 70);
    
    //footer
    UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 8, Y_mainW, 100)];
    AddressBookListHeaderView *phoneFooterView = [self headerViewIsInternalTableViewSectionisZero:1];
    phoneFooterView.frame = CGRectMake(0, 8, Y_mainW, 70);
    
    [footerView addSubview:phoneFooterView];
    _tableView.tableFooterView = footerView;
    
    _tableView.delegate  = self;
}

#pragma mark ——————

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0 && _isExpansion == YES) {
        return _arrOfCompany.count;
    }else{
        return 0;
    }
    
}
#pragma mark 分区的头视图
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 70;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (self.tableView != tableView) {
        return nil;
    }
    
    
    return [self headerViewIsInternalTableViewSectionisZero:0];
}

- (AddressBookListHeaderView *)headerViewIsInternalTableViewSectionisZero:(NSInteger)section{
    
    AddressBookListHeaderView *view = [[[NSBundle mainBundle]loadNibNamed:@"AddressBookListHeaderView" owner:self options:nil]objectAtIndex:0];
    view.frame = CGRectMake(0, 0, Y_mainW, 70);
    for (id obj in view.subviews) {
        if ([obj isKindOfClass:[UIImageView class]]) {
            UIImageView *img = obj;
            if (img.tag == 340) {
                //titleImg
                if (section!=0 &&section!=1) {
                    img.image = Y_IMAGE(@"公司LOGO0");
                }else{
                    img.image = nil;
                }
            }else{
                img.contentMode = UIViewContentModeScaleAspectFit;
                if (section == 0) {
                    //内部联系人
                    img.image = Y_IMAGE(@"向下箭头");
                }else{
                    if (section == 1) {
                        //外部联系人
                        img.image = Y_IMAGE(@"箭头");
                    }else{
                        img.hidden = YES;
                    }
                    
                }
            }
        }
        if ([obj isKindOfClass:[UILabel class]]) {
            UILabel *lab = obj;
            if (section==0) {
                lab.text = @"内部联系人";
            }else{
                if (section == 1) {
                    lab.text = @"外部联系人";
                }else{
//                    lab.text = @"公司名";
                    lab.text =  _unit;
                }
                
            }
        }
        
        if ([obj isKindOfClass:[UIButton class]]) {
            UIButton *btn = obj;
            btn.tag = TAG_BTN_B+section;
            [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    return view;
}
- (void)btnClick:(UIButton *)button {
    if (button.tag - TAG_BTN_B != 2) {
        if (button.tag - TAG_BTN_B == 1) {
            NSLog(@"跳转");
            MobileAddressBookViewController *mobileBookVc = VCInSB(@"MobileAddressBookViewController", @"AddressBookViewController");
            [self.navigationController pushViewController:mobileBookVc animated:YES];
        }else{
            
            NSLog(@"点击");
            _isExpansion = !_isExpansion;
            //只刷新指定分区
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AddressBookTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddressBookTableViewCell"];
    if (!cell) {
        cell = [[AddressBookTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AddressBookTableViewCell"];
    }
    NSString *nameStr = [_arrOfCompany[indexPath.row] objectForKey:@"name"];
    
    cell.nameL.text = nameStr.length>2?[nameStr substringWithRange:NSMakeRange(nameStr.length-2, 2)]:nameStr;
    cell.titleL.text = nameStr;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
      NSLog(@"详情");
    AddressDetailViewController *detailVc = VCInSB(@"AddressDetailViewController", @"AddressBookViewController");
//    detailVc.idStr = [_arrOfCompany[indexPath.row] objectForKey:@"id"];
    detailVc.infomationDic = [NSMutableDictionary dictionaryWithDictionary:_arrOfCompany[indexPath.row]];
    [self.navigationController pushViewController:detailVc animated:YES];
    
}

@end
