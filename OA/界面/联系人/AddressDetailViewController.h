//
//  AddressDetailViewController.h
//  OA
//
//  Created by yy on 17/7/7.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressDetailViewController : UIViewController
@property (nonatomic,strong)NSMutableDictionary *infomationDic;
@property (nonatomic,copy)NSString *idStr;
@end
