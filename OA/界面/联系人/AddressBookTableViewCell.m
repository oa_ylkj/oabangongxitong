//
//  AddressBookTableViewCell.m
//  OA
//
//  Created by yy on 17/6/12.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "AddressBookTableViewCell.h"

@implementation AddressBookTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _img.layer.cornerRadius = 20;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
