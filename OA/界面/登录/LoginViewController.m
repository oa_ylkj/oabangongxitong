//
//  LoginViewController.m
//  OA
//
//  Created by yy on 17/6/5.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "LoginViewController.h"
#import "UserModel.h"
#import "JPUSHService.h"
#define AccountKey @"account"
#define PwdKey @"pwd"

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwoedTextfield;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeUserInterface];
    [self initOfDataSouce];
    
}
- (void)initializeUserInterface{
    
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
//    self.navigationController.navigationBarHidden = YES;
}
- (void)initOfDataSouce{
    _phoneTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:AccountKey];
    _passwoedTextfield.text = [[NSUserDefaults standardUserDefaults]objectForKey:PwdKey];
    [self loginActionOfNet];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = YES;
    
    
}
- (IBAction)loginAction:(UIButton *)sender {
     NSLog(@"登陆");
//     self.view.window.rootViewController = [[OATabBarController alloc] init];
    [self loginActionOfNet];

}
- (void)loginActionOfNet{

    NSString *phoneStr = [_phoneTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *passwordStr = [_passwoedTextfield.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (phoneStr.length == 0) {
        [MBProgressHUD showError:@"请输入手机号！"];
        return;
    }
    if (passwordStr.length == 0) {
        [MBProgressHUD showError:@"请输入密码！"];
        return;
    }
    //加密
    NSString *base64Password =[ToolOfBasic encodeBase64String:passwordStr];
    NSString *baseLoginStr = [ToolOfBasic encodeBase64String:[NSString stringWithFormat:@"%@|%@",phoneStr,base64Password]];
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:baseLoginStr,@"sign", nil];
    [MBProgressHUD showMessage:@"登录中"];
    Y_Hide
    [[ToolOfNetWork sharedTools] YrequestURL:oa_login withParams:dic finished:^(id responsObject, NSError *error) {
        [MBProgressHUD hideHUD];
        if (_Success) {
            [ShareUserInfo sharedUserInfo].token = responsObject[@"data"];
            
        
            [[ToolOfNetWork sharedTools] YrequestURL:oa_getUserInfo withParams:@{}.mutableCopy finished:^(id responsObject, NSError *error) {
                
                if (_Success) {
                    [ShareUserInfo sharedUserInfo].userModel = [UserModel objectWithKeyValues:responsObject[@"data"]];
                    self.view.window.rootViewController = [[OATabBarController alloc] init];
                }else {
                    [MBProgressHUD showError:responsObject[@"message"]];
                }
                
            }];
            
            //成功
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:phoneStr forKey:AccountKey];
            [defaults setObject:passwordStr forKey:PwdKey];
            //注册极光
//            [JPUSHService setAlias:phoneStr completion:nil seq:0];
            [JPUSHService setAlias:phoneStr completion:^(NSInteger iResCode, NSString *iAlias, NSInteger seq) {
//                 NSLog(@"666");
//                 NSLog(@"iResCode=%ld iAlias=%@ seq=%ld ",(long)iResCode,iAlias,(long)seq);
            } seq:0];
//            [JPUSHService setAlias:phoneStr callbackSelector:nil object:self];
        } else {
            [MBProgressHUD showError:responsObject[@"message"]];
        }
        NSLog(@"%@",[[responsObject description]kdtk_stringByReplaceingUnicode]);
        NSLog(@"%@",error.localizedDescription);
    }];
    
}




- (IBAction)registereAction:(UIButton *)sender {
     NSLog(@"注册");
    RegisteredViewController *registeredVc = Y_storyBoard_id(@"RegisteredViewController");
     self.navigationController.navigationBar.hidden = NO;
    [self.navigationController pushViewController:registeredVc animated:YES];
    
}
- (IBAction)missPassWordAction:(UIButton *)sender {
    MissPassGetCodeViewController *getCodeVc = VCInSB(@"MissPassGetCodeViewController", @"RegisteredViewController");
    self.navigationController.navigationBar.hidden = NO;
    [self.navigationController pushViewController:getCodeVc animated:YES];
    
}
#pragma mark —————— object转json字符串
-(NSString*)DataTOjsonString:(id)object
{
    NSString *jsonString = nil;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:object
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return jsonString;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
