//
//  AboutVC.m
//  OA
//
//  Created by Admin on 2017/8/3.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "AboutVC.h"

@interface AboutVC ()
@property (weak, nonatomic) IBOutlet UILabel *banben;

@end

@implementation AboutVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"关于我们";
    self.banben.text = [NSString stringWithFormat:@"亚亮OA V%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
}



@end
