//
//  AgreementVC.m
//  OA
//
//  Created by Admin on 2017/8/3.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "AgreementVC.h"

@interface AgreementVC ()
@property (weak, nonatomic) IBOutlet UITextView *textView;
//@property (nonatomic ,strong) UITextView *web;
@end

@implementation AgreementVC

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.web = [[UITextView alloc]initWithFrame:self.view.frame];
    self.title = @"用户协议";
    self.automaticallyAdjustsScrollViewInsets = NO;
    NSDictionary *dic = @{};
    Y_Wait
    [[ToolOfNetWork sharedTools] YrequestURL:oa_getUserAgreement withParams:dic.mutableCopy finished:^(id responsObject, NSError *error) {
        Y_Hide
        if (_Success) {
            self.textView.text = responsObject[@"data"][@"agreement"];
//            [self.view addSubview: self.web];
        } else {
            [MBProgressHUD showError:responsObject[@"message"]];
        }
        NSLog(@"%@",[[responsObject description]kdtk_stringByReplaceingUnicode]);
        NSLog(@"%@",error.localizedDescription);
    }];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
