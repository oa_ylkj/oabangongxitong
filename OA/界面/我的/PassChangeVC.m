//
//  PassChangeVC.m
//  投票
//
//  Created by yy on 17/5/4.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "PassChangeVC.h"

@interface PassChangeVC ()
@property (weak, nonatomic) IBOutlet UITextField *oldPass;
@property (weak, nonatomic) IBOutlet UITextField *NewPass;
@property (weak, nonatomic) IBOutlet UITextField *sureNewPass;

@end

@implementation PassChangeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"密码管理";
    self.oldPass.borderStyle = UITextBorderStyleLine;
    self.oldPass.layer.borderWidth = 1;
    self.oldPass.layer.borderColor = [UIColor whiteColor].CGColor;
    self.NewPass.borderStyle = UITextBorderStyleLine;
    self.NewPass.layer.borderWidth = 1;
    self.NewPass.layer.borderColor = [UIColor whiteColor].CGColor;
    self.sureNewPass.borderStyle = UITextBorderStyleLine;
    self.sureNewPass.layer.borderWidth = 1;
    self.sureNewPass.layer.borderColor = [UIColor whiteColor].CGColor;
    // Do any additional setup after loading the view.
}
- (IBAction)savePassword:(id)sender {
    // V_updatePassword
    if (self.oldPass.text.length == 0) {
        [MBProgressHUD showError:@"请输入旧密码"];
        return;
    }
    if (self.NewPass.text.length == 0) {
        [MBProgressHUD showError:@"请输入新密码"];
        return;
    }
    if (self.sureNewPass.text.length == 0) {
        [MBProgressHUD showError:@"请确认新密码"];
        return;
    }
    if (![self.NewPass.text isEqualToString:self.sureNewPass.text]) {
        [MBProgressHUD showError:@"两次密码不一致，请重新输入"];
        return;
    }
    NSString *base64OldPassword =[ToolOfBasic encodeBase64String:self.oldPass.text];
    NSString *base64NewPassword = [ToolOfBasic encodeBase64String:self.NewPass.text];
    NSDictionary *dic = @{@"pwd":base64OldPassword,@"newpwd":base64NewPassword};
    Y_Wait
    [[ToolOfNetWork sharedTools]YrequestURL:oa_updatePwdByTel withParams:dic.mutableCopy finished:^(id responsObject, NSError *error) {
        Y_Hide
        if (_Success) {
            [MBProgressHUD showSuccess:responsObject[@"message"]];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
            
        }else{
            [MBProgressHUD showError:responsObject[@"message"]];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
