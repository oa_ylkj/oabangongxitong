//
//  SetViewController.m
//  OA
//
//  Created by Admin on 2017/8/3.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "SetViewController.h"
#import "AppDelegate.h"
@interface SetViewController ()
@property (weak, nonatomic) IBOutlet UILabel *cacenSize;

@end

@implementation SetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    float index = [self folderSizeAtPath:NSHomeDirectory()];
    if (index < 1.00) {
        self.cacenSize.text = @"暂无缓存";
    } else {
        self.cacenSize.text = [NSString stringWithFormat:@"%.2fM",index];
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSInteger index = indexPath.row;
    switch (index) {
        case 0:
            // 修改密码
            [self performSegueWithIdentifier:@"PassChangeVC" sender:self];
            break;
        case 1:
            // 用户协议
            [self performSegueWithIdentifier:@"AgreementVC" sender:self];
            break;
        case 2:
            // 意见反馈
            [self performSegueWithIdentifier:@"FeedbackVC" sender:self];
            break;
        case 3:
            // 关于我们
            [self performSegueWithIdentifier:@"AboutVC" sender:self];
            break;
        case 4: {
                [self clearCache:NSHomeDirectory()];
            
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    self.cacenSize.text = @"暂无缓存";
                    [MBProgressHUD showSuccess  :@"缓存已清除"];
                
                });
            }
            break;
        case 5: {
                [self outAction];
            }
            break;
        default:
            break;
    }
    
}
- (void)outAction{
    //
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    self.view.window.rootViewController = appDelegate.nav;
    
}

- (float) folderSizeAtPath:( NSString *) folderPath{
    
    NSFileManager * manager = [ NSFileManager defaultManager ];
    if (![manager fileExistsAtPath :folderPath]) return 0 ;
    NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath :folderPath] objectEnumerator ];
    NSString * fileName;
    long long folderSize = 0 ;
    while ((fileName = [childFilesEnumerator nextObject ]) != nil ){
        NSString * fileAbsolutePath = [folderPath stringByAppendingPathComponent :fileName];
        folderSize += [ self fileSizeAtPath :fileAbsolutePath];
    }
    return folderSize/( 1024.0 * 1024.0 );
}
- (long long) fileSizeAtPath:( NSString *) filePath{
    
    NSFileManager * manager = [ NSFileManager defaultManager ];
    if ([manager fileExistsAtPath :filePath]){
        return [[manager attributesOfItemAtPath :filePath error : nil ] fileSize ];
        
    }
    
    return 0 ;
    
}
-(void)clearCache:(NSString *)path{
    NSFileManager *fileManager=[NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:path]) {
        NSArray *childerFiles=[fileManager subpathsAtPath:path];
        for (NSString *fileName in childerFiles) {
            NSString *absolutePath=[path stringByAppendingPathComponent:fileName];
            [fileManager removeItemAtPath:absolutePath error:nil];
        }
    }
    [[SDImageCache sharedImageCache] cleanDisk];
}
@end
