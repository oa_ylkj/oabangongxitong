//
//  MyViewController.m
//  OA
//
//  Created by yy on 17/6/5.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "MyViewController.h"
#import "AppDelegate.h"
#import "SetViewController.h"
#import "SeeTheLogViewController.h"
@interface MyViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *namel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *zhiwL;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *companyL;
@property (weak, nonatomic) IBOutlet UILabel *headName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *renzL;

// 我的资料
@property (weak, nonatomic) IBOutlet UIView *headImg;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *zhiwei;
@property (weak, nonatomic) IBOutlet UILabel *company;
@property (weak, nonatomic) IBOutlet UILabel *dengji;

@end

@implementation MyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.namel.constant = 15.0 / 667 * Y_mainH;
    self.zhiwL.constant = 15.0 / 667 * Y_mainH;
    self.companyL.constant = 15.0 / 667 * Y_mainH;
    self.renzL.constant = 15.0 / 667 * Y_mainH;
    
    self.headImg.layer.cornerRadius = 35.0 / 375.0 * Y_mainW;
    self.headImg.clipsToBounds = YES;
    
    self.name.text = [ShareUserInfo sharedUserInfo].userModel.name;
    self.zhiwei.text = [ShareUserInfo sharedUserInfo].userModel.positionName;
    self.company.text = [ShareUserInfo sharedUserInfo].userModel.myUnitName;
    
    if (self.name.text.length > 2) {
        self.headName.text = [self.name.text substringFromIndex:self.name.text.length - 2];
    } else {
        self.headName.text = self.name.text;
    }
}
#pragma mark -- 我的二维码
- (IBAction)erweima:(id)sender {//工作计划
    WorkPlanViewController *workPlanVc = VCInSB(@"WorkPlanViewController", @"ThirdViewControllers");
    workPlanVc.isMyListVc = YES;
    [self.navigationController pushViewController:workPlanVc animated:YES];
}
#pragma mark -- 我的二维码
- (IBAction)email:(id)sender {
    MaintenanceViewController *maintenanceVc = VCInSB(@"MaintenanceViewController", @"ThirdViewControllers");
    maintenanceVc.isMyMaintenanceList = YES;
    [self.navigationController pushViewController:maintenanceVc animated:YES];
}
#pragma mark -- 我的二维码
- (IBAction)kaoqin:(id)sender {
    TotalStatisticsViewController *totalStatisticsVc = VCInSB(@"TotalStatisticsViewController", @"ThirdViewControllers");
    totalStatisticsVc.isMy = YES;
    [self.navigationController pushViewController:totalStatisticsVc animated:YES];
    
}
#pragma mark -- 我的二维码
- (IBAction)rizhi:(id)sender {
    SeeTheLogViewController *vc = VCInSB(@"SeeTheLogViewController", @"FirstViewControllers");
    vc.type = @"1";
    vc.hide = @"1";
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark -- 我的二维码
- (IBAction)set:(id)sender {
    SetViewController *vc = VCInSB(@"SetViewController", @"MyViewController");
    [self.navigationController pushViewController:vc animated:YES];
    
    
}


- (void)outAction{
    //
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    self.view.window.rootViewController = appDelegate.nav;
    
}
- (IBAction)logoutAction:(UIButton *)sender {
    [self outAction];
}

@end
