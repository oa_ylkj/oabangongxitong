//
//  FeedbackVC.m
//  北碚区博物馆巡查系统
//
//  Created by Admin on 2016/12/23.
//  Copyright © 2016年 yaltec. All rights reserved.
//

#import "FeedbackVC.h"

@interface FeedbackVC ()<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *content;

@end

@implementation FeedbackVC
{
    BOOL isEmpty;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.title = @"意见反馈";
    self.content.delegate = self;
    self.content.textColor = [UIColor lightGrayColor];
    isEmpty = YES;

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma -- UITextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView*)textView {
    if (isEmpty) {
        self.content.text = @"";
        self.content.textColor = [UIColor blackColor];
        isEmpty = NO;
    }
    return YES;
}

- (void) textViewDidEndEditing:(UITextView*)textView {
    if(self.content.text.length == 0){
        self.content.text = @"对APP有什么意见...";
        self.content.textColor = [UIColor lightGrayColor];
        isEmpty = YES;
    }
}

// 提交

- (IBAction)submit:(id)sender {
    if (CGColorEqualToColor(self.content.textColor.CGColor,[UIColor lightGrayColor].CGColor)){
        [MBProgressHUD showError:@"请输入反馈内容"];
        return;
    }
    NSDictionary *dic = @{@"content":self.content.text};
    Y_Wait
    [[ToolOfNetWork sharedTools] YrequestURL:oa_saveSuggest withParams:dic.mutableCopy finished:^(id responsObject, NSError *error) {
        Y_Hide
        if (_Success) {
            [MBProgressHUD showSuccess:@"您的意见已提交"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        } else {
            [MBProgressHUD showError:responsObject[@"message"]];
        }
        NSLog(@"%@",[[responsObject description]kdtk_stringByReplaceingUnicode]);
        NSLog(@"%@",error.localizedDescription);
    }];
}

@end
