//
//  MissPassWordViewController.m
//  OA
//
//  Created by yy on 17/6/15.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "MissPassWordViewController.h"

@interface MissPassWordViewController ()
@property (weak, nonatomic) IBOutlet UITextField *passWordTextField;
@property (weak, nonatomic) IBOutlet UITextField *twoPassWordTextFiled;
@property (nonatomic,copy) NSString *onePassStr;
@property (nonatomic,copy) NSString *twoPassStr;
@end

@implementation MissPassWordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     self.navigationController.navigationBar.hidden = NO;
     self.title = @"重新设置密码";
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)finishAction:(UIButton *)sender {
    _onePassStr = [_passWordTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _twoPassStr = [_twoPassWordTextFiled.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (_onePassStr.length <= 0 || _twoPassStr.length <= 0) {
        [MBProgressHUD showError:@"请输入密码！"];
        return;
    }
    if (![_onePassStr isEqualToString:_twoPassStr]) {
        [MBProgressHUD showError:@"两次密码不匹配！"];
        return;
    }
    
    NSMutableDictionary *parm = [NSMutableDictionary dictionary];
    [parm setObject:_phoneStr forKey:@"mobile"];
    [parm setObject:[ToolOfBasic encodeBase64String:_twoPassStr]  forKey:@"pwd"];
    [[ToolOfNetWork  sharedTools]YrequestURL:oa_setPassword withParams:parm finished:^(id responsObject, NSError *error) {
        if (_Success) {
            //返回上上级
            [MBProgressHUD showSuccess:@"密码设置成功!"];
            int index = (int)[[self.navigationController viewControllers]indexOfObject:self];
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(index -2)] animated:YES];
        }else{
            Y_MBP_ErrMessage
        }
    }];
}

@end
