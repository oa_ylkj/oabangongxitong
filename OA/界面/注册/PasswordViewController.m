//
//  PasswordViewController.m
//  OA
//
//  Created by yy on 17/6/14.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "PasswordViewController.h"

@interface PasswordViewController ()
@property (weak, nonatomic) IBOutlet UITextField *onePassWordTextFiled;
@property (weak, nonatomic) IBOutlet UITextField *twoPassWordTextFiled;
@property (weak, nonatomic) IBOutlet UIButton *finishBtn;
@property (nonatomic,copy) NSString *onePassStr;
@property (nonatomic,copy) NSString *twoPassStr;
@end

@implementation PasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"忘记密码";
    [self initializeUserInterface];
    
}
- (void)initializeUserInterface{
    _finishBtn.layer.cornerRadius = 8;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)finishAction:(UIButton *)sender {
    
    _onePassStr = [_onePassWordTextFiled.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _twoPassStr = [_twoPassWordTextFiled.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (_onePassStr.length <= 0 || _twoPassStr.length <= 0) {
        [MBProgressHUD showError:@"请输入密码！"];
        return;
    }
    if (![_onePassStr isEqualToString:_twoPassStr]) {
        [MBProgressHUD showError:@"两次密码不匹配！"];
        return;
    }
    
    //返回上上级
    int index = (int)[[self.navigationController viewControllers]indexOfObject:self];
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:(index -2)] animated:YES];
}

@end
