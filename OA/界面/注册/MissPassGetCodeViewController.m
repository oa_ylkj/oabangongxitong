//
//  MissPassGetCodeViewController.m
//  OA
//
//  Created by yy on 17/6/15.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "MissPassGetCodeViewController.h"

@interface MissPassGetCodeViewController ()
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UIButton *sendSmsCodeBtn;
@property (nonatomic,copy) NSString *phoneStr;
@property (nonatomic,copy) NSString *codeNumStr;
@end

@implementation MissPassGetCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _phoneNumberTextField.text = @"";
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor= BlueColor;
    [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:Y_IMAGE(@"返回icon")];
    [UIBarButtonItem.appearance setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -64) forBarMetrics:UIBarMetricsDefault];
     self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont systemFontOfSize:20]};
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark —————— 验证码
- (IBAction)getCodeAction:(UIButton *)sender {
    _phoneStr = [_phoneNumberTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (_phoneStr.length == 0) {
        [MBProgressHUD showError:@"请输入手机号！"];
        return;
    }
    //    Y_Wait
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:_phoneStr forKey:@"mobile"];
    NSLog(@"%@--%@",_phoneStr,_codeNumStr);
        [[ToolOfNetWork sharedTools]YrequestURL:oa_sendSmsCode  withParams:dic  finished:^(id responsObject, NSError *error) {
            Y_Hide
            if (_Success) {
                [MBProgressHUD showSuccess:responsObject[@"message"]];
                NSLog(@"ok==%@",[[responsObject description]kdtk_stringByReplaceingUnicode]);
                [self countdown];
    
            }else{
                [MBProgressHUD showError:responsObject[@"message"]];
                NSLog(@"error==%@",error.localizedDescription);
            }
        }];
//    [self countdown];
    
}
#pragma mark ——————
//MARK: 倒计时
- (void)countdown {
    __block NSInteger time = 59; //倒计时时间
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    
    dispatch_source_set_event_handler(_timer, ^{
        
        if(time <= 0){ //倒计时结束，关闭
            
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //设置按钮的样式
                self.sendSmsCodeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
                [self.sendSmsCodeBtn setTitle:@"重新发送" forState:UIControlStateNormal];
                [self.sendSmsCodeBtn setTitleColor:BlueColor forState:UIControlStateNormal];
                [self.sendSmsCodeBtn setBackgroundImage:Y_IMAGE(@"获取验证码点击按钮") forState:UIControlStateNormal];
                self.sendSmsCodeBtn.userInteractionEnabled = YES;
            });
            
        }else{
            
            int seconds = time % 60;
            dispatch_async(dispatch_get_main_queue(), ^{
                self.sendSmsCodeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
                //设置按钮显示读秒效果
                [self.sendSmsCodeBtn setTitle:[NSString stringWithFormat:@"%.2ds", seconds] forState:UIControlStateNormal];
                [self.sendSmsCodeBtn setBackgroundImage:Y_IMAGE(@"等待验证码状态") forState:UIControlStateNormal];
                [self.sendSmsCodeBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                self.sendSmsCodeBtn.userInteractionEnabled = NO;
            });
            time--;
        }
    });
    dispatch_resume(_timer);
    
    
}


- (IBAction)nextBtnAction:(UIButton *)sender {
    _phoneStr = [_phoneNumberTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    _codeNumStr = [_codeTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:_phoneStr forKey:@"mobile"];
    [dic setObject:_codeNumStr forKey:@"verifyCode"];
    
    [[ToolOfNetWork sharedTools]YrequestURL:oa_forgetPWDVerify  withParams:dic  finished:^(id responsObject, NSError *error) {
        Y_Hide
        if (_Success) {
            [MBProgressHUD showSuccess:responsObject[@"message"]];
            MissPassWordViewController *missPasswordVc = VCInSB(@"MissPassWordViewController", @"RegisteredViewController");
            missPasswordVc.phoneStr = _phoneStr;
            [self.navigationController pushViewController:missPasswordVc animated:YES];
        }else{
            [MBProgressHUD showError:responsObject[@"message"]];
            
        }
    }];

    
}


@end
