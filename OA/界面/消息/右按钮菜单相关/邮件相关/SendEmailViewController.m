//
//  SendEmailViewController.m
//  OA
//
//  Created by yy on 17/6/16.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "SendEmailViewController.h"

@interface SendEmailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *personL;
@property (weak, nonatomic) IBOutlet UITextField *titleTextFiled;

@property (weak, nonatomic) IBOutlet UILabel *fileNameL;
@property (weak, nonatomic) IBOutlet UITextView *contentTextView;

@end

@implementation SendEmailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark ——————
- (IBAction)toSendAction:(UIBarButtonItem *)sender {
     NSLog(@"发送");
    
}
- (IBAction)addPerson:(UIButton *)sender {
     NSLog(@"收件人");
}
- (IBAction)addFile:(UIButton *)sender {
     NSLog(@"附件");
}

@end
