//
//  EmailViewController.m
//  OA
//
//  Created by yy on 17/6/15.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "EmailViewController.h"
#import "EmailListTableViewCell.h"
@interface EmailViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *inboxBtn;
@property (weak, nonatomic) IBOutlet UIButton *sendEmailBtn;

@end

@implementation EmailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeUserInterface];
    
    
}
- (void)initializeUserInterface{
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [UIView new];
    
    _inboxBtn.layer.borderWidth = 0.3;
    _inboxBtn.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    _sendEmailBtn.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    _sendEmailBtn.layer.borderWidth = 0.3;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)inboxAction:(UIButton *)sender {
     NSLog(@"收件箱");
}
- (IBAction)sendEmailAction:(UIButton *)sender {
     NSLog(@"发邮件");
    SendEmailViewController *sendEmailVc = VCInSB(@"SendEmailViewController", @"EmailViewController");
    [self.navigationController pushViewController:sendEmailVc  animated:YES];
    
}
#pragma mark —————— 

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
     EmailListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EmailListTableViewCell"];
    if (!cell) {
        cell = [[EmailListTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"EmailListTableViewCell"];
    }
    return cell;
}


@end
