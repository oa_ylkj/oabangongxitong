//
//  EmailListTableViewCell.h
//  OA
//
//  Created by yy on 17/6/16.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmailListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *timeL;
@property (weak, nonatomic) IBOutlet UILabel *typeL;
@property (weak, nonatomic) IBOutlet UILabel *conL;
@property (weak, nonatomic) IBOutlet UIImageView *img;

@end
