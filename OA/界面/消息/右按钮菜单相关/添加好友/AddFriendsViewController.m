//
//  AddFriendsViewController.m
//  OA
//
//  Created by yy on 17/6/13.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "AddFriendsViewController.h"
#import "AddFriendsTableViewCell.h"
#import "SZQRCodeViewController.h"
@interface AddFriendsViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong)NSArray *titleArr;
@end

@implementation AddFriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"加好友";
    [self initOfDataSouce];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [UIView new];
    
}
- (void)initOfDataSouce{
    _titleArr = [NSArray arrayWithObjects:@"搜索手机号码添加",@"通过手机联系人添加",@"扫一扫添加",@"我的二维码", nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AddFriendsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddFriendsTableViewCell"];
    if (!cell) {
        cell = [[AddFriendsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AddFriendsTableViewCell"];
    }
    cell.titleL.text  = [NSString stringWithFormat:@"%@",_titleArr[indexPath.row]];;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    [self choose:indexPath];
}

- (void)choose:(NSIndexPath*)indexPath{
    if (indexPath.row==0) {
        
    }
    if (indexPath.row==1) {
        
    }
    if (indexPath.row==2) {
        NSLog(@"扫一扫");
        SZQRCodeViewController *vc = [[SZQRCodeViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (indexPath.row==3) {
        
    }
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
