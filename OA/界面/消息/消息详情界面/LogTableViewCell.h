//
//  LogTableViewCell.h
//  OA
//
//  Created by yy on 17/6/19.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleL;//类别标题
@property (weak, nonatomic) IBOutlet UILabel *personL;//xx的日报
@property (weak, nonatomic) IBOutlet UILabel *completeL;//已完成的工作
@property (weak, nonatomic) IBOutlet UILabel *unfinishedL;//未完成的工作
@property (weak, nonatomic) IBOutlet UILabel *coordinateL;//需要协调的工作

@property (nonatomic,strong)NSDictionary *dicWithLog;

@end
