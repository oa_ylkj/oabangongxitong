//
//  ClockTableViewCell.h
//  OA
//
//  Created by yy on 17/6/19.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClockTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *typeTitleL;//类别
@property (weak, nonatomic) IBOutlet UILabel *titleL;//标题
@property (weak, nonatomic) IBOutlet UILabel *contentL;//内容

@property (nonatomic,strong)NSDictionary *dicWithClockCell;
@end
