//
//  OverTimeTableViewCell.m
//  OA
//
//  Created by yy on 17/7/21.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "OverTimeTableViewCell.h"

@implementation OverTimeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDicWithOverTime:(NSDictionary *)dicWithOverTime{
    _dicWithOverTime = dicWithOverTime;
    MessageModel *model = [MessageModel objectWithKeyValues:_dicWithOverTime];
    _resonL.text = model.reason;
    _contentL.text = model.content;
     _personL.text = [NSString stringWithFormat:@"%@的加班申请",model.username];
    _isHoliday.text = [NSString stringWithFormat:@"是否法定节假日:%@",model.isholiday];
    _tyoeL.text = model.statusName;
 
}
@end
