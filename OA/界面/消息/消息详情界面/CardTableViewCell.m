//
//  CardTableViewCell.m
//  OA
//
//  Created by yy on 17/7/21.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "CardTableViewCell.h"

@implementation CardTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setDicWithCard:(NSDictionary *)dicWithCard{
    _dicWithCard = dicWithCard;
    MessageModel *model = [MessageModel objectWithKeyValues:dicWithCard];
    _timeL.text = [model.datenumber substringToIndex:10];
     _personL.text = [NSString stringWithFormat:@"%@的补卡申请",model.username];
    _resonL.text = model.reason;
    _tyepL.text = model.statusName;

}
@end
