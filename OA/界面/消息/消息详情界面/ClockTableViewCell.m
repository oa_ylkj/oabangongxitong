//
//  ClockTableViewCell.m
//  OA
//
//  Created by yy on 17/6/19.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "ClockTableViewCell.h"

@implementation ClockTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setDicWithClockCell:(NSDictionary *)dicWithClockCell{
    _dicWithClockCell = dicWithClockCell;
    MessageModel *model = [MessageModel objectWithKeyValues:_dicWithClockCell];
    _titleL.text = model.name;
    _contentL.text = model.content;
    if ([model.pushTypeId intValue] != 105) {//pushTypeId 105通知；运维 104； 101日志； 申请类102+01请假   加班02 出差03 外勤04 补卡05；审批类102+01请假 加班02 出差03 外勤04 补卡05
        _typeTitleL.text = @"考勤打卡";
    }else{
        _typeTitleL.text = @"通知";
    }
    
    //    cell.contentL.text = @"ClockTableViewCellClockTableViewCellClockTableViewCellClockTableViewCellClockTableViewCellClockTableViewCellClockTableViewCellClockTableViewCellClockTableViewCell";
}
@end
