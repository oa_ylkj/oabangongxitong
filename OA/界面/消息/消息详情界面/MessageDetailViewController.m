//
//  MessageDetailViewController.m
//  OA
//
//  Created by yy on 17/6/19.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "MessageDetailViewController.h"
#import "ClockTableViewCell.h"
#import "LogTableViewCell.h"
#import "TimeLabelTableViewCell.h"
#import "FieldTableViewCell.h"
#import "CardTableViewCell.h"
#import "OverTimeTableViewCell.h"
#import "LeaveTableViewCell.h"



#import "KaoqinViewController.h"

@interface MessageDetailViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong)NSMutableArray *arrOfdataSource;
@property (nonatomic,assign) int pageNo;
@end

@implementation MessageDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self initializeUserInterface];
    Y_tableViewMj_header([weakSelf initOfDataSouce];)
    Y_tableViewMj_footer([weakSelf getNewData];)
    [self initOfDataSouce];
}
- (void)initOfDataSouce{
    _pageNo = 1;
    NSMutableDictionary *parm = @{}.mutableCopy;
    [parm setObject:[NSString stringWithFormat:@"%d",_pageNo] forKey:@"pageNo"];
    [parm setObject:Y_pageSizeStr forKey:@"pageSize"];
    [[ToolOfNetWork sharedTools]YrequestURL:oa_queryList withParams:parm finished:^(id responsObject, NSError *error) {
        Y_headerEndRefreshing
        Y_footerEndRefreshing
        if (_Success) {
            _arrOfdataSource  = [NSMutableArray arrayWithArray:responsObject[@"data"]];
            [_tableView reloadData];
            if (_arrOfdataSource.count>0) {
                _pageNo += 1;
                
            }else{
                Y_MBP_SuccMessage;
            }
        }else{
            Y_MBP_Error;
        }
    }];
}
- (void)getNewData{
    NSMutableDictionary *parm = @{}.mutableCopy;
    [parm setObject:[NSString stringWithFormat:@"%d",_pageNo] forKey:@"pageNo"];
    [parm setObject:Y_pageSizeStr forKey:@"pageSize"];
    [[ToolOfNetWork sharedTools]YrequestURL:oa_queryList withParams:parm finished:^(id responsObject, NSError *error) {
        Y_headerEndRefreshing
        Y_footerEndRefreshing
        if (_Success) {
            [_arrOfdataSource addObjectsFromArray:responsObject[@"data"]];
            if (_arrOfdataSource.count>0) {
                _pageNo += 1;
                
            }else{
                Y_MBP_SuccMessage;
            }
            [_tableView reloadData];
        }else{
            Y_MBP_Error;
        }
    }];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //红角标消失
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}
- (void)initializeUserInterface{
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.estimatedRowHeight = 350;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

//- (void)initOfDataSouce{
//    _arrOfdataSource = [NSMutableArray array];
//    for (int i = 0 ; i < 8; i ++) {
//       
//        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//        [dic setObject:@"titleStr" forKey:@"title"];
//        [dic setObject:@"2017-07-22 12:90" forKey:@"time"];
//        [dic setObject:@"contentStrcontentStrcontentStrcontentStrcontentStrcontentStrcontentStrcontentStrcontentStrcontentStrcontentStr" forKey:@"content"];
//        [dic setObject:@"resonStr" forKey:@"reson"];
//        [dic setObject:@"personStr" forKey:@"person"];
//        [dic setObject:@"天数" forKey:@"day"];
//        [dic setObject:[NSString stringWithFormat:@"%d",i] forKey:@"type"];
//        
//        [_arrOfdataSource addObject:dic];
//    }
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark —————— 

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arrOfdataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MessageModel *model = [MessageModel objectWithKeyValues:_arrOfdataSource[indexPath.row]];
    //pushTypeId 105通知；运维 104； 101日志； 申请类102+01请假   加班02 出差03 外勤04 补卡05；审批类102+01请假 加班02 出差03 外勤04 补卡05
    switch ([model.pushTypeId intValue]) {
        //日志
        case 101:
             return  [self logtableView:tableView cellForRowAtIndexPath:indexPath];
            break;
        //通知
        case 105:
             return  [self clockTableView:tableView cellForRowAtIndexPath:indexPath];
            break;
        case 2:
             return  [self timeLableTableView:tableView cellForRowAtIndexPath:indexPath];
            break;
        //请假
        case 10201:
             return  [self leaveTableView:tableView cellForRowAtIndexPath:indexPath];
            break;
        case 10301:
            return  [self leaveTableView:tableView cellForRowAtIndexPath:indexPath];
            break;
        //加班
        case 10202:
             return  [self overTimeTableView:tableView cellForRowAtIndexPath:indexPath];
            break;
        case 10302:
            return  [self overTimeTableView:tableView cellForRowAtIndexPath:indexPath];
            break;
            
            
        //出差外勤
            
        
        case 10203:
            return  [self  fieldLableTableView:tableView cellForRowAtIndexPath:indexPath];//外勤
            break;
        case 10303:
            return  [self  fieldLableTableView:tableView cellForRowAtIndexPath:indexPath];//外勤
            break;
        case 10204:
            return  [self  fieldLableTableView:tableView cellForRowAtIndexPath:indexPath];//外勤
            break;
        case 10304:
            return  [self  fieldLableTableView:tableView cellForRowAtIndexPath:indexPath];//外勤
            break;
        //补卡
        case 10205:
            return    [self cardTableView:tableView cellForRowAtIndexPath:indexPath];
            break;
        case 10305:
            return    [self cardTableView:tableView cellForRowAtIndexPath:indexPath];
            break;


        default:
           return    [self  fieldLableTableView:tableView cellForRowAtIndexPath:indexPath];//出差
            break;
    }
   
}
- (LogTableViewCell *)logtableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    LogTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LogTableViewCell"];
    if (!cell) {
        cell = [[LogTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"LogTableViewCell"];
    }
    
    cell.dicWithLog = _arrOfdataSource[indexPath.row];
    return cell;
}

- (ClockTableViewCell *)clockTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ClockTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ClockTableViewCell"];
    if (!cell) {
        cell = [[ClockTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ClockTableViewCell"];
    }
    cell.dicWithClockCell = _arrOfdataSource[indexPath.row];
    return cell;
}
- (TimeLabelTableViewCell *)timeLableTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TimeLabelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TimeLabelTableViewCell"];
    if (!cell) {
        cell = [[TimeLabelTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TimeLabelTableViewCell"];
    }
    cell.timeL.text = @"7月11";
    return cell;
}

- (LeaveTableViewCell *)leaveTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LeaveTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LeaveTableViewCell"];
    if (!cell) {
        cell = [[LeaveTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"LeaveTableViewCell"];
    }
    cell.dicWithLeave = _arrOfdataSource[indexPath.row];
    return cell;
}

- (OverTimeTableViewCell *)overTimeTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OverTimeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OverTimeTableViewCell"];
    if (!cell) {
        cell = [[OverTimeTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"OverTimeTableViewCell"];
    }
    cell.dicWithOverTime = _arrOfdataSource[indexPath.row];
    return cell;
}

- (CardTableViewCell *)cardTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CardTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CardTableViewCell"];
    if (!cell) {
        cell = [[CardTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CardTableViewCell"];
    }
    cell.dicWithCard = _arrOfdataSource[indexPath.row];
    return cell;
}

- (FieldTableViewCell *)fieldLableTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FieldTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FieldTableViewCell"];
    if (!cell) {
        cell = [[FieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FieldTableViewCell"];
    }
    cell.dicWithField = _arrOfdataSource[indexPath.row];
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    MessageModel *model = [MessageModel objectWithKeyValues:_arrOfdataSource[indexPath.row]];
    
    switch ([model.pushTypeId intValue]) {
            
        case 105:
            return 170;
            break;
        case 2:
            return 50;
            break;

        default:
          
            return UITableViewAutomaticDimension;
            break;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    MessageModel *model = [MessageModel objectWithKeyValues:_arrOfdataSource[indexPath.row]];
    //pushTypeId 105通知；运维 104； 101日志； 申请类102+01请假   加班02 出差03 外勤04 补卡05；审批类102+01请假 加班02 出差03 外勤04 补卡05
    switch ([model.pushTypeId intValue]) {
            //日志
        case 101:
            [self rz:model];
            break;
            //通知
        case 105:
            [self tz:model];
            break;
            //考勤打卡提醒
        case 2:
            
            break;
//            //请假
        case 10201:
            [self qj:model isShenHePerSon:YES];
            break;
        case 10301:
            [self qj:model isShenHePerSon:NO];
            break;
            //加班
        case 10202:
            [self jb:model isShenHePerSon:YES];
            break;
        case 10302:
            [self jb:model isShenHePerSon:NO];
            break;
            
           //出差
        case 10203:
             [self cc:model isShenHePerSon:YES];
            break;
        case 10303:
             [self cc:model isShenHePerSon:NO];
            break;
          //外勤
        case 10204:
             [self wq:model isShenHePerSon:YES];
            break;
        case 10304:
             [self wq:model isShenHePerSon:NO];
            break;
            //补卡
        case 10205:
            [self bk:model isShenHePerSon:YES];
            break;
        case 10305:
             [self bk:model isShenHePerSon:NO];
            break;
            
            
        default:
            
            break;
    }

}
- (void)rz:(MessageModel*)model{
    BlogDetailViewController *vc = VCInSB(@"BlogDetailViewController", @"FirstViewControllers");
    vc.blogId = [model.id substringWithRange:NSMakeRange(model.id.length-8, 8)];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)tz:(MessageModel*)model{
    NoticeDetailViewController *noticeDetailVc = VCInSB(@"NoticeDetailViewController", @"SecondViewControllers");
//    noticeDetailVc.idOfdetail = model.id;
    noticeDetailVc.idOfdetail = [model.id substringWithRange:NSMakeRange(model.id.length-8, 8)];
    [self.navigationController pushViewController:noticeDetailVc animated:YES];
}
//- (void)jb:(MessageModel *)model isShenHePerSon:(BOOL)isShenhe{
////    if ([model.status intValue]== 2) {
////        
////    }else{
////        [self finishType:@"1" model:model isShenHe:isShenhe];
////    }
//    [self type:@"1" model:model isShenHe:isShenhe];
//    
//}
//- (void)qj:(MessageModel*)model isShenHePerSon:(BOOL)isShenhe{
//    [self type:@"3" model:model isShenHe:isShenhe];
//}
//- (void)cc:(MessageModel *)model isShenHePerSon:(BOOL)isShenhe{
//    [self type:@"5" model:model isShenHe:isShenhe];
//}
//- (void)wq:(MessageModel *)model isShenHePerSon:(BOOL)isShenhe{
//    [self type:@"7" model:model isShenHe:isShenhe];
//}
//- (void)bk:(MessageModel *)model isShenHePerSon:(BOOL)isShenhe{
//    [self type:@"9" model:model isShenHe:isShenhe];
//}
#pragma mark —————— 申请审核类

- (void)jb:(MessageModel *)model isShenHePerSon:(BOOL)isShenhe{
    if ([model.status intValue]== 0) {
        [self type:@"1" model:model isShenHe:isShenhe];
    }else{
        [self finishType:@"1" model:model isShenHe:isShenhe];
    }
    
}
- (void)qj:(MessageModel*)model isShenHePerSon:(BOOL)isShenhe{
    if ([model.status intValue] == 0) {
        [self type:@"3" model:model isShenHe:isShenhe];
    }else{
        [self finishType:@"3" model:model isShenHe:isShenhe];
    }
    
}
- (void)cc:(MessageModel *)model isShenHePerSon:(BOOL)isShenhe{
    if ([model.status intValue] == 0) {
        [self type:@"5" model:model isShenHe:isShenhe];
    }else{
        [self finishType:@"5" model:model isShenHe:isShenhe];
    }
}
- (void)wq:(MessageModel *)model isShenHePerSon:(BOOL)isShenhe{
    if ([model.status intValue] == 0) {
        [self type:@"7" model:model isShenHe:isShenhe];
    }else{
        [self finishType:@"7" model:model isShenHe:isShenhe];
    }
}
- (void)bk:(MessageModel *)model isShenHePerSon:(BOOL)isShenhe{
    
    if ([model.status intValue] == 0) {
        [self type:@"9" model:model isShenHe:isShenhe];
    }else{
        [self finishType:@"9" model:model isShenHe:isShenhe];
    }
}


- (void)type:(NSString*)typeStr model:(MessageModel *)model isShenHe:(BOOL)isShenhe{
    AuditViewController  *auditViewVc = VCInSB(@"AuditViewController", @"SecondViewControllers");
    NSString *idS = model.id;;
    idS = idS.length<=36?idS:[idS substringWithRange:NSMakeRange(idS.length-36, 36)];
    auditViewVc.idOfAudit = idS;
    auditViewVc.typeStr = typeStr;
    
//    auditViewVc.isOperationer = isShenhe;
    [self.navigationController pushViewController:auditViewVc animated:YES];
}
- (void)finishType:(NSString*)typeStr model:(MessageModel *)model isShenHe:(BOOL)isShenhe{
    AuditTwoViewController  *auditTwoViewVc = VCInSB(@"AuditTwoViewController", @"SecondViewControllers");
    NSString *idS = model.id;;
    idS = idS.length<=36?idS:[idS substringWithRange:NSMakeRange(idS.length-36, 36)];
    auditTwoViewVc.idOfAudit = idS;
    auditTwoViewVc.typeStr = typeStr;
    
    [self.navigationController pushViewController:auditTwoViewVc animated:YES];
}
#pragma mark —————— push
- (void)pushRiZivcdidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *idStr = _arrOfdataSource[indexPath.row];
}
- (void)pushKaoQinZivcdidSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *idStr = _arrOfdataSource[indexPath.row];
    KaoqinViewController *kaoQinVc = VCInSB(@"KaoqinViewController", @"FirstViewControllers");
    [self.navigationController pushViewController:kaoQinVc animated:YES];
    
}


@end
