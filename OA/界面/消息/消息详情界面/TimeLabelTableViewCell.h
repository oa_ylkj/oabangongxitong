//
//  TimeLabelTableViewCell.h
//  OA
//
//  Created by yy on 17/7/14.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeLabelTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *timeL;

@end
