//
//  FieldTableViewCell.m
//  OA
//
//  Created by yy on 17/7/21.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "FieldTableViewCell.h"

@implementation FieldTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDicWithField:(NSDictionary *)dicWithField{
    _dicWithField = dicWithField;
    
    MessageModel *model = [MessageModel objectWithKeyValues:dicWithField];
    
    _resonL.text = model.reason;
    _dayL.text = model.num;
    _timeL.text = [model.createtime substringToIndex:16];
    _titleL.text = [model.type isEqualToString:@"7"] ?@"外勤":@"出差";//
    
    if ([model.pushTypeId intValue] == 10303 || [model.pushTypeId intValue] == 10204) {
        _typeTitleL.text = @"外勤";
        _titleTL.text = @"外勤";
        _personL.text = [NSString stringWithFormat:@"%@的外勤申请",model.username];
        _resonTitleL.text = @"外勤目的:";
        _timeTitleL.text = @"外勤时间:";
        _dayTitleL.text = @"外勤天数:";
        _addressL.text = model.address;
        _addressTitleL.text = @"外勤地点:";
        
    }else{
        _typeTitleL.text = @"出差";
        _titleTL.text = @"出差";
        _personL.text = [NSString stringWithFormat:@"%@的出差申请",model.username];
        _resonTitleL.text = @"出差事由:";
        _timeTitleL.text = @"出差时间:";
        _dayTitleL.text = @"出差天数:";
        _addressL.text = model.address;
        _addressTitleL.text = @"出差地点:";
    }
    _typeL.text = model.statusName;
//    if ([model.status intValue] ==0) {
//        _typeL.text = @"";
//    }else{
//        _typeL.text = @"";
//    }
    
}
@end
