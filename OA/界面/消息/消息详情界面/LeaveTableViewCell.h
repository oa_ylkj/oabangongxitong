//
//  LeaveTableViewCell.h
//  OA
//
//  Created by yy on 17/7/21.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeaveTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *personL;
@property (weak, nonatomic) IBOutlet UILabel *resonL;
@property (weak, nonatomic) IBOutlet UILabel *leaveTypeL;
@property (weak, nonatomic) IBOutlet UILabel *timeL;
@property (weak, nonatomic) IBOutlet UILabel *dayL;
@property (weak, nonatomic) IBOutlet UILabel *typeL;

@property (nonatomic,strong)NSDictionary *dicWithLeave;
@end
