//
//  LeaveTableViewCell.m
//  OA
//
//  Created by yy on 17/7/21.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "LeaveTableViewCell.h"

@implementation LeaveTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setDicWithLeave:(NSDictionary *)dicWithLeave{
    _dicWithLeave = dicWithLeave;
    MessageModel *model = [MessageModel objectWithKeyValues:_dicWithLeave];
    _leaveTypeL.text = model.leaveType;
    _timeL.text = [model.createtime substringToIndex:16];
    _resonL.text = model.reason;
    _dayL.text = model.num;
      _personL.text = [NSString stringWithFormat:@"%@的请假申请",model.username];
    _typeL.text = model.statusName;
    
}
@end
