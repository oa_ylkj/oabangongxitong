//
//  FieldTableViewCell.h
//  OA
//
//  Created by yy on 17/7/21.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FieldTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *personL;
@property (weak, nonatomic) IBOutlet UILabel *resonL;
@property (weak, nonatomic) IBOutlet UILabel *timeL;
@property (weak, nonatomic) IBOutlet UILabel *dayL;
@property (weak, nonatomic) IBOutlet UILabel *typeL;

@property (weak, nonatomic) IBOutlet UILabel *typeTitleL;
@property (weak, nonatomic) IBOutlet UILabel *titleTL;
@property (weak, nonatomic) IBOutlet UILabel *resonTitleL;
@property (weak, nonatomic) IBOutlet UILabel *timeTitleL;
@property (weak, nonatomic) IBOutlet UILabel *dayTitleL;
@property (weak, nonatomic) IBOutlet UILabel *addressL;
@property (weak, nonatomic) IBOutlet UILabel *addressTitleL;
@property (nonatomic,strong)NSDictionary *dicWithField;
@end
