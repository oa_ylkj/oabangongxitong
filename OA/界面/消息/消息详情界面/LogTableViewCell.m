//
//  LogTableViewCell.m
//  OA
//
//  Created by yy on 17/6/19.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "LogTableViewCell.h"

@implementation LogTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setDicWithLog:(NSDictionary *)dicWithLog{
    _dicWithLog = dicWithLog;
    
    MessageModel *model = [MessageModel objectWithKeyValues:_dicWithLog];
      _personL.text = [NSString stringWithFormat:@"%@的日志",model.creator];
    _completeL.text = model.content;
    _unfinishedL.text = model.unfinished_content;
    _coordinateL.text = model.concerted_content;
//     _completeL.text = @"LogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCell";
//    //    cell.unfinishedL.text = @"LogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCell";
//    
//    cell.coordinateL.text = @"LogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCellLogTableViewCell";
}
@end
