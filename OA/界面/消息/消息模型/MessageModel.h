//
//  MessageModel.h
//  OA
//
//  Created by yy on 17/7/24.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageModel : NSObject
@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *time;
@property (nonatomic,copy)NSString *person;
@property (nonatomic,copy)NSString *day;

@property (nonatomic,copy)NSString *beginAddress;
@property (nonatomic,copy)NSString *beginNote;
@property (nonatomic,copy)NSString *beginStatus;
@property (nonatomic,copy)NSString *beginType;
@property (nonatomic,copy)NSString *concerted_content;
@property (nonatomic,copy)NSString *content;
@property (nonatomic,copy)NSString *createtime;
@property (nonatomic,copy)NSString *creator;
@property (nonatomic,copy)NSString *earlyTime;
@property (nonatomic,copy)NSString *endAddress;
@property (nonatomic,copy)NSString *endNote;
@property (nonatomic,copy)NSString *endStatus;
@property (nonatomic,copy)NSString *endType;
@property (nonatomic,copy)NSString *id;
@property (nonatomic,copy)NSString *lateTime;
@property (nonatomic,copy)NSString *loginname;
@property (nonatomic,copy)NSString *myUnitId;
@property (nonatomic,copy)NSString *name;


/**   {
 beginAddress = "";
 beginNote = "";
 beginStatus = 3;
 beginType = "";
 "concerted_content" = "";
 content = "推送日志本地测试";
 createtime = "2017-08-02 15:48:39.0";
 creator = fff;
 earlyTime = 0;
 endAddress = "";
 endNote = "";
 endStatus = 3;
 endType = "";
 id = ff71cb24775611e7b58000163e0431ce00000057;
 lateTime = 0;
 loginname = "";
 myUnitId = 00000001;
 name = "日报";
 "num_cc" = "0.0";
 "num_cd" = 0;
 "num_cq" = 0;
 "num_jb" = "0.0";
 "num_kg" = 0;
 "num_qj" = "0.0";
 "num_qk" = 0;
 "num_wq" = "0.0";
 "num_xx" = 0;
 "num_zt" = 0;
 pushTypeId = 101;
 readPeople = "凤飞飞;章万;";
 status = 0;
 toWhom = "黄德江;莫倩;";
 toWhomId = "00000016;00000017;";
 type = "";
 typeStatus = 1;
 "unfinished_content" = "";
 username = "";
 },*/

@property (nonatomic,copy)NSString *num_cc;
@property (nonatomic,copy)NSString *num_cd;
@property (nonatomic,copy)NSString *num_cq;
@property (nonatomic,copy)NSString *num_jb;
@property (nonatomic,copy)NSString *num_kg;
@property (nonatomic,copy)NSString *num_qj;
@property (nonatomic,copy)NSString *num_qk;
@property (nonatomic,copy)NSString *num_wq;
@property (nonatomic,copy)NSString *num_xx;
@property (nonatomic,copy)NSString *num_zt;
@property (nonatomic,copy)NSString *pushTypeId;
@property (nonatomic,copy)NSString *readPeople;
@property (nonatomic,copy)NSString *status;
@property (nonatomic,copy)NSString *toWhom;
@property (nonatomic,copy)NSString *toWhomId;
@property (nonatomic,copy)NSString *type;
@property (nonatomic,copy)NSString *typeStatus;
@property (nonatomic,copy)NSString *unfinished_content;

@property (nonatomic,copy)NSString *username;
@property (nonatomic,copy)NSString *reason;
@property (nonatomic,copy)NSString *address;
@property (nonatomic,copy)NSString *auditorsName;
@property (nonatomic,copy)NSString *num;
@property (nonatomic,copy)NSString *statusName;
 
@property (nonatomic,copy)NSString *leaveType;
@property (nonatomic,copy)NSString *isholiday;
@property (nonatomic,copy)NSString *datenumber;
//@property (nonatomic,copy)NSString *;
//@property (nonatomic,copy)NSString *;
//@property (nonatomic,copy)NSString *;
//@property (nonatomic,copy)NSString *;
//@property (nonatomic,copy)NSString *;
//@property (nonatomic,copy)NSString *;
//@property (nonatomic,copy)NSString *;
//@property (nonatomic,copy)NSString *;
//@property (nonatomic,copy)NSString *;

@end
