//
//  MessageViewController.m
//  OA
//
//  Created by yy on 17/6/5.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "MessageViewController.h"
#import "MessageTableViewCell.h"
#import "SZQRCodeViewController.h"
#import "AddFriendsViewController.h"
#import "MessageDetailViewController.h"
@interface MessageViewController () <UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *rightItemView;
@property (weak, nonatomic) IBOutlet UIButton *rightItemBtn;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *rightBackView;

@property (weak, nonatomic) IBOutlet UIButton *scanBtn;
@property (weak, nonatomic) IBOutlet UIButton *groupBtn;
@property (weak, nonatomic) IBOutlet UIButton *pan;
@property (weak, nonatomic) IBOutlet UIButton *email;
@property (weak, nonatomic) IBOutlet UIButton *friend;

@end

@implementation MessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self initializeUserInterface];
}
- (void)initializeUserInterface{
    self.title = @"OA办公系统";
    //红角标消失
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    _rightItemView.hidden = YES;
    _rightBackView.hidden = _rightItemView.hidden;
    _rightItemBtn.hidden = _rightItemView.hidden;
    self.view.backgroundColor = [UIColor greenColor];
    _tableView.tableFooterView = [UIView new];
   _tableView.delegate = self;

 
    [_scanBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 36)];
    [_scanBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 36, 0, 0)];
    
    [_groupBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 24)];
    [_groupBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 24, 0, 0)];
    
    [_friend setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 24)];
    [_friend setTitleEdgeInsets:UIEdgeInsetsMake(0, 24, 0, 0)];
    
    [_pan setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 24)];
    [_pan setTitleEdgeInsets:UIEdgeInsetsMake(0, 24, 0, 0)];
    
    [_email setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 24)];
    [_email setTitleEdgeInsets:UIEdgeInsetsMake(0, 24, 0, 0)];

}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _rightItemView.hidden = YES;
    _rightBackView.hidden = _rightItemView.hidden;
    _rightItemBtn.hidden = _rightItemView.hidden;
}
- (void)viewWillDisappear:(BOOL)animated{
    _rightItemView.hidden = NO;
    _rightBackView.hidden = _rightItemView.hidden;
    _rightItemBtn.hidden = _rightItemView.hidden;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark —————— 

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MessageTableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"MessageTableViewCell"];
    if (!cell) {
        cell = [[MessageTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MessageTableViewCell"];
    }
    if (indexPath.row == 2) {
        cell.countBtn.selected  = YES;
        [cell.countBtn setTitle:@"99+" forState:UIControlStateNormal];
    }
    if (cell.countBtn.selected == YES) {
        cell.widthOfBtn.constant = 40;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    MessageDetailViewController *detailVc = Y_storyBoard_id(@"MessageDetailViewController");
    [self.navigationController pushViewController:detailVc animated:YES
    ];
}


//#pragma mark ——————rightItem按钮"
//- (IBAction)rightItem:(UIBarButtonItem *)sender {//右按钮
//     NSLog(@"rightItem按钮");
//    _rightItemView.hidden = !_rightItemView.hidden;
//     _rightBackView.hidden = _rightItemView.hidden;
//    _rightItemBtn.hidden = _rightItemView.hidden;
//}
//- (IBAction)rightItemGrayBtn:(UIButton *)sender {//灰色办透明隐藏
//    _rightItemView.hidden = !_rightItemView.hidden;
//    _rightBackView.hidden = _rightItemView.hidden;
//    _rightItemBtn.hidden = _rightItemView.hidden;
//}
//#pragma mark —————— 扫一扫
//- (IBAction)scan:(UIButton *)sender {
//     NSLog(@"扫一扫");
//    SZQRCodeViewController *vc = [[SZQRCodeViewController alloc] init];
//    vc.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:vc animated:YES];
//}
//
//#pragma mark —————— 群聊
//- (IBAction)group:(UIButton *)sender {
//     NSLog(@"群聊");
//}
//
//#pragma mark —————— 朋友
//- (IBAction)friend:(UIButton *)sender {
//     NSLog(@"朋友");
//    AddFriendsViewController * addFriendsVc = Y_storyBoard_id(@"AddFriendsViewController");
//    [self.navigationController pushViewController:addFriendsVc animated:YES];
//}
//
//#pragma mark —————— 云盘
//- (IBAction)pan:(UIButton *)sender {
//     NSLog(@"云盘");
//}
//
//#pragma mark —————— 邮件
//- (IBAction)email:(id)sender {
//     NSLog(@"邮件");
//    EmailViewController *emailVc = Y_storyBoard_id(@"EmailViewController");
//    [self.navigationController pushViewController:emailVc animated:YES];
//}

@end
