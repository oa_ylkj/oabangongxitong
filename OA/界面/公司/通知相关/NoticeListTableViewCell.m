//
//  NoticeListTableViewCell.m
//  OA
//
//  Created by yy on 17/6/23.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "NoticeListTableViewCell.h"

@implementation NoticeListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setDic:(NSDictionary *)dic{
    _dic = dic;
    _nameL.text = _dic[@"recorder"];
    _timeL.text = [[NSString stringWithFormat:@"%@",_dic[@"createtime"]] substringWithRange:NSMakeRange(0, 16)];
    _titleL.text = [_dic objectForKey:@"name"];
    _messageL.text = [_dic objectForKey:@"typename"];
    if ([NSString stringWithFormat:@"%@",_dic[@"recorder"]].length>0 &&[NSString stringWithFormat:@"%@",_dic[@"recorder"]].length<=2) {

        _imgL.text = _dic[@"recorder"];
    }else if ([NSString stringWithFormat:@"%@",_dic[@"recorder"]].length>2){
        _imgL.text = [[NSString stringWithFormat:@"%@",_dic[@"recorder"]]substringWithRange:NSMakeRange([NSString stringWithFormat:@"%@",_dic[@"recorder"]].length-2, 2)];
    }
    
}
/**        {
 createtime = "2017-06-16 08:55:56.0";
 "effective_time" = "2017-06-16 00:00:00.0";
 filename = "";
 "finish_time" = "2017-06-16 00:00:00.0";
 id = 00000006;
 name = "关于周末加班的公告";
 recorder = "系统管理员";
 typename = "系统公告";
 },
 {
 createtime = "2017-06-15 14:32:11.0";
 "effective_time" = "2017-06-15 00:00:00.0";
 filename = "";
 "finish_time" = "2017-06-30 00:00:00.0";
 id = 00000005;
 name = "今日新闻";
 recorder = "犀利";
 typename = "新闻";
 }*/
@end
