//
//  AddNoticeViewController.m
//  OA
//
//  Created by yy on 17/6/23.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "AddNoticeViewController.h"

@interface AddNoticeViewController ()<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *titleTextView;

@property (weak, nonatomic) IBOutlet UILabel *titlePlaceHolderL;
@property (weak, nonatomic) IBOutlet UITextView *contentTextView;
@property (weak, nonatomic) IBOutlet UILabel *placeholderL;

@end

@implementation AddNoticeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"新增通知";
    [self initOfDataSouce];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)initOfDataSouce{
    _titleTextView.text = @"";
    _contentTextView.text = @"";
    _contentTextView.delegate  = self;
    _titleTextView.delegate  = self;
}

-(void)textViewDidChange:(UITextView *)textView{
    if (textView == _titleTextView) {
        if (textView.text.length == 0) {
            _titlePlaceHolderL.hidden  = NO;
        }else{
            _titlePlaceHolderL.hidden  = YES;
        }
    }else{
        if (textView.text.length == 0) {
            _placeholderL.hidden  = NO;
        }else{
            _placeholderL.hidden  = YES;
        }
    }
    
}

#pragma mark —————— 保存
- (IBAction)saveBtnAction:(UIButton *)sender {
    NSMutableDictionary *parm = [NSMutableDictionary dictionary];
    [parm setObject:_titleTextView.text forKey:@"name"];
    [parm setObject:_contentTextView.text forKey:@"content"];
    [[ToolOfNetWork sharedTools]YrequestURL:oa_saveNotice withParams:parm finished:^(id responsObject, NSError *error) {
        if (_Success) {
            Y_MBP_Save_Success
            _ref();
            [self.navigationController popViewControllerAnimated:YES];
            //刷新上级界面
        }else{
            Y_MBP_Save_Error
        }
    }];
}
- (void)refreshAction:(RefreshNotice)refresh{
    _ref = refresh;
}
@end
