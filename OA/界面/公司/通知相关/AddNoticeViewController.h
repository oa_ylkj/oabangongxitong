//
//  AddNoticeViewController.h
//  OA
//
//  Created by yy on 17/6/23.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^RefreshNotice)();
@interface AddNoticeViewController : UIViewController

@property (nonatomic,copy)RefreshNotice ref;
- (void)refreshAction:(RefreshNotice)refresh;
@end
