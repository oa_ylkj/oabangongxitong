//
//  NoticeViewController.m
//  OA
//
//  Created by yy on 17/6/23.
//  Copyright © 2017年 yy. All rights reserved.
//
#import "NoticeViewController.h"
#import "NoticeListTableViewCell.h"
#import "AddNoticeViewController.h"
#import "NoticeDetailViewController.h"

@interface NoticeViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *wdBtn;//未读
@property (weak, nonatomic) IBOutlet UIButton *ydBtn;//已读
@property (weak, nonatomic) IBOutlet UIImageView *headerBackimg;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *arrOfDataSource;
@property (nonatomic,assign) int pageNo;

@end

@implementation NoticeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"通知";
     Y_tableViewMj_header([weakSelf initOfDataSouce];)
     Y_tableViewMj_footer([weakSelf getNewData];)
    _arrOfDataSource = [NSMutableArray array];
    [self initializeUserInterface];
    [self initOfDataSouce];
    
}

-(void)viewWillAppear: (BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
}
- (void)initializeUserInterface{
    _wdBtn.selected = YES;//未读
    _ydBtn.selected = !_wdBtn.selected;
    [self backImg];
    _tableView.delegate = self;
    _tableView.tableFooterView = [UIView new];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)wdBtnAction:(UIButton *)sender {
    _wdBtn.selected = !_wdBtn.selected;
    _ydBtn.selected = !_wdBtn.selected;
    [self backImg];
    
}
- (IBAction)ydBtnAction:(UIButton *)sender {
    _ydBtn.selected = !_ydBtn.selected;
    _wdBtn.selected = !_ydBtn.selected;
    [self backImg];
}
- (void)backImg{
    if (_ydBtn.selected) {
        _headerBackimg.image = Y_IMAGE(@"已读背景");
    }else{
        _headerBackimg.image = Y_IMAGE(@"未读背景");
    }
    [self initOfDataSouce];
}
- (IBAction)addNotice:(UIBarButtonItem *)sender {
     NSLog(@"新添任务");
    AddNoticeViewController *addNot = VCInSB(@"AddNoticeViewController", @"SecondViewControllers");
    [addNot refreshAction:^{
        [_tableView.mj_header beginRefreshing];
    }];
    
    [self.navigationController pushViewController:addNot animated:YES];
   
}

- (void)initOfDataSouce{
    _pageNo = 1;
    if (_arrOfDataSource.count) {
        [_arrOfDataSource removeAllObjects];
    }
    NSString *pageNo = [NSString stringWithFormat:@"%d",_pageNo];
    NSString *pageSize = [NSString stringWithFormat:@"%d",Y_pageSize];
    NSMutableDictionary *parm = [NSMutableDictionary dictionaryWithObjectsAndKeys: pageNo,@"pageNo",pageSize, @"pageSize",nil];

    if (_wdBtn.selected == YES) {//未读
        [parm setObject:@"0" forKey:@"status"];//已读1 未读0
    }else{
        [parm setObject:@"1" forKey:@"status"];//已读1 未读0

    }
    
    [[ToolOfNetWork sharedTools]YrequestURL:oa_noticegetList withParams:parm finished:^(id responsObject, NSError *error) {
        Y_headerEndRefreshing
        Y_footerEndRefreshing
        if (_Success) {
            _arrOfDataSource = [NSMutableArray arrayWithArray:responsObject[@"data"]];
            if (_arrOfDataSource.count>0) {
                _pageNo+=1;
            }
            _tableView.mj_footer.hidden = (_arrOfDataSource.count == 0);
            [_tableView reloadData];
        }else{
            Y_MBP_ErrMessage
            [_tableView reloadData];
        }
    }];
}

- (void)getNewData{
    NSString *pageNo = [NSString stringWithFormat:@"%d",_pageNo];
    NSString *pageSize = [NSString stringWithFormat:@"%d",Y_pageSize];
    NSMutableDictionary *parm = [NSMutableDictionary dictionaryWithObjectsAndKeys: pageNo,@"pageNo",pageSize, @"pageSize",nil];
    
    if (_wdBtn.selected == YES) {//未读
        [parm setObject:@"0" forKey:@"status"];//已读1 未读0
    }else{
        [parm setObject:@"1" forKey:@"status"];//已读1 未读0
        
    }
    [[ToolOfNetWork sharedTools]YrequestURL:oa_noticegetList withParams:parm finished:^(id responsObject, NSError *error) {
        Y_headerEndRefreshing
        Y_footerEndRefreshing
        if (_Success) {
            NSArray *arr = [NSArray arrayWithArray:responsObject[@"data"]];
            if (arr.count>0) {
                _pageNo += 1;
            }
            [_arrOfDataSource addObjectsFromArray:responsObject[@"data"]];

            [_tableView reloadData];
        }else{
            Y_MBP_Error
        }
    }];
   
}
#pragma mark —————— UITableViewDataSource,UITableViewDelegate>
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arrOfDataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NoticeListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NoticeListTableViewCell"];
    if (!cell) {
        cell = [[NoticeListTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NoticeListTableViewCell"];
    }
    cell.dic = _arrOfDataSource[indexPath.row];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
   
    NoticeDetailViewController *detail = VCInSB(@"NoticeDetailViewController", @"SecondViewControllers");
    [detail refreshAction:^{
        [_tableView.mj_header beginRefreshing];
    }];
    detail.idOfdetail = _arrOfDataSource[indexPath.row][@"id"];
    [self.navigationController pushViewController:detail animated:YES];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
   
}

#pragma mark —————— 
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //方法实现后，默认实现手势滑动删除的方法
    
    if (editingStyle!=UITableViewCellEditingStyleDelete) {
         NSLog(@"删除");
        
        return ;
        
    }else{
         NSLog(@"非删除 ");
        
    }
}

#pragma mark 在滑动手势删除某一行的时候，显示出更多的按钮

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath

{
    if (_wdBtn.selected) {
       
        UITableViewRowAction *deleteRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"删除"handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
            
            NSLog(@"点击了删除");
             [self delNotice:indexPath];
            
        }];

        return @[deleteRowAction];
    }else{
        UITableViewRowAction *bjBtn = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"标记为未读" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
            NSLog(@"标记");
            [self getYiDuWeiDuType:indexPath];
           
        }];
        UITableViewRowAction *deleteRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"删除"handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
            
            NSLog(@"点击了删除");
             [self delNotice:indexPath];
           
        }];
        
        return @[deleteRowAction,bjBtn];
    }
}


- (void)getYiDuWeiDuType:(NSIndexPath*)indexPath{
    
//    NSMutableDictionary *parm = [NSMutableDictionary dictionaryWithObject:@"0" forKey:@"status"];//设为未读
    NSMutableDictionary *parm = [NSMutableDictionary dictionary];
    [parm setObject:_arrOfDataSource[indexPath.row][@"id"] forKey:@"id"];
    [[ToolOfNetWork sharedTools]YrequestURL:oa_noticeUpdateNoticeStatus withParams:parm finished:^(id responsObject, NSError *error) {
        if (_Success) {
            [MBProgressHUD showSuccess:@"设为未读成功"];
            [self  initOfDataSouce];
        }else{
            [MBProgressHUD showError:@"设为未读失败"];
        }
    }];
    
}

- (void)delNotice:(NSIndexPath*)indexPath{
    NSMutableDictionary *parm = [NSMutableDictionary dictionary];//
    [parm setObject:_arrOfDataSource[indexPath.row][@"id"] forKey:@"id"];
    [[ToolOfNetWork sharedTools]YrequestURL:oa_delNotice withParams:parm finished:^(id responsObject, NSError *error) {
        if (_Success) {
            [MBProgressHUD showSuccess:@"删除成功"];
            [self  initOfDataSouce];
        }else{
            [MBProgressHUD showError:@"删除失败"];
        }
    }];
}
@end
