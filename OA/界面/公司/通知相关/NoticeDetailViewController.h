//
//  NoticeDetailViewController.h
//  OA
//
//  Created by yy on 17/6/23.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^RefreshNoticeDetailVc)();
@interface NoticeDetailViewController : UIViewController
@property (nonatomic,copy)NSString *idOfdetail;
@property (nonatomic,copy)RefreshNoticeDetailVc ref;
- (void)refreshAction:(RefreshNoticeDetailVc)refresh;
@end
