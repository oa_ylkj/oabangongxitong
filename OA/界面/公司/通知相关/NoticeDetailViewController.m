//
//  NoticeDetailViewController.m
//  OA
//
//  Created by yy on 17/6/23.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "NoticeDetailViewController.h"

@interface NoticeDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *personL;
@property (weak, nonatomic) IBOutlet UILabel *typeL;
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *detailL;
@property (weak, nonatomic) IBOutlet UITextView *contentTextView;

@property (nonatomic,strong)NSMutableDictionary *dicOfData;
@end

@implementation NoticeDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initOfDataSouce];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initOfDataSouce{

    NSMutableDictionary *parm = [NSMutableDictionary dictionaryWithObject:_idOfdetail forKey:@"id"];
    [[ToolOfNetWork sharedTools]YrequestURL:oa_getNoticeDetail withParams:parm finished:^(id responsObject, NSError *error) {
        if (_Success) {
            Y_MBP_Success
             NSLog(@"%@",responsObject[@"data"]);
            _dicOfData = [NSMutableDictionary dictionaryWithDictionary:responsObject[@"data"]];
            [self showData];
            if (_ref!=nil) {
                _ref();
            }
            
//            if ([[_dicOfData objectForKey:@"status"] intValue] == 0) {
//               [self getYiDuType];
//            }
            
            
        }else{
            Y_MBP_Error
        }
    }];
}
- (void)showData{
    
    _personL.text = [_dicOfData objectForKey:@"recorder"];
    _typeL.text = [_dicOfData objectForKey:@"typename"];
    _titleL.text = [_dicOfData objectForKey:@"name"];
    _detailL.text = [_dicOfData objectForKey:@"name"];
    _contentTextView.text = [_dicOfData objectForKey:@"content"];
}
//- (void)getYiDuType{
//    NSMutableDictionary *parm = [NSMutableDictionary dictionaryWithObject:@"1" forKey:@"status"];
//    [parm setObject:_idOfdetail forKey:@"id"];
//    [[ToolOfNetWork sharedTools]YrequestURL:oa_noticeUpdateNoticeStatus withParams:parm finished:^(id responsObject, NSError *error) {
//        if (_Success) {
//            _ref();
//        }
//    }];
//    
//}

- (void)refreshAction:(RefreshNoticeDetailVc)refresh{
    _ref = refresh;
}
/**  data =     {
 content = "<p>这里有bug，忽略此信息</p>
 ";
 createtime = "2017-06-15 14:32:11.0";
 "effective_time" = "2017-06-15 00:00:00.0";
 filename = "";
 "finish_time" = "2017-06-30 00:00:00.0";
 id = 00000005;
 name = "今日新闻";
 recorder = "犀利";
 status = 1;
 type = 00000077;
 typename = "新闻";
 viewdepart = "";
 viewdepartid = "";
 viewrole = "";
 viewroleid = "";
 viewstaff = "系统管理员;";
 viewstaffid = "admin;";
 warn1 = 1;
 warn2 = 0;
 };*/
@end
