//
//  MoreViewController.m
//  OA
//
//  Created by Admin on 2017/6/14.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "MoreViewController.h"
#import "QiandaoViewController.h"
#import "KaoqinViewController.h"
#import "NoticeViewController.h"
#import "BlogChoiceViewController.h"
#import "LeaveViewController.h"
@interface MoreViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@property (weak, nonatomic) IBOutlet UILabel *shenpi;
@property (weak, nonatomic) IBOutlet UILabel *chuqin;

@end

@implementation MoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.title = @"更多";
    [self getData ];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
 
    
    [self getData ];
}
- (void) getData {//oa_getStatisticsNum
    NSDictionary *dic = @{};
//    Y_Wait
    [[ToolOfNetWork sharedTools] YrequestURL:oa_getStatisticsNum withParams:dic.mutableCopy finished:^(id responsObject, NSError *error) {
//        Y_Hide
        if (_Success) {
            NSLog(@"%@",responsObject[@"data"]);
            NSString *audit = responsObject[@"data"][@"audit"];
            NSString *cq = responsObject[@"data"][@"cq"];
            if (audit == nil || [audit isKindOfClass:[NSNull class]] || audit.length == 0){
                self.shenpi.text = @"0";
            } else {
                self.shenpi.text = responsObject[@"data"][@"audit"];
            }
            if (cq == nil || [cq isKindOfClass:[NSNull class]] || cq.length == 0){
                self.chuqin.text = @"0";
            } else {
                self.chuqin.text = responsObject[@"data"][@"cq"];
            }
            
            
        } else {
//            [MBProgressHUD showError:responsObject[@"message"]];
        }
        NSLog(@"%@",[[responsObject description]kdtk_stringByReplaceingUnicode]);
        NSLog(@"%@",error.localizedDescription);
    }];
}
#pragma mark -- 签到
- (IBAction)qiandao:(id)sender {
    QiandaoViewController *vc = VCInSB(@"QiandaoViewController", @"FirstViewControllers");

}
#pragma mark -- 审批
- (IBAction)shenpi:(id)sender {
    WaitAuditViewController *waitAudiVc  = VCInSB(@"WaitAuditViewController",  @"SecondViewControllers");
    waitAudiVc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:waitAudiVc animated:YES];
}
#pragma mark -- 请假
- (IBAction)qingjia:(id)sender {
    LeaveViewController *vc = VCInSB(@"LeaveViewController", @"FirstViewControllers");
     vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];

}
#pragma mark -- 考勤打卡
- (IBAction)kaoqin:(id)sender {
    KaoqinViewController *vc = VCInSB(@"KaoqinViewController", @"FirstViewControllers");
     vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark -- 日志
- (IBAction)rizhi:(id)sender {
    BlogChoiceViewController *vc = VCInSB(@"BlogChoiceViewController", @"FirstViewControllers");
     vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark -- 通知
- (IBAction)tongzhi:(id)sender {
    NoticeViewController *noticeView = VCInSB(@"NoticeViewController", @"SecondViewControllers");
     noticeView.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:noticeView animated:YES];
}
#pragma mark -- 审核
- (IBAction)shenhe:(id)sender {
    AuditTotalViewController *auditTotalVC = VCInSB(@"AuditTotalViewController", @"SecondViewControllers");
     auditTotalVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:auditTotalVC animated:YES];
    
   
}
#pragma mark -- 邮件
- (IBAction)youjian:(id)sender {
    NSLog(@"邮件");
    EmailViewController *emailVc = Y_storyBoard_id(@"EmailViewController");
    emailVc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:emailVc animated:YES];
}
#pragma mark -- 统计
- (IBAction)tongji:(id)sender {
    TotalStatisticsViewController *totalStatisticsvc = VCInSB(@"TotalStatisticsViewController", @"ThirdViewControllers");
     totalStatisticsvc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:totalStatisticsvc animated:YES];
}
#pragma mark -- 出勤天数
- (IBAction)kaoqinTS:(id)sender {
    TotalStatisticsViewController *totalStatisticsvc = VCInSB(@"TotalStatisticsViewController", @"ThirdViewControllers");
    totalStatisticsvc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:totalStatisticsvc animated:YES];
}
#pragma mark -- 工作计划
- (IBAction)gongzuojihua:(id)sender {
    WorkPlanViewController *workPlanVc = VCInSB(@"WorkPlanViewController", @"ThirdViewControllers");
     workPlanVc.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:workPlanVc animated:YES];
}
#pragma mark -- 运维记录
- (IBAction)yunweijilu:(id)sender {
    MaintenanceViewController *maintenanceVc = VCInSB(@"MaintenanceViewController", @"ThirdViewControllers");
     maintenanceVc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:maintenanceVc animated:YES];
}

@end
