//
//  TotalStatisticsTableViewCell.m
//  OA
//
//  Created by yy on 17/7/4.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "TotalStatisticsTableViewCell.h"

@implementation TotalStatisticsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setDic:(NSDictionary *)dic{
    _dic = dic;
    
    StatisticsOfOneDayModel *model = [StatisticsOfOneDayModel objectWithKeyValues:_dic];
    
    _dateL.text = [model.datenumber substringToIndex:10];
//    _weekL.text = model.week;
    _weekL.text = [self weekName:model.week];
    
}
- (NSString *)weekName:(NSString *)week{
    NSString *str = @"星期";
  
    switch ([week intValue]) {
        case 0:
            return  [NSString stringWithFormat:@"%@天",str];
            break;
        case 1:
            return  [NSString stringWithFormat:@"%@一",str];
            break;
        case 2:
            return  [NSString stringWithFormat:@"%@二",str];
            break;
        case 3:
            return  [NSString stringWithFormat:@"%@三",str];
            break;
        case 4:
            return  [NSString stringWithFormat:@"%@四",str];
            break;
        case 5:
            return  [NSString stringWithFormat:@"%@五",str];
            break;
        case 6:
            return  [NSString stringWithFormat:@"%@六",str];
            break;
        case 7:
            return  [NSString stringWithFormat:@"%@天",str];
            break;
            
        default:
            return @"";
            break;
    }
}

- (void)setDicOfQj:(NSDictionary *)dicOfQj{
    _dicOfQj = dicOfQj;
    
    StatisticsOfOneDayModel *model = [StatisticsOfOneDayModel objectWithKeyValues:_dicOfQj];

    if ([model.numname isEqualToString:@"天"]) {
        _dateL.text = [model.begintime substringToIndex:10];
        _weekL.text = [NSString stringWithFormat:@"%@%@",model.num,model.numname];
    }else{
        _dateL.text = [NSString stringWithFormat:@"%@   %@",[model.begintime substringToIndex:16],[model.endtime substringWithRange:NSMakeRange(11, 5)]];
        _weekL.text = [NSString stringWithFormat:@"%@%@",model.num,model.numname];
    }
    
}

- (void)setDicOfCd:(NSDictionary *)dicOfCd{
    _dicOfCd = dicOfCd;
    StatisticsOfOneDayModel *model = [StatisticsOfOneDayModel objectWithKeyValues:_dicOfCd];
    _dateL.text = [NSString stringWithFormat:@"%@ %@",[model.datenumber substringToIndex:10],[self weekName:model.week]];
//    NSString *h = [[ToolOfBasic timeFormatted:[model.datetimeSet intValue]]substringToIndex:2];
//    NSString *m = [[ToolOfBasic timeFormatted:[model.datetimeSet intValue]]substringFromIndex:3];
    NSString *h = [[ToolOfBasic timeFormatted:[model.num intValue]]substringToIndex:2];
    NSString *m = [[ToolOfBasic timeFormatted:[model.num intValue]]substringFromIndex:3];
    
    if ([h intValue]==0) {
        _weekL.text = [NSString stringWithFormat:@"%@分钟",m];
    }else{
        _weekL.text = [NSString stringWithFormat:@"%@小时%@分钟",h,m];
    }
}


@end
