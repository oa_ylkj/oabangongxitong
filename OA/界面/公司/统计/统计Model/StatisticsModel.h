//
//  StatisticsModel.h
//  OA
//
//  Created by yy on 17/7/25.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StatisticsModel : NSObject
@property (nonatomic,copy)NSString *datenumber;//日期
@property (nonatomic,copy)NSString *earlyTime;//早退时间（秒）
@property (nonatomic,copy)NSString *id;
@property (nonatomic,copy)NSString *lateTime;//迟到时间（秒）
@property (nonatomic,copy)NSString *loginname;
@property (nonatomic,copy)NSString *myUnitId;
@property (nonatomic,copy)NSString *num_avg;//平均工作时间
@property (nonatomic,copy)NSString *num_cc;//出差次数
@property (nonatomic,copy)NSString *num_cd;//迟到次数
@property (nonatomic,copy)NSString *num_cq;//出勤天数
@property (nonatomic,copy)NSString *num_jb;//加班次数
@property (nonatomic,copy)NSString *num_kg;//旷工次数
@property (nonatomic,copy)NSString *num_qj;//请假次数
@property (nonatomic,copy)NSString *num_qk;//缺卡次数
@property (nonatomic,copy)NSString *num_wq;//外勤次数
@property (nonatomic,copy)NSString *num_xx;//休息天数
@property (nonatomic,copy)NSString *num_zt;//早退次数
@property (nonatomic,copy)NSString *username;
@property (nonatomic,copy)NSString *team;

@property (nonatomic,strong)NSArray *attendanceList;//出勤集合
@property (nonatomic,strong)NSArray *earlyList;//早退列表
@property (nonatomic,strong)NSArray *lateList;//迟到列表
@property (nonatomic,strong)NSArray *qkList;//缺卡列表
@property (nonatomic,strong)NSArray *restList;//休息天数
@property (nonatomic,strong)NSArray *wqList;//外勤列表
@property (nonatomic,strong)NSArray *qjList;//请假
@property (nonatomic,strong)NSArray *kgList;//旷工列表
@property (nonatomic,strong)NSArray *jbList;//加班
@property (nonatomic,strong)NSArray *ccList;//出差列表
 

@property (nonatomic,copy)NSString *beginworktime;//入职时间
/**
 attendanceList =         (
 {
 datenumber = "2017-7-21";
 id = 1;
 week = "星期五";
 }
 );
 datenumber = "2017-07-01 00:00:00.0";
 earlyList =         (
 {
 datenumber = "2017-7-21";
 datetimeSet = "2017-07-21 08:30:00";
 id = 1;
 remarks = 1;
 week = "星期五";
 }
 );
 earlyTime = 0;
 id = "4e6349e7-6c52-11e7-b580-00163e0431ce";
 kgList =         (
 {
 datenumber = "2017-7-21";
 id = 1;
 week = "星期五";
 }
 );
 lateList =         (
 {
 datenumber = "2017-7-21";
 datetimeSet = "2017-07-21 08:30:00";
 id = 1;
 remarks = 1;
 week = "星期五";
 }
 );
 lateTime = 453780;
 loginname = dddddd;
 myUnitId = 00000000;
 "num_avg" = "35520.0000";
 "num_cc" = 0;
 "num_cd" = 10;
 "num_cq" = 23;
 "num_jb" = 0;
 "num_kg" = 0;
 "num_qj" = 0;
 "num_qk" = 0;
 "num_wq" = 0;
 "num_xx" = 0;
 "num_zt" = 0;
 qjList = "<null>";
 qkList =         (
 {
 datenumber = "2017-7-21";
 datetimeSet = "2017-07-21 08:30:00";
 id = 1;
 remarks = 1;
 week = "星期五";
 }
 );
 restList =         (
 {
 datenumber = "2017-7-21";
 id = 1;
 week = "星期五";
 }
 );
 username = test2;
 wqList =         (
 {
 datenumber = "2017-7-21";
 datetimeSet = "2017-07-21 08:30:00";
 id = 1;
 remarks = 1;
 week = "星期五";
 }
 );
 ;
 */
@end
