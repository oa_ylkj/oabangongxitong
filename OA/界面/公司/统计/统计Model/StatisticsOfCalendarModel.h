//
//  StatisticsOfCalendarModel.h
//  OA
//
//  Created by yy on 17/7/25.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StatisticsOfCalendarModel : NSObject
@property (nonatomic,copy)NSString *beginAddress;
@property (nonatomic,copy)NSString *beginNote;
@property (nonatomic,copy)NSString *beginStatus;
@property (nonatomic,copy)NSString *beginType;
@property (nonatomic,copy)NSString *begintime;
@property (nonatomic,copy)NSString *begintimeSet;
@property (nonatomic,copy)NSString *datenumber;
@property (nonatomic,copy)NSString *earlyTime;
@property (nonatomic,copy)NSString *endAddress;
@property (nonatomic,copy)NSString *endNote;
@property (nonatomic,copy)NSString *endStatus;
@property (nonatomic,copy)NSString *endType;
@property (nonatomic,copy)NSString *endtime;
@property (nonatomic,copy)NSString *endtimeSet;
@property (nonatomic,copy)NSString *id;
@property (nonatomic,copy)NSString *lateTime;
@property (nonatomic,copy)NSString *latitude;
@property (nonatomic,copy)NSString *loginname;
@property (nonatomic,copy)NSString *longitude;
@property (nonatomic,copy)NSString *myUnitId;
@property (nonatomic,copy)NSString *punchpoint;
@property (nonatomic,copy)NSString *range;
@property (nonatomic,copy)NSString *team;
@property (nonatomic,copy)NSString *type;
@property (nonatomic,copy)NSString *username;
@property (nonatomic,copy)NSString *status;

@property (nonatomic,strong)NSArray *statusList;

@property (nonatomic,copy)NSString *dateWeek;//今天星期
@property (nonatomic,copy)NSString *lastMonthDay;//上月天数
@property (nonatomic,copy)NSString *curMonthDay;//本月天数
@property (nonatomic,copy)NSString *curMonthWeek;//本月1号星期

/**data =     {
 beginAddress = "中渝都会首站";
 beginNote = "备注";
 beginStatus = "迟到";
 beginType = 0;
 begintime = "2017-07-25 08:35:00.0";
 begintimeSet = "2017-07-25 08:00:00.0";
 datenumber = "2017-07-25 00:00:00.0";
 earlyTime = 0;
 endAddress = "中渝都会首站";
 endNote = "备注";
 endStatus = "正常";
 endType = 0;
 endtime = "2017-07-25 18:03:00.0";
 endtimeSet = "2017-07-25 18:00:00.0";
 id = "5a4ece73-70d2-11e7-b580-00163e0431ce";
 lateTime = 300000;
 latitude = "<null>";
 loginname = dddddd;
 longitude = "<null>";
 myUnitId = 00000000;
 punchpoint = "<null>";
 range = "<null>";
 statusList =         (
 {
 datenumber = "2017-07-20 00:00:00.0";
 id = "6cd51f08-6d30-11e7-b580-00163e0431ce";
 status = "正常";
 },
 {
 datenumber = "2017-07-21 00:00:00.0";
 id = "f2597bd0-6dc0-11e7-b580-00163e0431ce";
 status = "其它";
 },
 {
 datenumber = "2017-07-22 00:00:00.0";
 id = "ab255fe6-6eac-11e7-b580-00163e0431ce";
 status = "其它";
 },
 {
 datenumber = "2017-07-24 00:00:00.0";
 id = "fe4a442e-7012-11e7-b580-00163e0431ce";
 status = "其它";
 },
 {
 datenumber = "2017-07-25 00:00:00.0";
 id = "5a4ece73-70d2-11e7-b580-00163e0431ce";
 status = "其它";
 }
 );
 team = "夏季";
 type = "上班";
 username = test2;
 };
 }*/
@end
