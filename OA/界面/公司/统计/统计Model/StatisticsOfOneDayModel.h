//
//  StatisticsOfOneDayModel.h
//  OA
//
//  Created by yy on 17/7/25.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StatisticsOfOneDayModel : NSObject
@property (nonatomic,copy)NSString *datenumber;
@property (nonatomic,copy)NSString *datetimeSet;
@property (nonatomic,copy)NSString *id;
@property (nonatomic,copy)NSString *remarks;//备注内容时间等
@property (nonatomic,copy)NSString *week;

@property (nonatomic,copy)NSString *status;

@property (nonatomic,copy)NSString *begintime;
@property (nonatomic,copy)NSString *endtime;
@property (nonatomic,copy)NSString *num;
@property (nonatomic,copy)NSString *numname;


/**qjList =     (
 {
 begintime = "2017-07-21 08:30:00";
 endtime = "2017-07-21 18:00:00";
 id = 1;
 num = 1;
 numname = "天";
 }
 );*/
/**datenumber = "2017-7-21";
 datetimeSet = "2017-07-21 08:30:00";
 id = 1;
 remarks = 1;
 week = "星期五";
*/
@end
