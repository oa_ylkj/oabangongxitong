//
//  TotalStatisticsTableViewCell.h
//  OA
//
//  Created by yy on 17/7/4.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TotalStatisticsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dateL;
@property (weak, nonatomic) IBOutlet UILabel *weekL;

@property (nonatomic,strong)NSDictionary *dic;
@property (nonatomic,strong)NSDictionary *dicOfQj;
@property (nonatomic,strong)NSDictionary *dicOfCd;
@end
