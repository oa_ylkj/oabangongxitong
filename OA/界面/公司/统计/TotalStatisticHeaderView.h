//
//  TotalStatisticHeaderView.h
//  OA
//
//  Created by yy on 17/7/4.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TotalStatisticHeaderView : UIView
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *detailL;
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UIButton *scalingBtn;

@end
