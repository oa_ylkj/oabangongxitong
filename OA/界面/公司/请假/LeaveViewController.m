//
//  LeaveViewController.m
//  OA
//
//  Created by Admin on 2017/7/19.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "LeaveViewController.h"
#import "csCell.h"
#import "csAddCell.h"
#import "shCell.h"
#import "shAddCell.h"
@interface LeaveViewController ()<TimeChouseDelegate,RegionDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic ,strong) NSMutableArray *csArr;
@property (nonatomic ,strong) NSMutableArray *shArr;
@property (weak, nonatomic) IBOutlet UILabel *startTime;
@property (weak, nonatomic) IBOutlet UILabel *endTime;
@property (weak, nonatomic) IBOutlet UILabel *type;
@property (weak, nonatomic) IBOutlet UITextView *reason;
@property (weak, nonatomic) IBOutlet UICollectionView *csCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *shCollectionView;

@property (weak, nonatomic) IBOutlet UITextField *num;

@property (nonatomic ,strong) BasisChooseTimeView *viewOfData;
@property (nonatomic ,strong) BasisChooseRegionView *dateMonthView;
@property (nonatomic ,strong) UILabel *choiceLabel;
@end

@implementation LeaveViewController
//婚假、事假、孕假、丧假、病假
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"请假申请";
    self.csArr = @[].mutableCopy;
    self.shArr = @[].mutableCopy;
    self.num.borderStyle = UITextBorderStyleLine;
    self.num.layer.borderWidth = 1;
    self.num.layer.borderColor = [UIColor whiteColor].CGColor;
    
//    UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
//    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
//    layout.minimumInteritemSpacing = 0.1;
//    [layout setItemSize:CGSizeMake(48, 48)];
//    self.csCollectionView.collectionViewLayout = layout;
//    self.shCollectionView.collectionViewLayout = layout;
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear: (BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)start:(id)sender {
    self.choiceLabel = self.startTime;
    [self.view addSubview:self.viewOfData];
}
- (IBAction)end:(id)sender {
    self.choiceLabel = self.endTime;
    [self.view addSubview:self.viewOfData];
}
- (IBAction)ty:(id)sender {
    self.choiceLabel = self.type;
    [self.view addSubview:self.dateMonthView];
}

//- (IBAction)csr:(id)sender {
//    BumenChoiceViewController *vc = VCInSB(@"BumenChoiceViewController", @"BumenViewController");
//    vc.type = @"1";
//    vc.vc = self;
//    vc.csshArr = self.csArr;
//    vc.back = ^() {
//        [self.csArr removeAllObjects];
//        [self.csArr addObjectsFromArray: [ShareUserInfo sharedUserInfo].csArr];
//        [self.csCollectionView reloadData];
//    };
//    [self.navigationController pushViewController:vc animated:YES];
//}
//- (IBAction)shr:(id)sender {
//    BumenChoiceViewController *vc = VCInSB(@"BumenChoiceViewController", @"BumenViewController");
//    vc.type = @"1";
//    vc.vc = self;
//    vc.csshArr = self.shArr;
//    vc.back = ^() {
//        if ([ShareUserInfo sharedUserInfo].csArr.count > 0) {
//            [self.shArr removeAllObjects];
//            [self.shArr addObject:[ShareUserInfo sharedUserInfo].csArr[0]];
//            [self.shCollectionView reloadData];
//        }
//        
//    };
//    [self.navigationController pushViewController:vc animated:YES];
//}
- (IBAction)send:(id)sender {
    NSString *begin = self.startTime.text;
    NSString *end = self.endTime.text;
    NSString *type = self.type.text;
    NSString *reason = self.reason.text;
    NSString *num = [self.num.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
    if (begin.length == 0) {
        [MBProgressHUD showError:@"请选择开始时间"];
        return;
    }
    if (end.length == 0) {
        [MBProgressHUD showError:@"请选择结束时间"];
        return;
    }
    if (!([begin compare:end] < 0)) {
        [MBProgressHUD showError:@"结束时间必须大于开始时间"];
        return;
    }
    if (type.length == 0) {
        [MBProgressHUD showError:@"请选择请假类型"];
        return;
    }
    if(num.length > 0 || self.num.text.length == 0) {
        [MBProgressHUD showError:@"请输入正确的天数"];
        return;
    }
    if (reason.length == 0) {
        [MBProgressHUD showError:@"请填写请假理由"];
        return;
    }
  
    
//    if (self.csArr.count == 0) {
//        [MBProgressHUD showError:@"请选择抄送人"];
//        return;
//    }
    if (self.shArr.count == 0) {
        [MBProgressHUD showError:@"请选择审核人"];
        return;
    }
    NSMutableString *csId = @"".mutableCopy;
    NSMutableString *csName = @"".mutableCopy;
    NSMutableString *shId = @"".mutableCopy;
    NSMutableString *shName = @"".mutableCopy;
    for (NSDictionary *dic in self.csArr){
        [csId appendFormat:@"%@",dic[@"id"]];
        [csId appendFormat:@";"];
        [csName appendFormat:@"%@",dic[@"name"]];
        [csName appendFormat:@";"];
    }
    for (NSDictionary *dic in self.shArr){
        [shId appendFormat:@"%@",dic[@"id"]];
//        [shId appendFormat:@";"];
        [shName appendFormat:@"%@",dic[@"name"]];
//        [shName appendFormat:@";"];
    }
    NSMutableDictionary *dic = @{}.mutableCopy;
    [dic setObject:begin forKey:@"begintime"];
    [dic setObject:end forKey:@"endtime"];
    [dic setObject:type forKey:@"leaveType"];
    [dic setObject:self.num.text forKey:@"num"];
    [dic setObject:@"天" forKey:@"numname"];
    [dic setObject:reason forKey:@"reason"];
    [dic setObject:self.shArr[0][@"id"] forKey:@"auditors"];
    [dic setObject:self.shArr[0][@"name"] forKey:@"auditorsName"];
    [dic setObject:csId forKey:@"lookers"];
    [dic setObject:csName forKey:@"lookersName"];
    
    Y_Wait
    [[ToolOfNetWork sharedTools] YrequestURL:oa_insertAttendanceAuditByLeave withParams:dic.mutableCopy finished:^(id responsObject, NSError *error) {
        Y_Hide
        if (_Success) {
            [MBProgressHUD showSuccess:@"申请成功"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
            
        } else {
            [MBProgressHUD showError:responsObject[@"message"]];
        }
        NSLog(@"%@",[[responsObject description]kdtk_stringByReplaceingUnicode]);
        NSLog(@"%@",error.localizedDescription);
    }];
    
}

#pragma mark --- UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == self.csCollectionView) {
        return self.csArr.count + 1;
    } else {
        return self.shArr.count + 1;
    }
    
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if(collectionView == self.csCollectionView) {
        if (indexPath.row < self.csArr.count) {
            csCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"csCell" forIndexPath:indexPath];
            cell.image.backgroundColor = [UIColor getColorWithHexString:@"FEA100"];
            cell.image.layer.cornerRadius = 20;
            cell.image.clipsToBounds = YES;
            
            NSString *nameStr = self.csArr[indexPath.row][@"name"];
            
            if (nameStr.length <3) {
                cell.name.text = nameStr;
            } else {
                cell.name.text = [nameStr substringWithRange:NSMakeRange(nameStr.length - 2, 2)];
            }
            return cell;
        } else {
            csAddCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"csAddCell" forIndexPath:indexPath];
            return cell;
        }
    } else {
        if (indexPath.row < self.shArr.count) {
            shCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"shCell" forIndexPath:indexPath];
            cell.image.backgroundColor = [UIColor getColorWithHexString:@"FEA100"];
            cell.image.layer.cornerRadius = 20;
            cell.image.clipsToBounds = YES;
            
            NSString *nameStr = self.shArr[indexPath.row][@"name"];
            
            if (nameStr.length <3) {
                cell.name.text = nameStr;
            } else {
                cell.name.text = [nameStr substringWithRange:NSMakeRange(nameStr.length - 2, 2)];
            }
            return cell;
        } else {
            shAddCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"shAddCell" forIndexPath:indexPath];
            return cell;
        }
    }
    
}

#pragma mark --- UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.csCollectionView) {
        if (indexPath.row < self.csArr.count) {
            [self.csArr removeObjectAtIndex:indexPath.row];
            [self.csCollectionView reloadData];
        } else {
            BumenChoiceViewController *vc = VCInSB(@"BumenChoiceViewController", @"BumenViewController");
            vc.type = @"1";
            vc.vc = self;
            vc.csshArr = self.csArr;
            vc.back = ^() {
                if ([ShareUserInfo sharedUserInfo].csArr.count > 0) {
                    [self.csArr removeAllObjects];
                    [self.csArr addObjectsFromArray:    [ShareUserInfo sharedUserInfo].csArr];
                    [self.csCollectionView reloadData];
                }
            };
            [self.navigationController pushViewController:vc animated:YES];
            
        }
    } else {
        if (indexPath.row < self.shArr.count) {
            [self.shArr removeObjectAtIndex:indexPath.row];
            [self.shCollectionView reloadData];
        } else {
            BumenChoiceViewController *vc = VCInSB(@"BumenChoiceViewController", @"BumenViewController");
            vc.type = @"1";
            vc.vc = self;
            vc.csshArr = self.shArr;
            vc.back = ^() {
                if ([ShareUserInfo sharedUserInfo].csArr.count > 0) {
                    [self.shArr removeAllObjects];
                    [self.shArr addObject:[ShareUserInfo sharedUserInfo].csArr[0]];
                    [self.shCollectionView reloadData];
                }
            };
            [self.navigationController pushViewController:vc animated:YES];
            
        }
    }
   
    
    
}

- (NSString *)dateOfDidSelect:(NSString *)str{
    self.choiceLabel.text = str;
    [_viewOfData removeFromSuperview];
    return str;
}

- (void)removeBasisChooseTimeView {
    [_viewOfData removeFromSuperview];
}

- (NSString *)regionOfdidSelect:(NSString *)strOfArea {
    self.type.text = strOfArea;
    [_dateMonthView removeFromSuperview];
    return strOfArea;
}
- (void)removeRegionView {
    [_dateMonthView removeFromSuperview];
}
-(UIView *)viewOfData{
    if (!_viewOfData) {
        
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"BasisChooseTimeView" owner:self options:nil];
        _viewOfData = (BasisChooseTimeView *)[nib objectAtIndex:0];
        _viewOfData.frame = CGRectMake(30, 30, Y_mainW-60, 280);
        _viewOfData.center = self.view.center;
        _viewOfData.layer.cornerRadius = 20;
        _viewOfData.layer.borderWidth = 0.2;
        _viewOfData.delegateOfTime = self;
        _viewOfData.isPrecision = YES;
        
    }
    return _viewOfData;
}
- (BasisChooseRegionView *)dateMonthView{
    if (!_dateMonthView) {
        _dateMonthView = [[[NSBundle mainBundle]loadNibNamed:@"BasisChooseRegionView" owner:self options:nil]objectAtIndex:0];
        _dateMonthView.frame = CGRectMake(30, 30, Y_mainW-60, 280);
        _dateMonthView.center = self.view.center;
        _dateMonthView.layer.cornerRadius = 20;
        _dateMonthView.layer.borderWidth = 0.2;
        _dateMonthView.delegateOfRegion = self;
        _dateMonthView.arrOfData = @[@"婚假",@"事假",@"孕假",@"丧假",@"病假"];
        
    }
    return _dateMonthView;
}
@end
