//
//  SeeTheLogViewController.h
//  OA
//
//  Created by Admin on 2017/7/14.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SeeTheLogViewController : UIViewController
@property (nonatomic ,strong) NSString *type; // 1 为查询自己的日志列表 0 为查询公司的所有日志

@property (nonatomic ,strong) NSString *hide; // 1 从我的界面进入我的日志需影藏写日志和看日志选择栏 0 正常显示
@end
