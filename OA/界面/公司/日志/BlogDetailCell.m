//
//  BlogDetailCell.m
//  OA
//
//  Created by Admin on 2017/8/8.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "BlogDetailCell.h"

@implementation BlogDetailCell

+ (instancetype)BlogDetailCell:(UITableView *)tableView{
    
    BlogDetailCell *cell =[tableView dequeueReusableCellWithIdentifier:@"BlogDetailCell"];
    
    if(cell==nil)
    {
        cell = [[BlogDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"BlogCell"];
        
    }
    cell.headimg.layer.cornerRadius = 20;
    cell.headimg.clipsToBounds = YES;
    return cell;
}


@end
