//
//  BlogCell.m
//  OA
//
//  Created by Admin on 2017/7/18.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "BlogCell.h"

@implementation BlogCell
+ (instancetype)BlogCell:(UITableView *)tableView{
    
    BlogCell *cell =[tableView dequeueReusableCellWithIdentifier:@"BlogCell"];
    
    if(cell==nil)
    {
        cell = [[BlogCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"BlogCell"];
        
    }
    cell.headImg.layer.cornerRadius = 20;
    cell.headImg.clipsToBounds = YES;
    return cell;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
