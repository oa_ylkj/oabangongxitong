//
//  SeeBlogCell.h
//  OA
//
//  Created by Admin on 2017/7/31.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SeeBlogCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UILabel *name;

@end
