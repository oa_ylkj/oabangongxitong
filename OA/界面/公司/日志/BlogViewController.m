//
//  BlogViewController.m
//  OA
//
//  Created by Admin on 2017/6/27.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "BlogViewController.h"
#import "BumenChoiceViewController.h"
#import "BlogLxrCell.h"
#import "BlogAddCell.h"
@interface BlogViewController ()<TimeChouseDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UITextField *blogtitle;
@property (weak, nonatomic) IBOutlet UILabel *choiceDate;
@property (nonatomic,strong)BasisChooseTimeView *viewOfDate;
@property (nonatomic ,strong) NSMutableArray *lxrArr;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UITextView *wancheng;
@property (weak, nonatomic) IBOutlet UITextView *weiwancheng;
@property (weak, nonatomic) IBOutlet UITextView *xietiao;
@property (weak, nonatomic) IBOutlet UILabel *type;


@end

@implementation BlogViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"日志";
    self.blogtitle.text = self.logType;
    self.type.text = [NSString stringWithFormat:@"工作%@",self.logType];
    self.blogtitle.borderStyle = UITextBorderStyleLine;
    self.blogtitle.layer.borderWidth = 1;
    self.blogtitle.layer.borderColor = [UIColor whiteColor].CGColor;
    self.lxrArr = @[].mutableCopy;
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设置格式：zzz表示时区
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    //NSDate转NSString
    NSString *currentDateString = [dateFormatter stringFromDate:date];
    self.choiceDate.text = currentDateString;
}

- (IBAction)choiceDate:(id)sender {
    [self.view addSubview:self.viewOfDate];
}
- (IBAction)commit:(id)sender {
    NSMutableString *nameStr = @"".mutableCopy;
    NSMutableString *idStr = @"".mutableCopy;
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设置格式：zzz表示时区
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    //NSDate转NSString
    NSString *currentDateString = [dateFormatter stringFromDate:date];
    for (NSDictionary *dic in self.lxrArr){
        [nameStr appendFormat:@"%@",dic[@"name"]];
        [nameStr appendFormat:@";"];
        [idStr appendFormat:@"%@",dic[@"id"]];
        [idStr appendFormat:@";"];
    }
    NSMutableDictionary *dic = @{}.mutableCopy;
    [dic setObject:@"" forKey:@"id"];
    [dic setObject:self.wancheng.text forKey:@"content"];
    [dic setObject:self.weiwancheng.text forKey:@"unfinished_content"];
    [dic setObject:self.xietiao.text forKey:@"concerted_content"];
    [dic setObject:currentDateString forKey:@"log_data"];
    [dic setObject:@"" forKey:@"name"];
    [dic setObject:self.type.text forKey:@"type"];
    [dic setObject:nameStr forKey:@"toWhom"];
    [dic setObject:idStr forKey:@"toWhomId"];
    Y_Wait
    [[ToolOfNetWork sharedTools] YrequestURL:oa_saveWorklog withParams:dic.mutableCopy finished:^(id responsObject, NSError *error) {
        Y_Hide
        if (_Success) {
            NSLog(@"%@",responsObject[@"data"]);
            [MBProgressHUD showSuccess:@"日志提交成功"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
           
        } else {
            [MBProgressHUD showError:responsObject[@"message"]];
        }
        NSLog(@"%@",[[responsObject description]kdtk_stringByReplaceingUnicode]);
        NSLog(@"%@",error.localizedDescription);
    }];
}

#pragma mark —————— TimeChouseDelegate
-(UIView *)viewOfDate{
    if (!_viewOfDate) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"BasisChooseTimeView" owner:self options:nil];
        _viewOfDate = (BasisChooseTimeView *)[nib objectAtIndex:0];
        _viewOfDate.frame = CGRectMake(30, 30, Y_mainW-60, 280);
        _viewOfDate.center = self.view.center;
        _viewOfDate.layer.cornerRadius = 20;
        _viewOfDate.delegateOfTime = self;
        _viewOfDate.isPrecision = NO;
    }
    return _viewOfDate;
}
- (NSString *)dateOfDidSelect:(NSString *)str{
    NSLog(@"tag = %ld ～ %@" ,(long)_viewOfDate.tag,str);
    self.choiceDate.text = str;
    [_viewOfDate removeFromSuperview];
    return str;
}

- (void)removeBasisChooseTimeView {
    [_viewOfDate removeFromSuperview];
}
#pragma mark --- UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.lxrArr.count + 1;
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < self.lxrArr.count) {
        BlogLxrCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BlogLxrCell" forIndexPath:indexPath];
        cell.image.backgroundColor = [UIColor getColorWithHexString:@"FEA100"];
        cell.image.layer.cornerRadius = 20;
        cell.image.clipsToBounds = YES;
        
        NSString *nameStr = self.lxrArr[indexPath.row][@"name"];
        
        if (nameStr.length <3) {
            cell.name.text = nameStr;
        } else {
            cell.name.text = [nameStr substringWithRange:NSMakeRange(nameStr.length - 2, 2)];
        }
        return cell;
    } else {
        BlogAddCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BlogAddCell" forIndexPath:indexPath];
        return cell;
    }
}

#pragma mark --- UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < self.lxrArr.count) {
        [self.lxrArr removeObjectAtIndex:indexPath.row];
        [self.collectionView reloadData];
    } else {
        BumenChoiceViewController *vc = VCInSB(@"BumenChoiceViewController", @"BumenViewController");
        vc.type = @"0";
        vc.vc = self;
        vc.lxrArr = self.lxrArr;
        vc.back = ^() {
            [self.lxrArr removeAllObjects];
            [self.lxrArr addObjectsFromArray:[ShareUserInfo sharedUserInfo].lxrArr];
            [self.collectionView reloadData];
            NSLog(@"1111");
        };
        [self.navigationController pushViewController:vc animated:YES];

    }
    
    
}

@end
