//
//  BlogDetailViewController.m
//  OA
//
//  Created by Admin on 2017/7/31.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "BlogDetailViewController.h"
#import "SeeBlogCell.h"
#import "BlogDetailCell.h"
@interface BlogDetailViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@property (weak, nonatomic) IBOutlet UILabel *headName;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UITextView *wancheng;
@property (weak, nonatomic) IBOutlet UITextView *weiwancheng;
@property (weak, nonatomic) IBOutlet UITextView *xietiao;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic ,strong) NSArray *lxrArr;
@property (weak, nonatomic) IBOutlet UITextField *liuyanText;
@property (weak, nonatomic) IBOutlet UIView *lyB;
@property (nonatomic ,strong) NSDictionary *data;
@property (nonatomic ,strong) NSArray *dPArr;// 点评列表
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lyHeightLayout;

@property (nonatomic ,assign) CGFloat tablevieH;
@property (nonatomic ,assign) NSInteger numOfCellH;
@end

@implementation BlogDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.liuyanText.borderStyle = UITextBorderStyleLine;
    self.liuyanText.layer.borderWidth = 1;
    self.liuyanText.layer.borderColor = [UIColor getColorWithHexString:@"F9F9F9"].CGColor;
    self.liuyanText.backgroundColor = [UIColor getColorWithHexString:@"F9F9F9"];
    self.numOfCellH = 0;
    self.tableview.userInteractionEnabled = NO;
    self.tablevieH = 0.0;
    NSDictionary *dic = @{@"id":self.blogId};
    Y_Wait
    [[ToolOfNetWork sharedTools] YrequestURL:oa_getWorkLoglog withParams:dic.mutableCopy finished:^(id responsObject, NSError *error) {
        Y_Hide
        if (_Success) {
            NSLog(@"%@",responsObject[@"data"]);
            self.data = responsObject[@"data"];
            self.lxrArr = [responsObject[@"data"][@"readPeople"] componentsSeparatedByString:@";"];
            [self loadUI];
        } else {
            [MBProgressHUD showError:responsObject[@"message"]];
        }
        NSLog(@"%@",[[responsObject description]kdtk_stringByReplaceingUnicode]);
        NSLog(@"%@",error.localizedDescription);
    }];
    [self getLY];
}
- (void)getLY {
    NSDictionary *dic = @{@"worklogid":self.blogId};
    Y_Wait
    [[ToolOfNetWork sharedTools] YrequestURL:oa_getWorklogcommentList withParams:dic.mutableCopy finished:^(id responsObject, NSError *error) {
        Y_Hide
        if (_Success) {
            NSLog(@"%@",responsObject[@"data"]);
            if (responsObject[@"data"] != nil) {
                self.dPArr = responsObject[@"data"];
                self.tablevieH = 0.0;
//                 self.numOfCellH = 0;
//                self.lyHeightLayout.constant = self.dPArr.count * 60;
                [self.tableview reloadData];
            }
//            [self loadLY];
        } else {
            [MBProgressHUD showError:responsObject[@"message"]];
        }
        NSLog(@"%@",[[responsObject description]kdtk_stringByReplaceingUnicode]);
        NSLog(@"%@",error.localizedDescription);
    }];

}
- (void)loadUI {
    self.headImg.layer.cornerRadius = self.headImg.bounds.size.width / 2;
//    self.headImg.backgroundColor = [UIColor getColorWithHexString:@""];
    NSString *recorder = self.data[@"recorder"];
    if (recorder.length > 2) {
        self.headName.text = [recorder substringFromIndex:recorder.length - 2];
    } else {
        self.headName.text = recorder;
    }
    self.userName.text = recorder;
    self.date.text = [self.data[@"log_data"] substringToIndex:16];
    self.wancheng.text = self.data[@"content"];
    self.weiwancheng.text = self.data[@"unfinished_content"];
    self.xietiao.text = self.data[@"concerted_content"];
    [self.collectionView reloadData];
}
- (void)loadLY {
//    CGFloat height = 0.0;
//    for (NSInteger index = 0; index < self.dPArr.count; index ++) {
//        CGSize size = [[self.dPArr objectAtIndex:index][@"content"] boundingRectWithSize:CGSizeMake(315.0 / 375.0 * kScreen_Width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13]} context:nil].size;
//        CGFloat ht = size.height;
//        UIView *view = [[UIView alloc]init];
//        view.frame = CGRectMake(0, 50 * index, Y_mainW, 50);
//        view.backgroundColor = [UIColor whiteColor];
//        
//        
//        UIView *imageView = [[UIView alloc]initWithFrame:CGRectMake(5, 1, 45, 45)];
//        imageView.backgroundColor = [UIColor getColorWithHexString:@"007AFF"];
//        imageView.layer.cornerRadius = 22.5;
//        imageView.clipsToBounds = YES;
//        
//        UILabel *name = [[UILabel alloc]init];
//        name.bounds = imageView.bounds;
//        name.center = imageView.center;
//        name.font = [UIFont systemFontOfSize:14];
//        name.textAlignment = NSTextAlignmentCenter;
//        name.textColor = [UIColor whiteColor];
//        name.text = self.dPArr[index][@"staffname"];
//        [imageView addSubview:name];
//        
//        [view addSubview:imageView];
//        
//        height = height + view.bounds.size.height;
//        
//        [self.lyB addSubview:view];
//    }
//    self.lyHeightLayout.constant = height;
}
- (IBAction)sendLy:(id)sender {
    NSDictionary *dic = @{@"worklogid":self.blogId,@"content":self.liuyanText.text};
    Y_Wait
    [[ToolOfNetWork sharedTools] YrequestURL:oa_saveWorklogComment withParams:dic.mutableCopy finished:^(id responsObject, NSError *error) {
        Y_Hide
        if (_Success) {
            NSLog(@"%@",responsObject[@"data"]);
//            [MBProgressHUD showMessage:@"留言成功"];
            self.liuyanText.text = @"";
            self.numOfCellH = 0;
             [self getLY];
        } else {
            [MBProgressHUD showError:responsObject[@"message"]];
        }
        NSLog(@"%@",[[responsObject description]kdtk_stringByReplaceingUnicode]);
        NSLog(@"%@",error.localizedDescription);
    }];
       
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dPArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BlogDetailCell *cell = [BlogDetailCell BlogDetailCell:tableView];
    NSString *nameStr = self.dPArr[indexPath.row][@"staffname"];
    if (nameStr.length <3) {
        cell.headName.text = nameStr;
    } else {
        cell.headName.text = [nameStr substringWithRange:NSMakeRange(nameStr.length - 2, 2)];
    }
//    cell.headName.text = nameStr.length > 3 ? [nameStr substringWithRange:NSMakeRange(nameStr.length - 2, 2)] : nameStr;
    cell.name.text = nameStr;
    cell.content.text = self.dPArr[indexPath.row][@"content"];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGSize size = [[self.dPArr objectAtIndex:indexPath.row][@"content"] boundingRectWithSize:CGSizeMake(320.0 / 375.0 * kScreen_Width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13]} context:nil].size;
        CGFloat ht = size.height;
    self.tablevieH = self.tablevieH + 40 + ht;
    self.numOfCellH ++;
    NSLog( @"----%ld------%f",indexPath.row,self.tablevieH);
    self.lyHeightLayout.constant = self.tablevieH;
    if (indexPath.row == self.dPArr.count - 1 && self.numOfCellH > self.dPArr.count + 1) {
        self.lyHeightLayout.constant = self.tablevieH / 2;
    }
    return 40 + ht;
}
#pragma mark --- UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.lxrArr.count - 1;
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
        SeeBlogCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SeeBlogCell" forIndexPath:indexPath];
        cell.view.backgroundColor = [UIColor getColorWithHexString:@"FEA100"];
        cell.view.layer.cornerRadius = 20;
        cell.view.clipsToBounds = YES;
        NSString *nameStr = self.lxrArr[indexPath.row];
        
        if (nameStr.length <3) {
            cell.name.text = nameStr;
        } else {
            cell.name.text = [nameStr substringWithRange:NSMakeRange(nameStr.length - 2, 2)];
        }
        return cell;
    if (indexPath.row == self.lxrArr.count - 1) {
        
    }
}

@end
