//
//  BlogCell.h
//  OA
//
//  Created by Admin on 2017/7/18.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlogCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *headName;
@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *type;
@property (weak, nonatomic) IBOutlet UILabel *wancheng;
@property (weak, nonatomic) IBOutlet UILabel *weiwancheng;
+ (instancetype)BlogCell:(UITableView *)tableView;
@end
