//
//  BlogDetailCell.h
//  OA
//
//  Created by Admin on 2017/8/8.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlogDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *headimg;
@property (weak, nonatomic) IBOutlet UILabel *headName;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *content;
+ (instancetype)BlogDetailCell:(UITableView *)tableView;
@end
