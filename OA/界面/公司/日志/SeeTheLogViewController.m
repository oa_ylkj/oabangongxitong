//
//  SeeTheLogViewController.m
//  OA
//
//  Created by Admin on 2017/7/14.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "SeeTheLogViewController.h"
#import "BlogCell.h"
#import "BlogDetailViewController.h"
@interface SeeTheLogViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (weak, nonatomic) IBOutlet UIImageView *topImg;
@property (weak, nonatomic) IBOutlet UIView *bjView;

//@property (nonatomic ,strong) NSString *type; // 1 为查询自己的日志列表 0 为查询公司的所有日志


@property (nonatomic ,strong) NSMutableArray *dataArr;
@end

@implementation SeeTheLogViewController
{
    NSInteger pageNo;
}
- (void)viewDidLoad {
    [super viewDidLoad];
//    self.type = @"0";
    self.title = @"日志列表";
    pageNo = 1;
    self.dataArr = @[].mutableCopy;
    if ([self.hide isEqualToString:@"0"]) {
        self.bjView.hidden = NO;
    } else {
        self.bjView.hidden = YES;
    }
    [self getData];

    self.tableview.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        pageNo = 1;
        [self.dataArr removeAllObjects];
        [self getData];
        
    }];
    
    self.tableview.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        pageNo ++;
        [self getData];
        
    }];
    // Do any additional setup after loading the view.
}
- (void)getData {
    NSDictionary *dic = @{@"pageNo":[NSString stringWithFormat:@"%ld",pageNo],@"pageSize":@"10",@"status":self.type};
    Y_Wait
    [[ToolOfNetWork sharedTools] YrequestURL:oa_getWorklogList withParams:dic.mutableCopy finished:^(id responsObject, NSError *error) {
        Y_Hide
        if (_Success) {
            NSLog(@"%@",responsObject[@"data"]);
            [self.tableview.mj_header endRefreshing];
            [self.tableview.mj_footer endRefreshing];
            [self.dataArr addObjectsFromArray: responsObject[@"data"]];
            [self.tableview reloadData];
            if ([self.type isEqualToString:@"0"]) {
                [self.topImg setImage:[UIImage imageNamed:@"选项1"]];
            } else {
                [self.topImg setImage:[UIImage imageNamed:@"选项2"]];
            }
        } else {
            [MBProgressHUD showError:responsObject[@"message"]];
        }
        NSLog(@"%@",[[responsObject description]kdtk_stringByReplaceingUnicode]);
        NSLog(@"%@",error.localizedDescription);
    }];
}
- (IBAction)backToWriteLog:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}
- (IBAction)writeBlog:(id)sender {
    if ([self.type isEqualToString:@"0"]) {
        self.type = @"1";
        [self.dataArr removeAllObjects];
        pageNo = 1;
//        [self.topImg setImage:[UIImage imageNamed:@"选项2"]];
        [self getData];
    }
    
}

- (IBAction)sendBlog:(id)sender {
    if ([self.type isEqualToString:@"1"]) {
        self.type = @"0";
        [self.dataArr removeAllObjects];
        pageNo = 1;
//        [self.topImg setImage:[UIImage imageNamed:@"选项1"]];
        [self getData];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic = self.dataArr[indexPath.row];
    BlogCell *cell = [BlogCell BlogCell:tableView];
    cell.name.text = dic[@"recorder"];
    cell.date.text = [dic[@"log_data"] substringToIndex:16];
    cell.type.text = dic[@"name"];
    cell.wancheng.text = [NSString stringWithFormat:@"已完成工作:%@",dic[@"content"]];
    cell.weiwancheng.text = [NSString stringWithFormat:@"未完成工作:%@",dic[@"unfinished_content"]];
    if (cell.name.text.length > 2) {
        cell.headName.text = [cell.name.text substringFromIndex:cell.name.text.length - 2];
    } else {
        cell.headName.text = cell.name.text;
    }
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 210;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    BlogCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    BlogDetailViewController *vc = VCInSB(@"BlogDetailViewController", @"FirstViewControllers");
    vc.blogId = self.dataArr[indexPath.row][@"id"];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
