//
//  BumenChoiceViewController.m
//  OA
//
//  Created by Admin on 2017/7/4.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "BumenChoiceViewController.h"
#import "CompanyCell.h"
#import "ZhanweiCell.h"
#import "BumenChoiceCell.h"
#import "BumenViewController.h"
#import "BmlxrCell.h"
@interface BumenChoiceViewController ()<UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *bodytableview;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic ,strong) NSArray *bumenArr;
@property (nonatomic ,strong) NSMutableArray *toWhomNameArr; // 发送人名字数组
@property (nonatomic ,strong) NSMutableArray *toWhomIdArr; // 发送人id数组
@end

@implementation BumenChoiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"选择联系人";
    self.bodytableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.toWhomIdArr = @[].mutableCopy;
    self.toWhomNameArr = @[].mutableCopy;
    [[ShareUserInfo sharedUserInfo].csArr removeAllObjects];
    [[ShareUserInfo sharedUserInfo].lxrArr removeAllObjects];
    if (self.csshArr) {
        [[ShareUserInfo sharedUserInfo].csArr addObjectsFromArray:self.csshArr];
    }
    if (self.lxrArr) {
        [[ShareUserInfo sharedUserInfo].lxrArr addObjectsFromArray:self.lxrArr];
    }
    UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.minimumInteritemSpacing = 0;
    [layout setItemSize:CGSizeMake(40, 40)];
    self.collectionView.collectionViewLayout = layout;
    [self getData];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.toWhomNameArr removeAllObjects];
    [self.toWhomIdArr removeAllObjects];
    [self.collectionView reloadData];
}
- (void) getData {
    NSDictionary *dic = @{};
    Y_Wait
    [[ToolOfNetWork sharedTools] YrequestURL:oa_getDeptList withParams:dic.mutableCopy finished:^(id responsObject, NSError *error) {
        //        [MBProgressHUD hideHUD];
        Y_Hide
        if (_Success) {
            NSLog(@"%@",responsObject[@"data"]);
            self.bumenArr = responsObject[@"data"];
            [self.bodytableview reloadData];
        } else {
            [MBProgressHUD showError:responsObject[@"message"]];
        }
        NSLog(@"%@",[[responsObject description]kdtk_stringByReplaceingUnicode]);
        NSLog(@"%@",error.localizedDescription);
    }];
}
- (IBAction)sure:(id)sender {
    [self goback];
}
- (void)goback {
    if (self.back != nil) {
        self.back();
    }
    [self.navigationController popToViewController:self.vc animated:YES];
}
#pragma mark --- UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2 + self.bumenArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        CompanyCell *cell = [CompanyCell CompanyCell:tableView];
        return cell;
    } else if (indexPath.row == 1) {
        ZhanweiCell *cell = [ZhanweiCell ZhanweiCell:tableView];
        return cell;
    } else {
        BumenChoiceCell *cell = [BumenChoiceCell BumenChoiceCell:tableView];
        cell.name.text = self.bumenArr[indexPath.row - 2][@"department"];
        return cell;
    }
}
#pragma mark --- UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 60;
    } else if (indexPath.row == 1) {
        return 30;
    } else {
        return 40;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (indexPath.row > 1) {
        BumenViewController *vc = VCInSB(@"BumenViewController", @"BumenViewController");
        vc.bumenName = self.bumenArr[indexPath.row - 2][@"department"];
        vc.bumenId = self.bumenArr[indexPath.row - 2][@"id"];
        vc.type = self.type;
        vc.back = ^(){
            [self goback];
        };
        [self.navigationController pushViewController:vc animated:YES];
    }
}
#pragma mark --- UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if ([self.type isEqualToString:@"0"]) {
        return [ShareUserInfo sharedUserInfo].lxrArr.count;
    } else {
        return [ShareUserInfo sharedUserInfo].csArr.count;
    }
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    BmlxrCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BmlxrCell" forIndexPath:indexPath];
    cell.image.backgroundColor = [UIColor getColorWithHexString:@"FEA100"];
    cell.image.layer.cornerRadius = 20;
    cell.image.clipsToBounds = YES;
    NSString *nameStr;
    if ([self.type isEqualToString:@"0"]) {
        nameStr = [ShareUserInfo sharedUserInfo].lxrArr[indexPath.row][@"name"];
    } else {
        nameStr = [ShareUserInfo sharedUserInfo].csArr[indexPath.row][@"name"];
    }

    
    if (nameStr.length <3) {
        cell.name.text = nameStr;
    } else {
        cell.name.text = [nameStr substringWithRange:NSMakeRange(nameStr.length - 2, 2)];
    }
    return cell;
}

#pragma mark --- UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.type isEqualToString:@"0"]) {
        [[ShareUserInfo sharedUserInfo].lxrArr removeObjectAtIndex:indexPath.row];
    } else {
        [[ShareUserInfo sharedUserInfo].csArr removeObjectAtIndex:indexPath.row];
    }
    
    [self.collectionView reloadData];
    
}

@end
