//
//  BlogChoiceViewController.m
//  OA
//
//  Created by Admin on 2017/7/4.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "BlogChoiceViewController.h"
#import "BlogViewController.h"
#import "SeeTheLogViewController.h"
@interface BlogChoiceViewController ()

@end

@implementation BlogChoiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"日志";
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear: (BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
}
- (IBAction)ribao:(id)sender {
    BlogViewController *vc = VCInSB(@"BlogViewController", @"FirstViewControllers");
    vc.logType = @"日报";
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)zhoubao:(id)sender {
    BlogViewController *vc = VCInSB(@"BlogViewController", @"FirstViewControllers");
    vc.logType = @"周报";
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)yuebao:(id)sender {
    BlogViewController *vc = VCInSB(@"BlogViewController", @"FirstViewControllers");
    vc.logType = @"月报";
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)seeLog:(id)sender {
    SeeTheLogViewController *vc = VCInSB(@"SeeTheLogViewController", @"FirstViewControllers");
    vc.hide = @"0";
    vc.type = @"0";
    [self.navigationController pushViewController:vc animated:NO];
    
}


@end
