//
//  LxrCell.h
//  OA
//
//  Created by Admin on 2017/7/24.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LxrCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *lxrView;
@property (weak, nonatomic) IBOutlet UILabel *name;

@end
