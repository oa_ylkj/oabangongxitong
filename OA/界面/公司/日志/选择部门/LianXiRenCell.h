//
//  LianXiRenCell.h
//  OA
//
//  Created by Admin on 2017/7/5.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LianXiRenCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lxrName;
@property (weak, nonatomic) IBOutlet UIButton *choiceButton;
+ (instancetype)LianXiRenCell:(UITableView *)tableView;
@end
