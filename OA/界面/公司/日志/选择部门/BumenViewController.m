//
//  BumenViewController.m
//  OA
//
//  Created by Admin on 2017/7/3.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "BumenViewController.h"
#import "LianXiRenCell.h"
#import "QuanxuanCell.h"
#import "BumenCell.h"
#import "LxrCell.h"
@interface BumenViewController ()<UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *bodytableview;
@property (nonatomic ,strong) NSArray *dataArr;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;


@property (nonatomic ,strong) NSMutableArray *lxrNameArr;
@property (nonatomic ,strong) NSMutableArray *lxrIdArr;

@property (nonatomic ,strong) NSMutableArray *arr; // 本部门已添加的所有人

@property (nonatomic) BOOL isQuanxuan; // 是否全选

@property (nonatomic) BOOL bl;//删除后 全选变未选择状态
@end

@implementation BumenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"选择联系人";
    self.bodytableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.lxrNameArr = @[].mutableCopy;
    self.lxrIdArr = @[].mutableCopy;
    [self getData];
    UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.minimumInteritemSpacing = 0;
    [layout setItemSize:CGSizeMake(40, 40)];
    self.collectionView.collectionViewLayout = layout;
//    self.arr = [[ShareUserInfo sharedUserInfo].lxrArr[self.bumenId] mutableCopy];
    self.isQuanxuan = NO;
    self.bl = NO;
    if (!self.arr) {
        self.arr = @[].mutableCopy;
    }
//    [self.collectionView registerClass:[LxrCell class] forCellWithReuseIdentifier:@"LxrCell"];
    
//    [self.collectionView registerNib:[UINib nibWithNibName:@"LxrCell" bundle:nil] forCellWithReuseIdentifier:@"LxrCell"];
    // Do any additional setup after loading the view.
}
- (void)getData {
    NSDictionary *dic = @{@"deptid":self.bumenId};
    Y_Wait
    [[ToolOfNetWork sharedTools] YrequestURL:oa_getStaffList withParams:dic.mutableCopy finished:^(id responsObject, NSError *error) {
        Y_Hide
        if (_Success) {
            NSLog(@"%@",responsObject[@"data"]);
            NSMutableArray *arr = @[].mutableCopy;
            self.dataArr = responsObject[@"data"];
            for (NSDictionary *dic in self.dataArr){
                [arr addObject:@{@"id":dic[@"id"],
                                @"name":dic[@"name"]}];
            }
            self.dataArr = arr;
            [self.bodytableview reloadData];
        } else {
            [MBProgressHUD showError:responsObject[@"message"]];
        }
        NSLog(@"%@",[[responsObject description]kdtk_stringByReplaceingUnicode]);
        NSLog(@"%@",error.localizedDescription);
    }];

}
- (IBAction)sure:(id)sender {
    if (self.back != nil) {
        self.back();
    }
}
#pragma mark --- UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.dataArr.count == 0) {
        return 1;
    }
    return 2 + self.dataArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        BumenCell *cell = [BumenCell BumenCell:tableView];
        cell.bumenName.text = self.bumenName;
        return cell;
    } else if (indexPath.row == 1) {
        QuanxuanCell *cell = [QuanxuanCell QuanxuanCell:tableView];
        if (self.bl) {
            cell.quanxuan.selected = NO;
            self.bl = NO;
        }
        return cell;
    } else {
        LianXiRenCell *cell = [LianXiRenCell LianXiRenCell:tableView];
        cell.lxrName.text = self.dataArr[indexPath.row - 2][@"name"];
//        if (!self.isQuanxuan) {
//            cell.choiceButton.selected = NO;
//        }
        cell.choiceButton.selected = NO;
        if ([self.type isEqualToString:@"0"]) {
            for (NSDictionary *dic in [ShareUserInfo sharedUserInfo].lxrArr){
                if ([dic[@"id"] isEqualToString:self.dataArr[indexPath.row - 2][@"id"]]) {
                    cell.choiceButton.selected = YES;
                    break;
                }
            }
        } else {
            for (NSDictionary *dic in [ShareUserInfo sharedUserInfo].csArr){
                if ([dic[@"id"] isEqualToString:self.dataArr[indexPath.row - 2][@"id"]]) {
                    cell.choiceButton.selected = YES;
                    break;
                }
            }
        }
        
        return cell;
    }
    
}
#pragma mark --- UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        return 70;
    } else if (indexPath.row == 1) {
        return 44;
    } else {
        return 40;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    if (indexPath.row == 0)  {
        return;
    } else if(indexPath.row == 1) {
        QuanxuanCell *cell =[tableView cellForRowAtIndexPath:indexPath];
        [self.arr removeAllObjects];
        NSMutableArray *deletArr = @[].mutableCopy;
        if ([self.type isEqualToString:@"0"]) {
            if (!cell.quanxuan.selected) {
                for (NSDictionary *dic in self.dataArr){
                    for (NSDictionary *dict in [ShareUserInfo sharedUserInfo].lxrArr ) {
                        if ([dic[@"id"] isEqualToString:dict[@"id"]]) {
                            
                            [deletArr addObject:dict];
                        }
                    }
                }
                [[ShareUserInfo sharedUserInfo].lxrArr   removeObjectsInArray:deletArr];
                for (NSDictionary *dic in self.dataArr) {
                    [[ShareUserInfo sharedUserInfo].lxrArr addObject:@{@"id":dic[@"id"],@"name":dic[@"name"]}];
                }
            } else {
                [[ShareUserInfo sharedUserInfo].lxrArr removeObjectsInArray:self.dataArr];
            }
        } else {
            if (!cell.quanxuan.selected) {
                for (NSDictionary *dic in self.dataArr){
                    for (NSDictionary *dict in [ShareUserInfo sharedUserInfo].csArr ) {
                        if ([dic[@"id"] isEqualToString:dict[@"id"]]) {

                            [deletArr addObject:dict];
                        }
                    }
                }
                [[ShareUserInfo sharedUserInfo].csArr   removeObjectsInArray:deletArr];
                for (NSDictionary *dic in self.dataArr) {
                    [[ShareUserInfo sharedUserInfo].csArr addObject:@{@"id":dic[@"id"],@"name":dic[@"name"]}];
                }
            } else {
                [[ShareUserInfo sharedUserInfo].csArr removeObjectsInArray:self.dataArr];
            }
        }
        
        cell.quanxuan.selected = !cell.quanxuan.selected;
        [self.bodytableview reloadData];
        [self.collectionView reloadData];
    } else {
        
        LianXiRenCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if (cell.choiceButton.selected) {
            return;
        }
//        [self.lxrNameArr addObject:self.dataArr[indexPath.row - 2][@"name"]];
//        [self.lxrIdArr addObject:self.dataArr[indexPath.row - 2][@"id"]];
        
//        [self.arr addObject:@{@"id":self.dataArr[indexPath.row - 2][@"id"],@"name":self.dataArr[indexPath.row - 2][@"name"]}];
        if ([self.type isEqualToString:@"0"]) {
            [[ShareUserInfo sharedUserInfo].lxrArr addObject:@{@"id":self.dataArr[indexPath.row - 2][@"id"],@"name":self.dataArr[indexPath.row - 2][@"name"]}];
        } else {
            [[ShareUserInfo sharedUserInfo].csArr addObject:@{@"id":self.dataArr[indexPath.row - 2][@"id"],@"name":self.dataArr[indexPath.row - 2][@"name"]}];
        }
//        [[ShareUserInfo sharedUserInfo].lxrArr addObject:@{@"id":self.dataArr[indexPath.row - 2][@"id"],@"name":self.dataArr[indexPath.row - 2][@"name"]}];
        cell.choiceButton.selected = YES;
        [self.collectionView reloadData];
    }
    
    
}

#pragma mark --- UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if ([self.type isEqualToString:@"0"]) {
        return [ShareUserInfo sharedUserInfo].lxrArr.count;
    } else {
        return [ShareUserInfo sharedUserInfo].csArr.count;
    }
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LxrCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LxrCell" forIndexPath:indexPath];
    cell.lxrView.backgroundColor = [UIColor getColorWithHexString:@"FEA100"];
    cell.lxrView.layer.cornerRadius = 20;
    cell.lxrView.clipsToBounds = YES;
    NSString *nameStr;
    if ([self.type isEqualToString:@"0"]) {
        nameStr = [ShareUserInfo sharedUserInfo].lxrArr[indexPath.row][@"name"];
    } else {
        nameStr = [ShareUserInfo sharedUserInfo].csArr[indexPath.row][@"name"];
    }
    
    if (nameStr.length <3) {
        cell.name.text = nameStr;
    } else {
        cell.name.text = [nameStr substringWithRange:NSMakeRange(nameStr.length - 2, 2)];
    }
    return cell;
}

#pragma mark --- UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.type isEqualToString:@"0"]) {
        for (NSDictionary *dic in self.dataArr){
            if ([dic[@"id"] isEqualToString:[ShareUserInfo sharedUserInfo].lxrArr[indexPath.row][@"id"]]) {
                self.isQuanxuan = NO;
                self.bl = YES;
                break;
            }
        }
        [[ShareUserInfo sharedUserInfo].lxrArr removeObjectAtIndex:indexPath.row];
    } else {
        for (NSDictionary *dic in self.dataArr){
            if ([dic[@"id"] isEqualToString:[ShareUserInfo sharedUserInfo].csArr[indexPath.row][@"id"]]) {
                self.isQuanxuan = NO;
                self.bl = YES;
                break;
            }
        }
        [[ShareUserInfo sharedUserInfo].csArr removeObjectAtIndex:indexPath.row];
    }
    
//    [[ShareUserInfo sharedUserInfo].lxrArr setObject:self.arr forKey:self.bumenId];
    [self.collectionView reloadData];
    [self.bodytableview reloadData];
}

@end
