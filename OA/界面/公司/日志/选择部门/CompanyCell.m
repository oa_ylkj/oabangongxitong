//
//  CompanyCell.m
//  OA
//
//  Created by Admin on 2017/7/5.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "CompanyCell.h"

@implementation CompanyCell
+ (instancetype)CompanyCell:(UITableView *)tableView{
    
    CompanyCell *cell =[tableView dequeueReusableCellWithIdentifier:@"CompanyCell"];
    
    if(cell==nil)
    {
        cell = [[CompanyCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CompanyCell"];
        
    }
    return cell;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
