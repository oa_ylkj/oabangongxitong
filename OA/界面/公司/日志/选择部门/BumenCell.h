//
//  BumenCell.h
//  OA
//
//  Created by Admin on 2017/7/5.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BumenCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *bumenName;
+ (instancetype)BumenCell:(UITableView *)tableView;
@end
