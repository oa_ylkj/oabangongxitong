//
//  BumenChoiceCell.m
//  OA
//
//  Created by Admin on 2017/7/5.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "BumenChoiceCell.h"

@implementation BumenChoiceCell
+ (instancetype)BumenChoiceCell:(UITableView *)tableView{
    
    BumenChoiceCell *cell =[tableView dequeueReusableCellWithIdentifier:@"BumenChoiceCell"];
    
    if(cell==nil)
    {
        cell = [[BumenChoiceCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"BumenChoiceCell"];
        
    }
    return cell;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
