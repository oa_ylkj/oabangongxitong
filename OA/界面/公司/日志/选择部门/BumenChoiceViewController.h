//
//  BumenChoiceViewController.h
//  OA
//
//  Created by Admin on 2017/7/4.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^block)();
@interface BumenChoiceViewController : UIViewController
@property (nonatomic ,copy) block back;
@property (nonatomic ,strong) NSString *type; // 0:日志  1:抄送人和审核人
@property (nonatomic ,strong) NSArray *csshArr; // 抄送人和审核人arr
@property (nonatomic ,strong) NSArray *lxrArr; // 日志人arr
@property (nonatomic ,strong) UIViewController *vc;
@end
