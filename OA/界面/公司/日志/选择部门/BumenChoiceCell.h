//
//  BumenChoiceCell.h
//  OA
//
//  Created by Admin on 2017/7/5.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BumenChoiceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *name;
+ (instancetype)BumenChoiceCell:(UITableView *)tableView;
@end
