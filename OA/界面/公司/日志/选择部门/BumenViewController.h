//
//  BumenViewController.h
//  OA
//
//  Created by Admin on 2017/7/3.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^choiceBack)();
@interface BumenViewController : UIViewController
@property (nonatomic ,strong) NSString *bumenName;
@property (nonatomic ,assign) NSString *bumenId;

@property (nonatomic ,copy) choiceBack back;

@property (nonatomic ,strong) NSString *type;// 0:写日志 1:抄送人 2:审核人
@end
