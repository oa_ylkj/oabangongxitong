//
//  CompanyViewController.m
//  OA
//
//  Created by yy on 17/6/5.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "CompanyViewController.h"
#import "MoreViewController.h"
@interface CompanyViewController ()
@property (nonatomic, strong)  UIWebView *zhuye;

@end

@implementation CompanyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSDictionary *dic = @{};
    self.zhuye = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, Y_mainW, Y_mainH)];
    
    
    Y_Wait
    [[ToolOfNetWork sharedTools] YrequestURL:oa_getCompany withParams:dic.mutableCopy finished:^(id responsObject, NSError *error) {
        Y_Hide
        if (_Success) {
            [self.zhuye loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",responsObject[@"data"]]]]];
            [self.view addSubview:self.zhuye];
        } else {
            [MBProgressHUD showError:responsObject[@"message"]];
        }
        NSLog(@"%@",[[responsObject description]kdtk_stringByReplaceingUnicode]);
        NSLog(@"%@",error.localizedDescription);
    }];

    // Do any additional setup after loading the view.
}
//- (void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
//    NSURL* url = [NSURL URLWithString:@"www.baidu.com"];//创建URL
//    NSURLRequest* request = [NSURLRequest requestWithURL:url];//创建NSURLRequest
//    [self.zhuye loadRequest:request];
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)more:(id)sender {
    MoreViewController *vc =  VCInSB(@"MoreViewController",@"CompanyViewController");
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)notice:(id)sender {
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
