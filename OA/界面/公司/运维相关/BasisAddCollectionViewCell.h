//
//  DFAddCollectionViewCell.h
//  DeFeng
//
//  Created by rimi on 16/11/2.
//  Copyright © 2016年 iXcoder. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^remvBlock)();
@interface BasisAddCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UIButton *removeBtn;

@property (nonatomic,copy)remvBlock remove;
- (void)removeOfimg:(remvBlock)rem;

@end
