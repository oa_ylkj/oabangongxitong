//
//  RegionChooseViewController.h
//  OA
//
//  Created by yy on 17/7/20.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>

//@protocol RegionChooseDelegate <NSObject>
//- (void)chooseRegion:(NSDictionary *)regionDic;
//@end

@interface RegionChooseViewController : UIViewController
@property (nonatomic,copy)NSString *regionName;
//@property (nonatomic,weak)id <RegionChooseDelegate>regionChooseDelegate;
@end
