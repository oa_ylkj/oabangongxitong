//
//  MaintenanceDetailTableViewCell.h
//  OA
//
//  Created by yy on 17/6/21.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol addressChouseDelegate <NSObject>
- (void)chouseAddressBtnAction:(UIButton *)sender;
@end

@interface MaintenanceDetailTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *addressL;
@property (weak, nonatomic) IBOutlet UIButton *downBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (nonatomic,weak) id <addressChouseDelegate> delegateOfAddress;

@end
@interface MaintenanceDetailTableViewCellTwo : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *contentTextView;
@property (weak, nonatomic) IBOutlet UITextField *personTextField;
@property (weak, nonatomic) IBOutlet UITextField *dealOfTimerTextField;
@property (weak, nonatomic) IBOutlet UITextField *endTimerTextField;
@property (weak, nonatomic) IBOutlet UITextField *noteTextField;
@property (weak, nonatomic) IBOutlet UIButton *personBtn;
@property (weak, nonatomic) IBOutlet UIButton *delaOfLineTimeBtn;
@property (weak, nonatomic) IBOutlet UIButton *endTimeBtn;

@end

@protocol timeAndStarDelegate <NSObject>

- (void)timeOrStarAction:(UIButton *)sender;

@end
@interface MaintenanceDetailTableViewCellThree : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *choosebtn;
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (nonatomic,weak) id<timeAndStarDelegate>delegate;

@end

@interface MaintenanceDetailTableViewCellFour : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UIButton *onebtn;
@property (weak, nonatomic) IBOutlet UIButton *twobtn;
@property (weak, nonatomic) IBOutlet UIButton *thrbtn;
@property (weak, nonatomic) IBOutlet UIImageView *img;


@end

