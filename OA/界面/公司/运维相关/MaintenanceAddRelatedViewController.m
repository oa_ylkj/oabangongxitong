//
//  MaintenanceAddRelatedViewController.m
//  OA
//
//  Created by yy on 17/7/6.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "MaintenanceAddRelatedViewController.h"
#import "RegionChooseViewController.h"
#import "BumenViewController.h"
@interface MaintenanceAddRelatedViewController ()<TimeChouseDelegate>//RegionChooseDelegate
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backViewBottom;
//完成时间
@property (weak, nonatomic) IBOutlet UIView *endTimeView;
@property (weak, nonatomic) IBOutlet UILabel *endTimeL;
//处理人
@property (weak, nonatomic) IBOutlet UIView *dealView;
@property (weak, nonatomic) IBOutlet UILabel *dealPersonL;
@property (weak, nonatomic) IBOutlet UIButton *dealBtn;
@property (weak, nonatomic) IBOutlet UIImageView *dealImg;
//
@property (weak, nonatomic) IBOutlet UILabel *cityL;
@property (copy, nonatomic) NSString *cityId;

@property (weak, nonatomic) IBOutlet UIImageView *cityImg;
@property (weak, nonatomic) IBOutlet UIButton *cityBtn;

@property (weak, nonatomic) IBOutlet UITextView *contentTextView;
@property (weak, nonatomic) IBOutlet UILabel *receivingTimeL;//接收时间
@property (weak, nonatomic) IBOutlet UIImageView *emergencyImg;//
@property (weak, nonatomic) IBOutlet UIButton *oneEmergencyBtn;//302 301 300
@property (weak, nonatomic) IBOutlet UIButton *twoEmergencyBtn;
@property (weak, nonatomic) IBOutlet UILabel *emergencyL;
@property (weak, nonatomic) IBOutlet UIButton *thrEmergencyBtn;
@property (weak, nonatomic) IBOutlet UILabel *lineTimeL;
@property (weak, nonatomic) IBOutlet UITextField *noteTextF;
//
@property (nonatomic,strong)BasisChooseTimeView *viewOfDate;

@property (nonatomic,strong)NSMutableDictionary *dicOfdataSource;
@property (nonatomic,strong)NSMutableDictionary *dicOfdealPerson;

@end

@implementation MaintenanceAddRelatedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"操作界面";
    
    [self initializeUserInterface];
}

- (void)initializeUserInterface{
    [self addSaveBtn];
    switch (_typeOfNum) {
        case -1:
            self.title = @"新增运维";
            _cityImg.hidden = NO;
            _dealView.hidden = YES;
            _endTimeView.hidden = YES;
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notice:) name:@"regionChooseInfo"object:nil];
            _noteTextF.enabled = YES;
            _contentTextView.editable = YES;
             _dicOfdataSource = [NSMutableDictionary dictionary];
            [_dicOfdataSource setObject:@"4" forKey:@"urgency"];
            _dealBtn.userInteractionEnabled = NO;
            break;
        case 0:
            self.title = @"分配运维";
            _endTimeView.hidden = YES;
            _noteTextF.enabled = NO;
            _contentTextView.editable = NO;
            _dicOfdealPerson = [NSMutableDictionary dictionary];
              _dealBtn.userInteractionEnabled = YES;
            [self initOfDataSouce];
            break;
        case 1:
            self.title = @"完成运维";
            _noteTextF.enabled = NO;
            _contentTextView.editable = NO;
              _dealBtn.userInteractionEnabled = NO;
            [self initOfDataSouce];
           
            break;
            
        default:
            break;
    }
    [self showEmergencyView];
}

- (void)initOfDataSouce{
    NSMutableDictionary *parm = [NSMutableDictionary dictionary];
    [parm setObject:_idOfStr forKey:@"id"];
    [[ToolOfNetWork sharedTools]YrequestURL:oa_getOperationsDetail withParams:parm finished:^(id responsObject, NSError *error) {
        if (_Success) {
            _dicOfdataSource = [NSMutableDictionary dictionaryWithDictionary:responsObject[@"data"]];
            [self initializeUserInterfaceWithData];
        }else{
            Y_MBP_Error
        }
    }];
}

- (void)initializeUserInterfaceWithData{
    
    MaintenanceModel *model = [MaintenanceModel objectWithKeyValues:_dicOfdataSource];
    
    _cityL.text = model.regionName;
    _contentTextView.text = model.content;
    _receivingTimeL.text = [model.receivedate substringWithRange:NSMakeRange(0, 10)];
    _lineTimeL.text = [model.lookdate substringWithRange:NSMakeRange(0, 10)];
    _endTimeL.text = [model.finishdate substringWithRange:NSMakeRange(0, 10)];
    _noteTextF.text = model.note;
    if (_typeOfNum == 0 && _dicOfdealPerson.allKeys.count ==0) {
        _dealPersonL.hidden = YES;
        _dealImg.hidden = NO;
    }else{
        _dealPersonL.hidden = NO;
        _dealImg.hidden = YES;
        _dealPersonL.text = model.lookers;
    }
   
    [self urgencyType:model.urgency];
    
    
}

- (void)urgencyType:(NSString *)type{
    _oneEmergencyBtn.hidden = YES;
    _twoEmergencyBtn.hidden = YES;
    _thrEmergencyBtn.hidden = YES;
//    NSString *strImg = @"";
//    switch ([type intValue]) {
//        case 0:
//            strImg = @"一颗星";
//            break;
//        case 1:
//            strImg = @"两颗星";
//            break;
//        case 2:
//            strImg = @"三颗星";
//            break;
//        default:
//            break;
//    }
//    _emergencyImg.image = Y_IMAGE(strImg);
//    _emergencyImg.contentMode = UIViewContentModeScaleAspectFit;
   
    switch ([type intValue]) {
        case 0:
            _emergencyL.text = @"一般";
            _emergencyL.textColor = BlueColor;
            break;
        case 1:
            _emergencyL.text = @"中等";
            _emergencyL.textColor = OrageStatusColor;
            break;
        case 2:
            _emergencyL.text = @"紧急";
            _emergencyL.textColor = RedColor;
            
            break;
        default:
            break;
    }
    _emergencyImg.hidden = YES;
    
}
#pragma mark —————— UIBarButtonItem
- (void)addSaveBtn{
    UIBarButtonItem *saveBtn = [[UIBarButtonItem alloc]initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(saveBtnActionYw:)];
    self.navigationItem.rightBarButtonItem = saveBtn;
}

- (void)saveBtnActionYw:(UIBarButtonItem *)sender{
     NSLog(@"保存");
    NSMutableDictionary *parms = [NSMutableDictionary dictionary];
    NSString *url = @"";
    switch (_typeOfNum) {
        case -1:
            self.title = @"新增运维";
            if (_cityId.length == 0  || _cityL.text.length == 0) {
                [MBProgressHUD showError:@"请选择区域"];
                return;
            }else{
                [parms setObject:_cityL.text forKey:@"regionName"];//区域名称
                [parms setObject:_cityId forKey:@"regionId"];//区域id
            }
            [parms setObject:_contentTextView.text forKey:@"content"];//内容
            [parms setObject:_receivingTimeL.text forKey:@"receivedate"];//接受时间
            [parms setObject:_lineTimeL.text forKey:@"lookdate"];//处理期限
            [parms setObject:_dicOfdataSource[@"urgency"] forKey:@"urgency"];//紧急程度：1,2,3
            [parms setObject:_noteTextF.text forKey:@"note"];
            url = oa_saveOperations;
            break;
        case 0:
            self.title = @"分配运维";
           
            if (_dicOfdealPerson.allKeys.count > 0){
                NSString *strOfname =  _dicOfdealPerson[@"name"];
                [parms setObject:strOfname forKey:@"lookers"];//抄送者
                NSString*strOfid = _dicOfdealPerson[@"id"];
                 [parms setObject:strOfid forKey:@"staffId"];//抄送者
//                if (strOfname.length>2) {
//                    strOfname = [strOfname substringWithRange:NSMakeRange(strOfname.length-2, 2)];
//                }

                
            }else{
                [MBProgressHUD showError:@"请选择处理人！"];
                return;
            }
            [parms setObject:_dicOfdataSource[@"id"] forKey:@"id"];
            [parms setObject:_lineTimeL.text forKey:@"lookdate"];//处理期限
             url = oa_updateLookersOperations;
            break;
        case 1:
            self.title = @"完成运维";
            [parms setObject:_dicOfdataSource[@"id"] forKey:@"id"];
            if (_endTimeL.text.length>0) {
                [parms setObject:_endTimeL.text forKey:@"finishdate"];//完成时间
            }else{
                [MBProgressHUD showError:@"请输入完成时间"];
                return;
            }
            url = oa_completeOperations;
            break;
        default:
            break;
    }
     NSLog(@"parms==%@",parms);
    [[ToolOfNetWork sharedTools]YrequestURL:url withParams:parms finished:^(id responsObject, NSError *error) {
        if (_Success) {
            if (_typeOfNum == -1) {
                 _ref();
            }else{
                if(_ref !=nil){
                    _ref();
                }
            }
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            Y_MBP_Error
        }
    }];
}

#pragma mark —————— block 返回刷新
- (void)refreshAction:(RefreshMaintenaceVc)refresh{
    
    if (_typeOfNum == -1) {
        _ref = refresh;
    }else{
        if (_ref!=nil) {
            _ref = refresh;
        }
    
    }
    
    
}


- (void)showEmergencyView{
    int typeNum = [[NSString stringWithFormat:@"%@",_dicOfdataSource[@"urgency"]] intValue];
    switch (typeNum) {
        case 2:
            _oneEmergencyBtn.hidden = NO;
            _twoEmergencyBtn.hidden = YES;
            _thrEmergencyBtn.hidden = YES;
            break;
        case 1:
            _oneEmergencyBtn.hidden = YES;
            _twoEmergencyBtn.hidden = NO;
            _thrEmergencyBtn.hidden = YES;
            break;
        case 0:
            _oneEmergencyBtn.hidden = YES;
            _twoEmergencyBtn.hidden = YES;
            _thrEmergencyBtn.hidden = NO;
            break;
            
        default://4
            _oneEmergencyBtn.hidden = NO;
            _twoEmergencyBtn.hidden = NO;
            _thrEmergencyBtn.hidden = NO;
            break;
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ——————

//区域选择
- (IBAction)cityBtnAciton:(UIButton *)sender {//310
    if (_typeOfNum == -1) {
        RegionChooseViewController *regionChooseVc = Y_storyBoard_id(@"RegionChooseViewController");
        regionChooseVc.regionName = _cityL.text;
        [self.navigationController pushViewController:regionChooseVc animated:YES];
    }
}

//紧急程度
- (IBAction)emergencyBtnAction:(UIButton *)sender {//urgency012
    NSInteger tag = sender.tag-TAG_BTN_C;
     NSLog(@"%ld",(long)tag);
    
    int i = [[NSString stringWithFormat:@"%@",_dicOfdataSource[@"urgency"]] intValue];
    if (i == 4) {
        [_dicOfdataSource setObject:[NSString stringWithFormat:@"%ld",(long)tag] forKey:@"urgency"];
        [self showEmergencyView];
    }else{
        [_dicOfdataSource setObject:@"4" forKey:@"urgency"];//清空状态
        [self showEmergencyView];
    }

}
//接收时间
- (IBAction)receivingTimeBtnAction:(UIButton *)sender {//312
   [self.view endEditing:YES];

     if (_typeOfNum == -1) {
         [self getTiemViewWithTag:sender.tag];
     }
}
//处理期限
- (IBAction)lineTimeBtnAction:(UIButton *)sender {//313
    [self.view endEditing:YES];

     NSLog(@"处理期限");
    if (_typeOfNum == -1 ||_typeOfNum == 0) {//分配+新增
        [self getTiemViewWithTag:sender.tag];
    }
    
}

//完成时间
- (IBAction)endTimeBtn:(UIButton *)sender {//315
   [self.view endEditing:YES];

    NSLog(@"完成时间");
    [self getTiemViewWithTag:sender.tag];
}

- (void)getTiemViewWithTag:(NSInteger)tag{
    [self.view addSubview:self.viewOfDate];
    _viewOfDate.tag = tag;
}
#pragma mark ——————
//处理人
- (IBAction)dealPersonBtnAction:(UIButton *)sender {//314
    NSLog(@"处理人");
//    _dealPersonL.text = @""
    BumenChoiceViewController *bumenVc = VCInSB(@"BumenChoiceViewController", @"BumenViewController");
    bumenVc.type = @"1";
    bumenVc.vc = self;
    if (_dicOfdealPerson.allKeys.count == 0) {
        bumenVc.csshArr = @[];
    } else {
        bumenVc.csshArr = [NSArray arrayWithObject:_dicOfdealPerson];

    }
    bumenVc.back = ^() {
        if ([ShareUserInfo sharedUserInfo].csArr.count>0) {
            _dicOfdealPerson = [[ShareUserInfo sharedUserInfo].csArr objectAtIndex:0];
            NSString *strOfname =  _dicOfdealPerson[@"name"];
            if (strOfname.length>2) {
            strOfname =  [strOfname substringWithRange:NSMakeRange(strOfname.length-2, 2)];
            }
            [self initializeUserInterfaceWithData];
            _dealPersonL.text = strOfname;
          
        }
    };
    [self.navigationController pushViewController:bumenVc animated:YES];
    
}

//抄送人审核人的协议

#pragma mark —————— timeChoose
-(BasisChooseTimeView *)viewOfDate{
    if (!_viewOfDate) {
        _viewOfDate = [[[NSBundle mainBundle]loadNibNamed:@"BasisChooseTimeView" owner:self options:nil]objectAtIndex:0];
        _viewOfDate.frame = CGRectMake(30, 30, Y_mainW-60, 280);
        _viewOfDate.center = self.view.center;
        _viewOfDate.layer.cornerRadius = 20;
        _viewOfDate.layer.borderWidth = 0.2;
        _viewOfDate.delegateOfTime = self;
        _viewOfDate.isPrecision = NO;
    }
    return _viewOfDate;
}
- (NSString *)dateOfDidSelect:(NSString *)str{
    NSInteger tag = _viewOfDate.tag;
    switch (tag-TAG_BTN_C) {
        case  12://接收时间
            _receivingTimeL.text  = str;
            break;
        case  13://处理期限
            _lineTimeL.text = str;
            break;
        case  15://完成时间
            _endTimeL.text = str;
            break;
            
        default:
            break;
    }
    [_viewOfDate removeFromSuperview];
    
    return str;
}
- (void)removeBasisChooseTimeView{
    [_viewOfDate removeFromSuperview];
}


#pragma mark —————— regionChoose

- (void)notice:(NSNotification *)regionNotification{
    NSDictionary *dic = regionNotification.userInfo;
    _cityL.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"name"]];
    _cityId = [NSString stringWithFormat:@"%@",[dic objectForKey:@"id"]];
}
@end
