//
//  DFAddCollectionViewCell.m
//  DeFeng
//
//  Created by rimi on 16/11/2.
//  Copyright © 2016年 iXcoder. All rights reserved.
//

#import "BasisAddCollectionViewCell.h"

@implementation BasisAddCollectionViewCell


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:_imgView];
    }
    return self;
}

- (IBAction)removeBtnAction:(UIButton *)sender {
    _remove();
}

- (void)removeOfimg:(remvBlock)rem{
    _remove = rem;
}

@end
