//
//  MaintenanceDetailTableViewCell.m
//  OA
//
//  Created by yy on 17/6/21.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "MaintenanceDetailTableViewCell.h"

@implementation MaintenanceDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)chooseAddressBtn:(UIButton *)sender {
    if (_delegateOfAddress  && [_delegateOfAddress respondsToSelector:@selector(chouseAddressBtnAction:)]) {
        
        [_delegateOfAddress chouseAddressBtnAction:sender];
    }
}

@end

@implementation MaintenanceDetailTableViewCellTwo

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end

@implementation MaintenanceDetailTableViewCellThree

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (IBAction)chooseTimeOrStarAction:(UIButton *)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(timeOrStarAction:)]) {
        [_delegate timeOrStarAction:sender];
    }
}

@end

@implementation MaintenanceDetailTableViewCellFour

- (void)awakeFromNib{
    [super awakeFromNib];
    
}

@end
