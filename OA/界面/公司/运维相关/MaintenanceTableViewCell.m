//
//  MaintenanceTableViewCell.m
//  OA
//
//  Created by yy on 17/6/21.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "MaintenanceTableViewCell.h"
#import "MaintenanceModel.h"
@implementation MaintenanceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _starImg.contentMode = UIViewContentModeScaleAspectFit;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setDic:(NSDictionary *)dic{
    _dic = dic;
    MaintenanceModel *model = [MaintenanceModel objectWithKeyValues:_dic];
    _nameL.text = model.recorder.length>=2?[model.recorder substringWithRange:NSMakeRange(model.recorder.length-2, 2)]:model.recorder;
    _personL.text = model.recorder;
    _timeL.text = [model.createtime substringToIndex:10];
    _contentL.text = model.content;
    _addressL.text = model.regionName;//区域字段
    _typeL.text = [self typewith:model.status model:model];
    
    
    _starImg.image = [self starImgOfstr:_dic[@"urgency"]];
   
  
}
- (UIImage *)starImgOfstr:(NSString *)str{//urgency 紧急程度：1,2,3
    
    NSString *strOfImgName = @"";
    switch ([str intValue]) {
        case 0:
            strOfImgName = @"一颗星";
            _starImgHeight.constant = 13;
            break;
        case 1:
            strOfImgName = @"两颗星";
            _starImgHeight.constant = 13;
            break;
        case 2:
            strOfImgName = @"三颗星";
            _starImgHeight.constant = 21;
            break;
            
        default:
            break;
    }
    UIImage *img = [UIImage imageNamed:strOfImgName];
    return img;
}
- (NSString *)typewith:(NSString *)type  model:(MaintenanceModel*)model{
    NSString *str = @"";
    switch ([type intValue]) {
        case 0:
            str = @"未分配";
            break;
        case 1:
            str = @"已分配";
            _personL.text = [NSString stringWithFormat:@"%@  处理人:%@",model.recorder,model.lookers];
            break;
        case 2:
            str = @"已完成";
            _personL.text = [NSString stringWithFormat:@"%@  处理人:%@",model.recorder,model.lookers];
            break;
            
        default:
            break;
    }
    return str;
    
}
/**{ 
 {
 content = "内容";
 createtime = "2017-07-05 00:00:00.0";
 finishdate = "2017-06-29 00:00:00.0";
 headPortrait = "";
 id = 00000003;
 lookdate = "2017-06-29 00:00:00.0";
 lookers = "李wu;李四;";
 myUnitId = 00000001;
 note = "备注";
 receivedate = "2017-06-29 00:00:00.0";
 recorder = admin;
 regionId = 500101;
 regionName = "万州区";
 status = 0;
 urgency = 1;
 },
,*/
@end
