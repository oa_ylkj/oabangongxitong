//
//  MainteanceShowViewController.m
//  OA
//
//  Created by yy on 17/7/7.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "MainteanceShowViewController.h"

@interface MainteanceShowViewController ()<TimeChouseDelegate,RegionDelegate>
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backViewBottom;
//完成时间
@property (weak, nonatomic) IBOutlet UIView *endTimeView;
@property (weak, nonatomic) IBOutlet UILabel *endTimeL;
//处理人
@property (weak, nonatomic) IBOutlet UIView *dealView;
@property (weak, nonatomic) IBOutlet UILabel *dealPersonL;
@property (weak, nonatomic) IBOutlet UIButton *dealBtn;
@property (weak, nonatomic) IBOutlet UILabel *dealPersonLabel;
//
@property (weak, nonatomic) IBOutlet UILabel *cityL;
@property (weak, nonatomic) IBOutlet UIButton *cityBtn;
@property (weak, nonatomic) IBOutlet UILabel *townL;
@property (weak, nonatomic) IBOutlet UITextView *contentTextView;
@property (weak, nonatomic) IBOutlet UILabel *receivingTimeL;//接收时间
@property (weak, nonatomic) IBOutlet UIImageView *emergencyImg;//
@property (weak, nonatomic) IBOutlet UILabel *emergencyL;
@property (weak, nonatomic) IBOutlet UIButton *oneEmergencyBtn;//紧急301 302 303
@property (weak, nonatomic) IBOutlet UIButton *twoEmergencyBtn;
@property (weak, nonatomic) IBOutlet UIButton *thrEmergencyBtn;
@property (weak, nonatomic) IBOutlet UILabel *lineTimeL;
@property (weak, nonatomic) IBOutlet UITextField *noteTextF;
//
@property (nonatomic,strong)BasisChooseTimeView *viewOfDate;
@property (nonatomic,strong)BasisChooseRegionView *viewOfPlace;
//
@property (nonatomic,strong)NSMutableDictionary *dicOfdataSource;
@end

@implementation MainteanceShowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"详情";
    [self initOfDataSouce];
    [self initializeUserInterface];
}

- (void)initOfDataSouce{
    _dicOfdataSource = [NSMutableDictionary dictionary];
  
 
    NSMutableDictionary *parm = [NSMutableDictionary dictionary];
    [parm setObject:_idOfStr forKey:@"id"];//1==我的列表，0=＝所有
    [[ToolOfNetWork sharedTools]YrequestURL:oa_getOperationsDetail withParams:parm finished:^(id responsObject, NSError *error) {
        if (_Success) {
            _dicOfdataSource = [NSMutableDictionary dictionaryWithDictionary:responsObject[@"data"]];
            
            [self initializeUserInterface];
        }else{
            Y_MBP_Error
        }
    }];
}
- (void)initializeUserInterface{
    
    MaintenanceModel *model = [MaintenanceModel objectWithKeyValues:_dicOfdataSource];
    
    _cityL.text = model.regionName;
    _contentTextView.text = model.content;
    _receivingTimeL.text = [model.receivedate substringWithRange:NSMakeRange(0, 10)];
    _lineTimeL.text = [model.lookdate substringWithRange:NSMakeRange(0, 10)];
    _endTimeL.text = [model.finishdate substringWithRange:NSMakeRange(0, 10)];
    _noteTextF.text = model.note;
//    _dealPersonL.text = model.lookers;
    _dealPersonLabel.text = model.lookers.length>2?[model.lookers substringWithRange:NSMakeRange(model.lookers.length-2, 2)]:model.lookers;
    [self urgencyType:model.urgency];
    
}

- (void)urgencyType:(NSString *)type{
    _oneEmergencyBtn.hidden = YES;
    _twoEmergencyBtn.hidden = YES;
    _thrEmergencyBtn.hidden = YES;
//    NSString *strImg = @"";
//    switch ([type intValue]) {
//        case 0:
//            strImg = @"一颗星";
//            break;
//        case 1:
//            strImg = @"两颗星";
//            break;
//        case 2:
//            strImg = @"三颗星";
//            break;
//        default:
//            break;
//    } 
//    _emergencyImg.image = Y_IMAGE(strImg);
//    _emergencyImg.contentMode = UIViewContentModeScaleAspectFit;
    switch ([type intValue]) {
        case 0:
            _emergencyL.text = @"一般";
            _emergencyL.textColor = BlueColor;
            break;
        case 1:
            _emergencyL.text = @"中等";
            _emergencyL.textColor = OrageStatusColor;
            break;
        case 2:
            _emergencyL.text = @"紧急";
            _emergencyL.textColor = RedColor;
            
            break;
        default:
            break;
    }
    _emergencyImg.hidden = YES;

}
- (void)showEmergencyView{
    int typeNum = [[NSString stringWithFormat:@"%@",_dicOfdataSource[@"urgency"]] intValue];
    switch (typeNum) {
        case 1:
            _oneEmergencyBtn.hidden = NO;
            _twoEmergencyBtn.hidden = YES;
            _thrEmergencyBtn.hidden = YES;
            break;
        case 2:
            _oneEmergencyBtn.hidden = YES;
            _twoEmergencyBtn.hidden = NO;
            _thrEmergencyBtn.hidden = YES;
            break;
        case 3:
            _oneEmergencyBtn.hidden = YES;
            _twoEmergencyBtn.hidden = YES;
            _thrEmergencyBtn.hidden = NO;
            break;
            
        default:
            _oneEmergencyBtn.hidden = NO;
            _twoEmergencyBtn.hidden = NO;
            _thrEmergencyBtn.hidden = NO;
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ——————

//主城
- (IBAction)cityBtnAciton:(UIButton *)sender {//310
    [self.view addSubview:self.viewOfPlace];
    _viewOfPlace.tag = sender.tag;
}
//区县
- (IBAction)townBtnAction:(UIButton *)sender {//311
    [self.view addSubview:self.viewOfPlace];
    _viewOfPlace.tag = sender.tag;
}

//紧急程度
- (IBAction)emergencyBtnAction:(UIButton *)sender {
    NSInteger tag = sender.tag-TAG_BTN_C;
    NSLog(@"%ld",(long)tag);
    
    int i = [[NSString stringWithFormat:@"%@",_dicOfdataSource[@"urgency"]] intValue];
    if (i == 0) {
        [_dicOfdataSource setObject:[NSString stringWithFormat:@"%ld",(long)tag] forKey:@"urgency"];
        [self showEmergencyView];
    }else{
        [_dicOfdataSource setObject:@"0" forKey:@"urgency"];
        [self showEmergencyView];
    }
    
}
//接收时间
- (IBAction)receivingTimeBtnAction:(UIButton *)sender {//312
    [self getTiemViewWithTag:sender.tag];
}
//处理期限
- (IBAction)lineTimeBtnAction:(UIButton *)sender {//313
    NSLog(@"处理期限");
    [self getTiemViewWithTag:sender.tag];
}
//处理人
- (IBAction)dealPersonBtnAction:(UIButton *)sender {//314
    NSLog(@"处理人");
}
//完成时间
- (IBAction)endTimeBtn:(UIButton *)sender {//315
    NSLog(@"完成时间");
    [self getTiemViewWithTag:sender.tag];
    
}

- (void)getTiemViewWithTag:(NSInteger)tag{
    [self.view addSubview:self.viewOfDate];
    _viewOfDate.tag = tag;
}

#pragma mark —————— personChoose
- (BasisChooseRegionView *)viewOfPlace{
    if (!_viewOfPlace) {
        _viewOfPlace = [[[NSBundle mainBundle]loadNibNamed:@"BasisChooseRegionView" owner:self options:nil]objectAtIndex:0];
        _viewOfPlace.frame = CGRectMake(30, 30, Y_mainW-60, 280);
        _viewOfPlace.center = self.view.center;
        _viewOfPlace.layer.cornerRadius = 20;
        _viewOfPlace.layer.borderWidth = 0.2;
        _viewOfPlace.delegateOfRegion = self;
        _viewOfPlace.arrOfData = [NSArray arrayWithObjects:@"1row",@"2row",@"3",@"4",@"5",@"6",nil];
        
        
    }
    return _viewOfPlace;
}

- (NSString *)regionOfdidSelect:(NSString *)strOfArea{
    NSInteger tag = _viewOfPlace.tag-TAG_BTN_C;
    switch (tag) {
        case 10://主城
            _cityL.text = strOfArea;
            break;
        case 11://区县
            _townL.text = strOfArea;
            break;
            
        default:
            break;
    }
    [_viewOfPlace removeFromSuperview];
    
    
    return strOfArea;
}
- (void)removeRegionView{
    [_viewOfPlace removeFromSuperview];
}
#pragma mark —————— timeChoose
-(BasisChooseTimeView *)viewOfDate{
    if (!_viewOfDate) {
        _viewOfDate = [[[NSBundle mainBundle]loadNibNamed:@"BasisChooseTimeView" owner:self options:nil]objectAtIndex:0];
        _viewOfDate.frame = CGRectMake(30, 30, Y_mainW-60, 280);
        _viewOfDate.center = self.view.center;
        _viewOfDate.layer.cornerRadius = 20;
        _viewOfDate.layer.borderWidth = 0.2;
        _viewOfDate.delegateOfTime = self;
        _viewOfDate.isPrecision = NO;
    }
    return _viewOfDate;
}
- (NSString *)dateOfDidSelect:(NSString *)str{
    NSInteger tag = _viewOfDate.tag;
    switch (tag-TAG_BTN_C) {
        case  12://接收时间
            _receivingTimeL.text  = str;
            break;
        case  13://处理期限
            _lineTimeL.text = str;
            break;
        case  15://完成时间
            _endTimeL.text = str;
            break;
            
        default:
            break;
    }
    [_viewOfDate removeFromSuperview];
    
    return str;
}
- (void)removeBasisChooseTimeView{
    [_viewOfDate removeFromSuperview];
}

@end
