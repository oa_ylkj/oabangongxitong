//
//  MaintenanceViewController.m
//  OA
//
//  Created by yy on 17/6/21.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "MaintenanceViewController.h"
#import "MaintenanceTableViewCell.h"
#import "MaintenanceDetailViewController.h"
#import "MainteanceShowViewController.h"
#import "MaintenanceAddRelatedOneViewController.h"
@interface MaintenanceViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic)  IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSMutableArray *arrOfdataSource;
@property (nonatomic,assign) int pageNo;

@property (weak, nonatomic) IBOutlet UIButton *allBtn;
@property (weak, nonatomic) IBOutlet UIButton *oneBtn;
@property (weak, nonatomic) IBOutlet UIImageView *headerBackImg;
@end

@implementation MaintenanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     [self initializeUserInterface];
    Y_tableViewMj_header([weakSelf initOfDataSouce];)
    Y_tableViewMj_footer([weakSelf getNewDataSouce];)
    [self initOfDataSouce];
   
}
#pragma mark —————— 背景颜色

-(void)viewWillAppear: (BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
}
- (IBAction)oneAction:(UIButton *)sender {
    _oneBtn.selected = YES;
    _allBtn.selected = !_oneBtn.selected;
    [self backImg];
}
- (IBAction)allAction:(UIButton *)sender {
    _allBtn.selected = YES;
    _oneBtn.selected = !_allBtn.selected;
    [self backImg];
    
}
- (void)backImg{
    if (_oneBtn.selected) {
        _headerBackImg.image = Y_IMAGE(@"已读背景");
    }else{
        _headerBackImg.image = Y_IMAGE(@"未读背景");
    }
    [self initOfDataSouce];
}
#pragma mark ——————刷新通知
- (void)initializeUserInterface{
   
    self.title = @"运维记录";
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [UIView new];
    
    if (_isMyMaintenanceList == NO) {
        _allBtn.selected = YES;//所有
        _oneBtn.selected = !_allBtn.selected;
        [self backImg];
    }else{
        _oneBtn.selected = YES;//个人
        _allBtn.selected = !_oneBtn.selected;
        [self backImg];
    }
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(notificationOfRefresh:) name:@"RefreshAudit" object:nil];
    
}
- (void)notificationOfRefresh:(NSNotification *)notification{
    [_tableView.mj_header beginRefreshing];
}


- (void)initOfDataSouce{
    _pageNo = 1;
    _arrOfdataSource = [NSMutableArray array];
 
    NSString *pageNo = [NSString stringWithFormat:@"%d",_pageNo];
    NSString *pageSize = [NSString stringWithFormat:@"%d",Y_pageSize];
    NSMutableDictionary *parm = [NSMutableDictionary dictionaryWithObjectsAndKeys: pageNo,@"pageNo",pageSize, @"pageSize",nil];
//    if (_isMyMaintenanceList==YES) {
//        [parm setObject:@"1" forKey:@"status"];//1==我的列表，0=＝所有
//    }else{
//        [parm setObject:@"0" forKey:@"status"];//1==我的列表，0=＝所有
//    }
    
    if (_oneBtn.selected==YES) {
        [parm setObject:@"1" forKey:@"status"];//1==我的列表，0=＝所有
    }else{
        [parm setObject:@"0" forKey:@"status"];//1==我的列表，0=＝所有
    }
    
    [[ToolOfNetWork sharedTools]YrequestURL:oa_getoperationsList withParams:parm finished:^(id responsObject, NSError *error) {
        Y_headerEndRefreshing
        Y_footerEndRefreshing
        if (_Success) {
            _arrOfdataSource = [NSMutableArray arrayWithArray:responsObject[@"data"]];
            if (_arrOfdataSource.count>0) {
                _pageNo+=1;
            }
            Y_MBP_SuccMessage
            [_tableView reloadData];
        }else{
            Y_MBP_ErrMessage
        }
    }];
}
- (void)getNewDataSouce{
    NSString *pageNo = [NSString stringWithFormat:@"%d",_pageNo];
    NSString *pageSize = [NSString stringWithFormat:@"%d",Y_pageSize];
    NSMutableDictionary *parm = [NSMutableDictionary dictionaryWithObjectsAndKeys: pageNo,@"pageNo",pageSize, @"pageSize",nil];
    if (_isMyMaintenanceList==YES) {
        [parm setObject:@"1" forKey:@"status"];//1==我的列表，0=＝所有
    }else{
        [parm setObject:@"0" forKey:@"status"];//1==我的列表，0=＝所有
    }
    
    [[ToolOfNetWork sharedTools]YrequestURL:oa_getoperationsList withParams:parm finished:^(id responsObject, NSError *error) {
        Y_headerEndRefreshing
        Y_footerEndRefreshing
        if (_Success) {
            NSArray *arr = [NSArray arrayWithArray:responsObject[@"data"]];
            [_arrOfdataSource addObjectsFromArray:arr];
            if (arr.count>0) {
                _pageNo+=1;
            }
            [_tableView reloadData];
        }else{
            Y_MBP_Error
        }
    }];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (IBAction)AddNewMaintenance:(UIBarButtonItem *)sender {
     NSLog(@"新增运维");
    MaintenanceAddRelatedOneViewController *maintenanceAddVc = VCInSB(@"MaintenanceAddRelatedOneViewController", @"ThirdViewControllers");
    maintenanceAddVc.typeOfNum = -1;
    [maintenanceAddVc refreshAction:^{
        [_tableView.mj_header beginRefreshing];
    }];

    [self.navigationController pushViewController:maintenanceAddVc animated:YES];
}
#pragma mark —————— 
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _arrOfdataSource.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *seqView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Y_mainW, 12)];
    seqView.backgroundColor = [UIColor clearColor];
    return seqView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 12;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MaintenanceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MaintenanceTableViewCell"];
    if (!cell) {
        cell = [[MaintenanceTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MaintenanceTableViewCell"];
    }
    if (_arrOfdataSource.count!=0) {//多次切换所有和个人点击崩溃
          cell.dic = _arrOfdataSource[indexPath.section];
    }
  
//    cell.starImg.image = [self starImgOfstr:_arrOfdataSource[indexPath.section][@"urgency"]];
//     cell.starImgHeight.constant = 21;
//     cell.starImgHeight.constant = 14;
//     cell.starImgHeight.constant = 7;
    return cell;
}

//- (UIImage *)starImgOfstr:(NSString *)str{//urgency 紧急程度：1,2,3
//   
//    NSString *strOfImgName = @"";
//    switch ([str intValue]) {
//        case 0:
//            strOfImgName = @"一颗星";
//            
//            break;
//        case 1:
//             strOfImgName = @"两颗星";
//            break;
//        case 2:
//             strOfImgName = @"三颗星";
//            break;
//            
//        default:
//            break;
//    }
//     UIImage *img = [UIImage imageNamed:strOfImgName];
//    return img;
//}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"%@--%@",_arrOfdataSource[indexPath.section][@"status"],_arrOfdataSource[indexPath.section][@"regionName"] );
    
    if ([_arrOfdataSource[indexPath.section][@"status"] intValue] == 2) {
        //已完成详情
        MainteanceShowViewController *maintenanceShowVc = VCInSB(@"MainteanceShowViewController", @"ThirdViewControllers");
        maintenanceShowVc.idOfStr = _arrOfdataSource[indexPath.section][@"id"];
        [self.navigationController pushViewController:maintenanceShowVc animated:YES];
    }else{
        MaintenanceAddRelatedViewController *maintenanceVc = VCInSB(@"MaintenanceAddRelatedViewController", @"ThirdViewControllers");//-1 添加 0 分配 1 完成
        [maintenanceVc refreshAction:^{
            [_tableView.mj_header beginRefreshing];
        }];
        
        switch ([_arrOfdataSource[indexPath.section][@"status"] intValue]) {
            case 0:
                maintenanceVc.typeOfNum = 0;
                break;
            case 1:
                maintenanceVc.typeOfNum = 1;
                break;
            default:
                maintenanceVc.typeOfNum = -1;//新增
                break;
                
        }
         maintenanceVc.idOfStr = _arrOfdataSource[indexPath.section][@"id"];
        [self.navigationController pushViewController:maintenanceVc animated:YES];

    }
}


@end
