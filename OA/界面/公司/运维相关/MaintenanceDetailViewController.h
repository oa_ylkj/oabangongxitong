//
//  MaintenanceDetailViewController.h
//  OA
//
//  Created by yy on 17/6/21.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MaintenanceDetailViewController : UIViewController
@property (nonatomic,assign) BOOL isAddMaintenanceType;
@property (nonatomic,copy)NSString *idOfMaintenance;
@end
