//
//  MaintenanceDetailViewController.m
//  OA
//
//  Created by yy on 17/6/21.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "MaintenanceDetailViewController.h"
#import "MaintenanceDetailTableViewCell.h"
@interface MaintenanceDetailViewController ()<UITableViewDataSource,UITableViewDelegate,addressChouseDelegate,timeAndStarDelegate,RegionDelegate,TimeChouseDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong)BasisChooseTimeView *viewOfData;
@property (nonatomic,strong)BasisChooseRegionView *viewOfRegion;

@property (nonatomic,strong)NSMutableDictionary *dicOfdataSource;
@end

@implementation MaintenanceDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeUserInterface];
    [self initOfDataSouce];
}

- (void)initializeUserInterface{
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [UIView new];
    if (_isAddMaintenanceType == NO) {
        self.title = @"详情";
        self.navigationItem.rightBarButtonItem = nil;
    }else{
        self.title = @"新增运维记录";
    }
}
- (void)initOfDataSouce{
    _dicOfdataSource = [NSMutableDictionary dictionary];
     [_dicOfdataSource setObject:@"主城" forKey:@"city"];
     [_dicOfdataSource setObject:@"区县" forKey:@"town"];
     [_dicOfdataSource setObject:@"内容" forKey:@"content"];
     [_dicOfdataSource setObject:@"人" forKey:@"person"];
     [_dicOfdataSource setObject:@"2017" forKey:@"line"];
     [_dicOfdataSource setObject:@"2017" forKey:@"endTime"];
     [_dicOfdataSource setObject:@"笔记备注" forKey:@"note"];
     [_dicOfdataSource setObject:@"2017" forKey:@"getTime"];
     [_dicOfdataSource setObject:@"0" forKey:@"setType"];
    
    NSMutableDictionary *parm = [NSMutableDictionary dictionary];
    [parm setObject:@"00000001" forKey:@"id"];
    [[ToolOfNetWork sharedTools]YrequestURL:oa_getOperationsDetail withParams:parm finished:^(id responsObject, NSError *error) {
        Y_headerEndRefreshing
        Y_footerEndRefreshing
        if (_Success) {
            _dicOfdataSource = [NSMutableDictionary dictionaryWithDictionary:responsObject[@"data"]];
            [_tableView reloadData];
        }else{
            Y_MBP_Error
        }
    }];
    
    
    
}
- (IBAction)saveBtn:(UIBarButtonItem *)sender {
     NSLog(@"保存");
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 1) {
        return 1;
    }else{
        return 2;
    }
    
    return 3;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *seqView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Y_mainW, 12)];
    seqView.backgroundColor = [UIColor clearColor];
    return seqView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 12;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
         return  [self oneCell:tableView AtIndexPath:indexPath];
    }else{
        if (indexPath.section == 1) {
             return [self twoCell:tableView AtIndexPath:indexPath];
        }else{
            if (indexPath.row == 0) {
                return [self threeCell:tableView AtIndexPath:indexPath];
            }else{
                return [self fourCell:tableView AtIndexPath:indexPath];
            }
            
        }
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        return 320;
    }else{
        return 50;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}

#pragma mark —————— cell
- (MaintenanceDetailTableViewCell *)oneCell:(UITableView *)tableView AtIndexPath:(NSIndexPath *)indexPath {
    MaintenanceDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MaintenanceDetailTableViewCell"];
    if (!cell) {
        cell = [[MaintenanceDetailTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MaintenanceDetailTableViewCell"];
    }
    if (indexPath.row == 0) {
        cell.titleL.text = @"主城选择";
        cell.downBtn.tag = TAG_BTN + 1;
        cell.addressL.text = _dicOfdataSource[@"city"];
    }else{
        cell.titleL.text = @"区县选择";
         cell.downBtn.tag = TAG_BTN + 2;
        cell.addressL.text = _dicOfdataSource[@"town"];
    }
    if (self.isAddMaintenanceType == YES) {
        cell.downBtn.hidden = NO;
        cell.delegateOfAddress = self;
    }else{
        cell.downBtn.hidden = YES;
    }
    return cell;
}
- (MaintenanceDetailTableViewCellTwo *)twoCell:(UITableView *)tableView AtIndexPath:(NSIndexPath *)indexPath{
    MaintenanceDetailTableViewCellTwo *cell = [tableView dequeueReusableCellWithIdentifier:@"MaintenanceDetailTableViewCellTwo"];
    if (!cell) {
        cell = [[MaintenanceDetailTableViewCellTwo alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MaintenanceDetailTableViewCellTwo"];
    }

    [cell.noteTextField addTarget:self action:@selector(noteTextHaveChanged:) forControlEvents:UIControlEventEditingChanged];
     NSLog(@"%ld",(long)cell.noteTextField.tag);
    if (self.isAddMaintenanceType == YES) {
        cell.personTextField.enabled = YES;
        cell.dealOfTimerTextField.enabled = YES;
        cell.endTimerTextField.enabled = YES;
        cell.noteTextField.enabled = YES;
        
        cell.personBtn.hidden = NO;
        cell.personBtn.tag  = cell.personTextField.tag+10;
        cell.delaOfLineTimeBtn.hidden = NO;
        cell.delaOfLineTimeBtn.tag = cell.dealOfTimerTextField.tag+10;
        cell.endTimeBtn.hidden = NO;
        cell.endTimeBtn.tag = cell.endTimerTextField.tag+10;
        
        [cell.personBtn addTarget:self action:@selector(addPerson:) forControlEvents:UIControlEventTouchUpInside];
        [cell.delaOfLineTimeBtn addTarget:self action:@selector(addLineTime:) forControlEvents:UIControlEventTouchUpInside];
        [cell.endTimeBtn addTarget:self action:@selector(addEndTime:) forControlEvents:UIControlEventTouchUpInside];
        
        
    }else{
        cell.personTextField.enabled = NO;//125
        cell.dealOfTimerTextField.enabled = NO;//126
        cell.endTimerTextField.enabled = NO;//127
        cell.noteTextField.enabled = NO;//130
        
        cell.personBtn.hidden = YES;
        cell.delaOfLineTimeBtn.hidden = YES;
        cell.endTimeBtn.hidden = YES;
        
    }
    cell.contentTextView.text = _dicOfdataSource[@"content"];
    cell.personTextField.text = _dicOfdataSource[@"person"];
    cell.dealOfTimerTextField.text = _dicOfdataSource[@"line"];
    cell.endTimerTextField.text = _dicOfdataSource[@"endTime"];
    cell.noteTextField.text = _dicOfdataSource[@"note"];
    return cell;
}

- (MaintenanceDetailTableViewCellThree *)threeCell:(UITableView *)tableView AtIndexPath:(NSIndexPath *)indexPath{
    MaintenanceDetailTableViewCellThree *cell = [tableView dequeueReusableCellWithIdentifier:@"MaintenanceDetailTableViewCellThree"];
    if (!cell) {
        cell = [[MaintenanceDetailTableViewCellThree alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MaintenanceDetailTableViewCellThree"];
    }
    if (indexPath.row == 0 ) {
        cell.titleL.text = @"接收时间";
        cell.choosebtn.tag = TAG_BTN + 3;
        cell.detailTextLabel.text = _dicOfdataSource[@"getTime"];
    }
 
    cell.delegate = self;
    return cell;
}

- (MaintenanceDetailTableViewCellFour *)fourCell:(UITableView *)tableView AtIndexPath:(NSIndexPath *)indexPath{
    MaintenanceDetailTableViewCellFour*cell = [tableView dequeueReusableCellWithIdentifier:@"MaintenanceDetailTableViewCellFour"];
    if (!cell) {
        cell = [[MaintenanceDetailTableViewCellFour alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MaintenanceDetailTableViewCellFour"];
    }
   
    if (_isAddMaintenanceType == YES) {
        cell.img.hidden = YES;
        [cell.onebtn addTarget:self action:@selector(jinjiAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.twobtn addTarget:self action:@selector(yibanAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.thrbtn addTarget:self action:@selector(zhongdengAction:) forControlEvents:UIControlEventTouchUpInside];
        
        int typeNum = [[NSString stringWithFormat:@"%@",_dicOfdataSource[@"setType"]] intValue];
        switch (typeNum) {
            case 1:
                cell.onebtn.hidden = NO;
                cell.twobtn.hidden = YES;
                cell.thrbtn.hidden = YES;
                break;
            case 2:
                cell.onebtn.hidden = YES;
                cell.twobtn.hidden = NO;
                cell.thrbtn.hidden = YES;
                break;
            case 3:
                cell.onebtn.hidden = YES;
                cell.twobtn.hidden = YES;
                cell.thrbtn.hidden = NO;
                break;
                
            default:
                cell.onebtn.hidden = NO;
                cell.twobtn.hidden = NO;
                cell.thrbtn.hidden = NO;
                break;
        }

    }else{
        cell.onebtn.hidden = YES;
        cell.twobtn.hidden = YES;
        cell.thrbtn.hidden = YES;
        cell.img.hidden = NO;
        cell.img.contentMode = UIViewContentModeScaleAspectFit;
        int typeNum = [[NSString stringWithFormat:@"%@",_dicOfdataSource[@"setType"]] intValue];
        switch (typeNum) {
            case 1:
                cell.img.image = Y_IMAGE(@"三颗星");
                break;
            case 2:
               cell.img.image = Y_IMAGE(@"两颗星");
                break;
            case 3:
                cell.img.image = Y_IMAGE(@"一颗星");
                break;
                
            default:
                break;
        }

    }
    
    return cell;
}
#pragma mark ——————区域选择 cell1
- (void)chouseAddressBtnAction:(UIButton *)sender{
     NSLog(@"主城区县区域选择按钮");
    [self.view endEditing:YES];
    if (sender.tag-TAG_BTN==1 ) {
         NSLog(@"主城");
        [self.view addSubview:self.viewOfRegion];
        _viewOfRegion.tag = sender.tag;
    }else{
         [self.view addSubview:self.viewOfRegion];
        _viewOfRegion.tag = sender.tag;
         NSLog(@"区县");
    }
}

#pragma mark —————— cell2
- (void)addPerson:(UIButton *)sender{//135
    [self.view endEditing:YES];
     NSLog(@"%lD",(long)sender.tag);
     NSLog(@"添加人");
//    AddressBookViewController *personVc = Y_storyBoard_id(@"AddressBookViewController");
//    personVc.hidesBottomBarWhenPushed = YES;
//    personVc.isPersonChouse = YES;
//    [self.navigationController pushViewController:personVc animated:YES];
 

}
- (void)addLineTime:(UIButton *)sender{
    [self.view endEditing:YES];
    [self.view addSubview:self.viewOfData];
     _viewOfData.tag = sender.tag;
}
- (void)addEndTime:(UIButton *)sender{
    [self.view endEditing:YES];
     [self.view addSubview:self.viewOfData];
     _viewOfData.tag = sender.tag;
    
}
- (void)noteTextHaveChanged:(UITextField *)textField{
     NSLog(@"备注＝＝%@",textField.text);
}
#pragma mark —————— 接收时间选择 cell3
- (void)timeOrStarAction:(UIButton *)sender{
    [self.view endEditing:YES];
    if (sender.tag - TAG_BTN == 3) {
        NSLog(@"时间");
        [self.view addSubview:self.viewOfData];
        _viewOfData.tag = sender.tag;
       
    }else{
         NSLog(@"紧急");
    }
    
}
#pragma mark ——————  紧急程度 cell4
- (void)jinjiAction:(UIButton *)sender{
    
    if ([_dicOfdataSource[@"setType"] isEqualToString:@"1"]) {
         [_dicOfdataSource setObject:@"0" forKey:@"setType"];
    }else{
         [_dicOfdataSource setObject:@"1" forKey:@"setType"];
    }
   
    [self.tableView reloadData];
}
- (void)yibanAction:(UIButton *)sender{
    if ([_dicOfdataSource[@"setType"] isEqualToString:@"2"]) {
        [_dicOfdataSource setObject:@"0" forKey:@"setType"];
    }else{
        [_dicOfdataSource setObject:@"2" forKey:@"setType"];
    }
     [self.tableView reloadData];
}
- (void)zhongdengAction:(UIButton *)sender{
    if ([_dicOfdataSource[@"setType"] isEqualToString:@"3"]) {
        [_dicOfdataSource setObject:@"0" forKey:@"setType"];
    }else{
        [_dicOfdataSource setObject:@"3" forKey:@"setType"];
    }
     [self.tableView reloadData];
}
#pragma mark ——————  view BasisChooseTimeView BasisChooseRegionView


- (UIView *)viewOfRegion{
    if (!_viewOfRegion) {
        _viewOfRegion = (BasisChooseRegionView*)[[[NSBundle mainBundle]loadNibNamed:@"BasisChooseRegionView" owner:self options:nil] objectAtIndex:0];
        _viewOfRegion.frame = CGRectMake(30, 30, Y_mainW-60, 280);
        _viewOfRegion.center = self.view.center;
        _viewOfRegion.layer.cornerRadius = 20;
        _viewOfRegion.layer.borderWidth = 0.2;
        _viewOfRegion.delegateOfRegion = self;
     }
    return _viewOfRegion;
    
}
-(UIView *)viewOfData{
    if (!_viewOfData) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"BasisChooseTimeView" owner:self options:nil];
        _viewOfData = (BasisChooseTimeView *)[nib objectAtIndex:0];
        _viewOfData.frame = CGRectMake(30, 30, Y_mainW-60, 280);
        _viewOfData.center = self.view.center;
        _viewOfData.layer.cornerRadius = 20;
        _viewOfData.layer.borderWidth = 0.2;
        _viewOfData.isPrecision = NO;
        _viewOfData.delegateOfTime = self;
        
    }
    return _viewOfData;
}
#pragma mark —————— View的协议
- (NSString *)regionOfdidSelect:(NSString *)strOfArea{
     NSLog(@"tag=%ld 地区选择了：%@",(long)_viewOfRegion.tag,strOfArea);
    [_viewOfRegion removeFromSuperview];
    if (_viewOfRegion.tag==TAG_BTN+1) {
        [_dicOfdataSource setObject:strOfArea forKey:@"city"];
    }
    if (_viewOfRegion.tag==TAG_BTN+2) {
        [_dicOfdataSource setObject:strOfArea forKey:@"town"];
    }
     [self.tableView reloadData];
    return strOfArea;
}


- (NSString *)dateOfDidSelect:(NSString *)str{
    NSLog(@"tag = %ld 时间选择 %@" ,(long)_viewOfData.tag,str);
    if (_viewOfData.tag-10==TAG_BTN+6) {
         NSLog(@"处理期限：%@",str);
        [_dicOfdataSource setObject:str forKey:@"line"];
    }
    if (_viewOfData.tag-10==TAG_BTN+7) {
        NSLog(@"完成时间：%@",str);
        [_dicOfdataSource setObject:str forKey:@"endTime"];
    }
     if (_viewOfData.tag==TAG_BTN+3) {
         [_dicOfdataSource setObject:str forKey:@"getTime"];
     }
    [_viewOfData removeFromSuperview];
    [self.tableView reloadData];
    return str;
}
- (void)removeBasisChooseTimeView{
    [_viewOfData removeFromSuperview];
}
- (void)removeRegionView{
    [_viewOfRegion removeFromSuperview];
}
#pragma mark —————— touchesBegan
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [_viewOfData removeFromSuperview];
    [_viewOfRegion removeFromSuperview];
    
}

@end
