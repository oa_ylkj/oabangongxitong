//
//  MaintenanceAddRelatedOneViewController.h
//  OA
//
//  Created by yy on 17/9/13.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^RefreshMaintenaceVc)();
@interface MaintenanceAddRelatedOneViewController : UIViewController
@property (nonatomic,strong)NSString *idOfStr;
@property (nonatomic,assign)NSInteger typeOfNum;//-1 添加 0 分配 1 完成
//未分配=0=分配界面，未完成=1=完成界面，已完成=2=详情界面
@property (nonatomic,copy)RefreshMaintenaceVc ref;
- (void)refreshAction:(RefreshMaintenaceVc)refresh;
@end
