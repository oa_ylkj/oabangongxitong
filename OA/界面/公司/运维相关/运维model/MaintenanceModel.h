//
//  MaintenanceModel.h
//  OA
//
//  Created by yy on 17/7/19.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MaintenanceModel : NSObject
@property (nonatomic,copy)NSString *createtime;
@property (nonatomic,copy)NSString *finishdate;
@property (nonatomic,copy)NSString *headPortrait;
@property (nonatomic,copy)NSString *lookdate;
@property (nonatomic,copy)NSString *myUnitId;
@property (nonatomic,copy)NSString *note;
@property (nonatomic,copy)NSString *receivedate;
@property (nonatomic,copy)NSString *recorder;
@property (nonatomic,copy)NSString *regionId;
@property (nonatomic,copy)NSString *regionName;

@property (nonatomic,copy)NSString *status;
@property (nonatomic,copy)NSString *urgency;

@property (nonatomic,copy)NSString *content;
@property (nonatomic,copy)NSString *id;
@property (nonatomic,copy)NSString *lookers;



/** content = "内容";
 createtime = "2017-07-05 00:00:00.0";
 finishdate = "2017-06-29 00:00:00.0";
 headPortrait = "";
 id = 00000003;
 lookdate = "2017-06-29 00:00:00.0";
 lookers = "李wu;李四;";
 myUnitId = 00000001;
 note = "备注";
 receivedate = "2017-06-29 00:00:00.0";
 recorder = admin;
 regionId = 500101;
 regionName = "万州区";/
 status = 0;
 urgency = 1;/
 },
 */
@end
