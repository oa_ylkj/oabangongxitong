//
//  AreaModel.h
//  投票
//
//  Created by yy on 17/3/13.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <Foundation/Foundation.h>
//typedef enum : NSUInteger {
//    ResultLocalData,  //本地数据
//    ResultInternetData,//网络数据
//    ResultInternerError,//网络出错
//} ResultType;


@interface AreaModel : NSObject


@property (nonatomic,strong) NSString *code;
@property (nonatomic,strong) NSString *createTime;
@property (nonatomic,strong) NSString *creator;
@property (nonatomic,strong) NSString *id;
@property (nonatomic,strong) NSString *level;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *parentId;
@property (nonatomic,strong) NSString *regionType;
@property (nonatomic,strong) NSString *remarks;

@property (nonatomic,strong) NSString *isOpen;

@end
