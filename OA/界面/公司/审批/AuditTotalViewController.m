//
//  AuditTotalViewController.m
//  OA
//
//  Created by yy on 17/7/3.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "AuditTotalViewController.h"
#import "IssuedAuditViewController.h"

@interface AuditTotalViewController ()
@property (weak, nonatomic) IBOutlet UIButton *waitAuditBtn;
@property (weak, nonatomic) IBOutlet UIButton *issuedBtn;
@property (weak, nonatomic) IBOutlet UIButton *overTimeBtn;
@property (weak, nonatomic) IBOutlet UIButton *fieldBtn;
@property (weak, nonatomic) IBOutlet UIButton *businessTripBtn;
@property (weak, nonatomic) IBOutlet UIButton *reimbursementBtn;

@end

@implementation AuditTotalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"审批";
    [self initializeUserInterface];
}
-(void)viewWillAppear: (BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
}
- (void)initializeUserInterface{
    
    _waitAuditBtn.imageEdgeInsets = UIEdgeInsetsMake(-_waitAuditBtn.titleLabel.intrinsicContentSize.height, 0, 0, -_waitAuditBtn.titleLabel.intrinsicContentSize.width);
    _waitAuditBtn.titleEdgeInsets = UIEdgeInsetsMake(_waitAuditBtn.currentImage.size.height, -_waitAuditBtn.currentImage.size.width, 0, 0);
 
    _issuedBtn.imageEdgeInsets = UIEdgeInsetsMake(-_issuedBtn.titleLabel.intrinsicContentSize.height, 0, 0, -_issuedBtn.titleLabel.intrinsicContentSize.width);
    _issuedBtn.titleEdgeInsets = UIEdgeInsetsMake(_issuedBtn.currentImage.size.height, -_issuedBtn.currentImage.size.width, 0, 0);
    
    _overTimeBtn.imageEdgeInsets = UIEdgeInsetsMake(-_overTimeBtn.titleLabel.intrinsicContentSize.height, 0, 0, -_overTimeBtn.titleLabel.intrinsicContentSize.width);
    _overTimeBtn.titleEdgeInsets = UIEdgeInsetsMake(_overTimeBtn.currentImage.size.height, -_overTimeBtn.currentImage.size.width, 0, 0);
    
    _fieldBtn.imageEdgeInsets = UIEdgeInsetsMake(-_fieldBtn.titleLabel.intrinsicContentSize.height, 0, 0, -_fieldBtn.titleLabel.intrinsicContentSize.width);
    _fieldBtn.titleEdgeInsets = UIEdgeInsetsMake(_fieldBtn.currentImage.size.height, -_fieldBtn.currentImage.size.width, 0, 0);
    
    _businessTripBtn.imageEdgeInsets = UIEdgeInsetsMake(-_businessTripBtn.titleLabel.intrinsicContentSize.height, 0, 0, -_businessTripBtn.titleLabel.intrinsicContentSize.width);
    _businessTripBtn.titleEdgeInsets = UIEdgeInsetsMake(_businessTripBtn.currentImage.size.height, -_businessTripBtn.currentImage.size.width, 0, 0);
    
    _reimbursementBtn.imageEdgeInsets = UIEdgeInsetsMake(-_reimbursementBtn.titleLabel.intrinsicContentSize.height, 0, 0, -_reimbursementBtn.titleLabel.intrinsicContentSize.width);
    _reimbursementBtn.titleEdgeInsets = UIEdgeInsetsMake(_reimbursementBtn.currentImage.size.height, -_reimbursementBtn.currentImage.size.width, 0, 0);
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark —————— action
//待审核
- (IBAction)waitAudit:(UIButton *)sender {
   
     NSLog(@"待审核");
    WaitAuditViewController *waitAudiVc  = VCInSB(@"WaitAuditViewController",  @"SecondViewControllers");
    [self.navigationController pushViewController:waitAudiVc animated:YES];
}
//已发出
- (IBAction)issued:(UIButton *)sender {
    NSLog(@"已发出");//我发出的
    IssuedAuditViewController *issuedAuditVc = VCInSB(@"IssuedAuditViewController", @"SecondViewControllers");
    [self.navigationController pushViewController:issuedAuditVc animated:YES];
}
//加班
- (IBAction)overTime:(UIButton *)sender {
    NSLog(@"加班");
    OverTimeViewController *overTimeVc = VCInSB(@"OverTimeViewController", @"SecondViewControllers");
    overTimeVc.isFieldType = NO;
    [self.navigationController pushViewController:overTimeVc animated:YES];
}

//外勤
- (IBAction)field:(UIButton *)sender {
//    NSLog(@"外勤");
//    FieldViewController *fieldVc = VCInSB(@"FieldViewController", @"SecondViewControllers");
//    [self.navigationController pushViewController:fieldVc animated:YES];
    
    OverTimeViewController *overTimeVc = VCInSB(@"OverTimeViewController", @"SecondViewControllers");
    overTimeVc.isFieldType = YES;
    [self.navigationController pushViewController:overTimeVc animated:YES];
}
//出差
- (IBAction)businessTrip:(UIButton *)sender {
    NSLog(@"出差");
    BussinessApplyViewController *bussinessApplyVc = VCInSB(@"BussinessApplyViewController", @"SecondViewControllers");
    [self.navigationController pushViewController:bussinessApplyVc animated:YES];
}


//报账
- (IBAction)reimbursement:(UIButton *)sender {
    NSLog(@"报账");
}

@end
