//
//  BussinessApplyViewController.m
//  OA
//
//  Created by yy on 17/7/5.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "BussinessApplyViewController.h"
#import "csCell.h"
#import "csAddCell.h"
#import "shCell.h"
#import "shAddCell.h"
@interface BussinessApplyViewController ()<TimeChouseDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UITextField *placeTF;
@property (weak, nonatomic) IBOutlet UIButton *beginTiemBtn;
@property (weak, nonatomic) IBOutlet UILabel *beginTiemL;
@property (weak, nonatomic) IBOutlet UIButton *endTiemBtn;
@property (weak, nonatomic) IBOutlet UILabel *endTimeL;

@property (weak, nonatomic) IBOutlet UITextField *dayTF;
@property (weak, nonatomic) IBOutlet UITextView *contentTextV;
@property (weak, nonatomic) IBOutlet UIButton *approverBtn;
@property (weak, nonatomic) IBOutlet UIButton *sendToBtn;
@property (nonatomic,strong)BasisChooseTimeView *viewOfData;
@property (nonatomic ,strong) NSMutableArray *csArr;
@property (nonatomic ,strong) NSMutableArray *shArr;
@property (weak, nonatomic) IBOutlet UICollectionView *csCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *shCollectionView;
@end

@implementation BussinessApplyViewController

- (void)viewDidLoad {
    self.title = @"出差申请";
    self.csArr = @[].mutableCopy;
    self.shArr = @[].mutableCopy;
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)beginTimeAction:(UIButton *)sender {
         NSLog(@"开始时间");
    [self.view endEditing:YES];
    [self.view addSubview:self.viewOfData];
    _viewOfData.tag = TAG_BTN + 50;
    
}
- (IBAction)endTimeAction:(UIButton *)sender {
         NSLog(@"结束时间");
    [self.view endEditing:YES];

    [self.view addSubview:self.viewOfData];
    _viewOfData.tag = TAG_BTN + 51;
}
- (IBAction)approverAction:(UIButton *)sender {
     NSLog(@"审批人");
}
- (IBAction)sendToAction:(UIButton *)sender {
     NSLog(@"抄送人");
}
- (IBAction)okAction:(UIButton *)sender {
    NSMutableDictionary *dic = @{}.mutableCopy;
    NSString *num = [self.dayTF.text stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
    if (self.placeTF.text.length == 0) {
        [MBProgressHUD showError:@"请填写出差地点"];
        return;
    }
    if (self.beginTiemL.text.length == 0) {
        [MBProgressHUD showError:@"请选择开始时间"];
        return;
    }
    if (self.endTimeL.text.length == 0) {
        [MBProgressHUD showError:@"请选择结束时间"];
        return;
    }
    if (self.dayTF.text.length == 0 || num.length > 0) {
        [MBProgressHUD showError:@"请填正确的出差天数"];
        return;
    }
    if (self.contentTextV.text.length == 0) {
        [MBProgressHUD showError:@"请填写出差理由"];
        return;
    }
//    if (self.csArr.count == 0) {
//        [MBProgressHUD showError:@"请选择抄送人"];
//        return;
//    }
    if (self.shArr.count == 0) {
        [MBProgressHUD showError:@"请选择审核人"];
        return;
    }
    if (!([self.beginTiemL.text compare:self.endTimeL.text] < 0)) {
        [MBProgressHUD showError:@"结束时间必须大于开始时间"];
        return;
    }
    NSMutableString *csId = @"".mutableCopy;
    NSMutableString *csName = @"".mutableCopy;
    NSMutableString *shId = @"".mutableCopy;
    NSMutableString *shName = @"".mutableCopy;
    for (NSDictionary *dic in self.csArr){
        [csId appendFormat:@"%@",dic[@"id"]];
        [csId appendFormat:@";"];
        [csName appendFormat:@"%@",dic[@"name"]];
        [csName appendFormat:@";"];
    }
    for (NSDictionary *dic in self.shArr){
        [shId appendFormat:@"%@",dic[@"id"]];
//        [shId appendFormat:@";"];
        [shName appendFormat:@"%@",dic[@"name"]];
//        [shName appendFormat:@";"];
    }
    
    [dic setObject:self.beginTiemL.text forKey:@"begintime"];
    [dic setObject:self.endTimeL.text forKey:@"endtime"];
    [dic setObject:self.dayTF.text forKey:@"num"];
    [dic setObject:@"天" forKey:@"numname"];
    [dic setObject:self.contentTextV.text forKey:@"reason"];
    [dic setObject:self.placeTF.text forKey:@"address"];
    [dic setObject:shId forKey:@"auditors"];
    [dic setObject:shName forKey:@"auditorsName"];
    [dic setObject:csId forKey:@"lookers"];
    [dic setObject:csName forKey:@"lookersName"];
    
    Y_Wait
    [[ToolOfNetWork sharedTools] YrequestURL:oa_insertAttendanceAuditByBusinessTravel withParams:dic.mutableCopy finished:^(id responsObject, NSError *error) {
        Y_Hide
        if (_Success) {
            [MBProgressHUD showSuccess:@"申请成功"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
            
        } else {
            [MBProgressHUD showError:responsObject[@"message"]];
        }
        NSLog(@"%@",[[responsObject description]kdtk_stringByReplaceingUnicode]);
        NSLog(@"%@",error.localizedDescription);
    }];

}

#pragma mark --- UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == self.csCollectionView) {
        return self.csArr.count + 1;
    } else {
        return self.shArr.count + 1;
    }
    
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if(collectionView == self.csCollectionView) {
        if (indexPath.row < self.csArr.count) {
            csCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"csCell" forIndexPath:indexPath];
            cell.image.backgroundColor = [UIColor getColorWithHexString:@"FEA100"];
            cell.image.layer.cornerRadius = 20;
            cell.image.clipsToBounds = YES;
            
            NSString *nameStr = self.csArr[indexPath.row][@"name"];
            
            if (nameStr.length <3) {
                cell.name.text = nameStr;
            } else {
                cell.name.text = [nameStr substringWithRange:NSMakeRange(nameStr.length - 2, 2)];
            }
            return cell;
        } else {
            csAddCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"csAddCell" forIndexPath:indexPath];
            return cell;
        }
    } else {
        if (indexPath.row < self.shArr.count) {
            shCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"shCell" forIndexPath:indexPath];
            cell.image.backgroundColor = [UIColor getColorWithHexString:@"FEA100"];
            cell.image.layer.cornerRadius = 20;
            cell.image.clipsToBounds = YES;
            
            NSString *nameStr = self.shArr[indexPath.row][@"name"];
            
            if (nameStr.length <3) {
                cell.name.text = nameStr;
            } else {
                cell.name.text = [nameStr substringWithRange:NSMakeRange(nameStr.length - 2, 2)];
            }
            return cell;
        } else {
            shAddCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"shAddCell" forIndexPath:indexPath];
            return cell;
        }
    }
    
}

#pragma mark --- UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.csCollectionView) {
        if (indexPath.row < self.csArr.count) {
            [self.csArr removeObjectAtIndex:indexPath.row];
            [self.csCollectionView reloadData];
        } else {
            BumenChoiceViewController *vc = VCInSB(@"BumenChoiceViewController", @"BumenViewController");
            vc.type = @"1";
            vc.vc = self;
            vc.csshArr = self.csArr;
            vc.back = ^() {
                if ([ShareUserInfo sharedUserInfo].csArr.count > 0) {
                    [self.csArr removeAllObjects];
                    [self.csArr addObjectsFromArray:    [ShareUserInfo sharedUserInfo].csArr];
                    [self.csCollectionView reloadData];
                }
            };
            [self.navigationController pushViewController:vc animated:YES];
            
        }
    } else {
        if (indexPath.row < self.shArr.count) {
            [self.shArr removeObjectAtIndex:indexPath.row];
            [self.shCollectionView reloadData];
        } else {
            BumenChoiceViewController *vc = VCInSB(@"BumenChoiceViewController", @"BumenViewController");
            vc.type = @"1";
            vc.vc = self;
            vc.csshArr = self.shArr;
            vc.back = ^() {
                if ([ShareUserInfo sharedUserInfo].csArr.count > 0) {
                    [self.shArr removeAllObjects];
                    [self.shArr addObject:[ShareUserInfo sharedUserInfo].csArr[0]];
                    [self.shCollectionView reloadData];
                }
            };
            [self.navigationController pushViewController:vc animated:YES];
            
        }
    }
    
}

#pragma mark —————— 时间选择View相关
-(UIView *)viewOfData{
    if (!_viewOfData) {
       
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"BasisChooseTimeView" owner:self options:nil];
        _viewOfData = (BasisChooseTimeView *)[nib objectAtIndex:0];
        _viewOfData.frame = CGRectMake(30, 30, Y_mainW-60, 280);
        _viewOfData.center = self.view.center;
        _viewOfData.layer.cornerRadius = 20;
        _viewOfData.layer.borderWidth = 0.2;
        _viewOfData.delegateOfTime = self;
        _viewOfData.isPrecision = YES;

    }
    return _viewOfData;
}


- (NSString *)dateOfDidSelect:(NSString *)str{
    NSLog(@"tag = %ld 时间选择 %@" ,(long)_viewOfData.tag,str);
    if (_viewOfData.tag - TAG_BTN == 50) {
        _beginTiemL.text = str;
    }else{
        _endTimeL.text = str;
    }
    [_viewOfData removeFromSuperview];
    return str;
}
- (void)removeBasisChooseTimeView{
    [_viewOfData removeFromSuperview];
}

#pragma mark —————— touchesBegan
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [_viewOfData removeFromSuperview];
}

@end
