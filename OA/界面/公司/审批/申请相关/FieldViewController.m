
//
//  FieldViewController.m
//  OA
//
//  Created by yy on 17/7/5.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "FieldViewController.h"

@interface FieldViewController ()
@property (weak, nonatomic) IBOutlet UIButton *beginTiemBtn;
@property (weak, nonatomic) IBOutlet UIButton *endTiemBtn;
@property (weak, nonatomic) IBOutlet UILabel *beginTimeL;
@property (weak, nonatomic) IBOutlet UILabel *endTimeL;
@property (weak, nonatomic) IBOutlet UILabel *textFieldTitleL;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UILabel *contentTiTleL;
@property (weak, nonatomic) IBOutlet UITextView *contentTextView;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
@property (weak, nonatomic) IBOutlet UIButton *auditBtn;

@end

@implementation FieldViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (IBAction)beginTimeAction:(UIButton *)sender {
}

- (IBAction)endTimeAction:(UIButton *)sender {
    
}
- (IBAction)sendBtnAction:(UIButton *)sender {
}
- (IBAction)auditBtnAction:(UIButton *)sender {
}
- (IBAction)yesBtnAction:(UIButton *)sender {
}

@end
