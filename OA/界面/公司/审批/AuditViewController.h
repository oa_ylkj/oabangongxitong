//
//  AuditViewController.h
//  OA
//
//  Created by yy on 17/6/30.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AuditViewController : UIViewController
@property (nonatomic,assign)BOOL isOperationer;
@property (nonatomic,assign)NSString *typeStr;
@property (nonatomic,copy) NSString *idOfAudit;
@end
