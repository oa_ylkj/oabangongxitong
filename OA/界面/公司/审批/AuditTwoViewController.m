//
//  AuditTwoViewController.m
//  OA
//
//  Created by yy on 17/7/4.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "AuditTwoViewController.h"

@interface AuditTwoViewController ()
@property (weak, nonatomic) IBOutlet UILabel *imgL;
@property (weak, nonatomic) IBOutlet UIImageView *img;//头像
@property (weak, nonatomic) IBOutlet UILabel *personL;//人名
@property (weak, nonatomic) IBOutlet UIImageView *chapterImg;//章
@property (weak, nonatomic) IBOutlet UILabel *typeL;//申请类型
@property (weak, nonatomic) IBOutlet UILabel *beginTimeL;//开始时间
@property (weak, nonatomic) IBOutlet UILabel *endTimeL;//结束时间
@property (weak, nonatomic) IBOutlet UILabel *departmentL;//所在部门
@property (weak, nonatomic) IBOutlet UILabel *dayTitleL;//天数标题
@property (weak, nonatomic) IBOutlet UILabel *dayNumL;//天数
@property (weak, nonatomic) IBOutlet UILabel *contentL;//内容 需要拼接标题字段
@property (weak, nonatomic) IBOutlet UILabel *resonL;//原因事由

@property (weak, nonatomic) IBOutlet UILabel *noteL;//审核备注

@property (nonatomic,strong)NSMutableDictionary *dicOfData;
@property (nonatomic,strong)AuditModel *model;
@end
@implementation AuditTwoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"已审批详情";
    [self initOfDataSouce];
    
}
- (void)initOfDataSouce{
    _dicOfData = [NSMutableDictionary dictionary];
//    [_dicOfData setObject:@"内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容" forKey:@"content"];
    NSMutableDictionary *parm = [NSMutableDictionary dictionaryWithObject:_idOfAudit forKey:@"id"];
    [[ToolOfNetWork sharedTools]YrequestURL:oa_getDetailAuditById withParams:parm finished:^(id responsObject, NSError *error) {
        if (_Success) {
            Y_MBP_Success
            _dicOfData = [NSMutableDictionary dictionaryWithDictionary:responsObject[@"data"]];
            _model = [AuditModel objectWithKeyValues:_dicOfData];
            [self initializeUserInterface];
        }else{
            Y_MBP_ErrMessage
        }
    }];
}
- (void)initializeUserInterface{
    
    ////类型:1加班3请假5出差7外勤9补卡
    switch ([_model.type intValue]) {
        case 1:
            _dayTitleL.text = @"加班天数:";
           
            _contentL.text =  [NSString stringWithFormat:@"加班目的:%@",_model.content];
//             _typeL.text = [NSString stringWithFormat:@"%@  是否为节假日:%@",_model.typeName,_model.isholiday];
            _typeL.text = [NSString stringWithFormat:@"是否为节假日:%@",_model.isholiday];
            _resonL.text = [NSString stringWithFormat:@"加班原因:%@",_model.reason];
            _beginTimeL.text = [_model.begintime substringToIndex:16];
            _endTimeL.text = [_model.endtime substringToIndex:16];
            break;
            
        case 3:
             _dayTitleL.text = @"请假天数:";
            _contentL.text =  [NSString stringWithFormat:@"请假原因:%@",_model.reason];
            _typeL.text = _model.leaveType.length>0?_model.leaveType:_model.typeName;
            _resonL.text = @"";
            _beginTimeL.text = [_model.begintime substringToIndex:16];
            _endTimeL.text = [_model.endtime substringToIndex:16];
            break;
            
        case 5:
            _dayTitleL.text = @"出差天数:";
            _contentL.text =  [NSString stringWithFormat:@"出差地点:%@",_model.address];
             _typeL.text = _model.typeName;
            _resonL.text = [NSString stringWithFormat:@"出差事由:%@",_model.reason];
            _beginTimeL.text = [_model.begintime substringToIndex:16];
            _endTimeL.text = [_model.endtime substringToIndex:16];
            break;
            
        case 7:
            _dayTitleL.text = @"外勤天数:";
            _contentL.text =  [NSString stringWithFormat:@"外勤地点:%@",_model.address];
             _typeL.text = _model.typeName;
            _resonL.text = [NSString stringWithFormat:@"外勤目的:%@",_model.reason];
            _beginTimeL.text = [_model.begintime substringToIndex:16];
            _endTimeL.text = [_model.endtime substringToIndex:16];
            break;
            
        case 9:
            _dayTitleL.text = @"补卡天数:";
            _contentL.text =  [NSString stringWithFormat:@"缺卡原因:%@",_model.reason];
             _typeL.text = _model.typeName;
            _resonL.text = @"";
            _beginTimeL.text = @"";
            _endTimeL.text = @"";
            break;
            
        default:
            break;
    }
    
    _imgL.text = _model.username.length>2?[_model.username substringWithRange:NSMakeRange(_model.username.length-2, 2)]:_model.username;
    _personL.text = _model.username;
    
    _departmentL.text = _model.deptName;
    _dayNumL.text = [NSString stringWithFormat:@"%@%@",_model.num,_model.numname];
    _noteL.text = [NSString stringWithFormat:@"审批备注: %@",_model.reasonBack];
    _chapterImg.image = [self isAgree];
    _chapterImg.contentMode = UIViewContentModeScaleAspectFit;
    
    

}

- (UIImage *)isAgree{
    if ([_model.status intValue] == 1) {
        return Y_IMAGE(@"审核通过");
    }else{
        return Y_IMAGE(@"审核未通过");
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getData{
    //    oa_getDetailAuditById
}

@end
