//
//  AuditTwoViewController.h
//  OA
//
//  Created by yy on 17/7/4.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AuditTwoViewController : UIViewController
@property (nonatomic,strong)NSString *typeStr;
@property (nonatomic,copy)NSString *idOfAudit;
@end
