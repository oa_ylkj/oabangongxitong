//
//  WaitAuditTableViewCell.m
//  OA
//
//  Created by yy on 17/7/4.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "WaitAuditTableViewCell.h"

@implementation WaitAuditTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDic:(NSDictionary *)dic{
    _dic = dic;
    
    AuditModel *model = [AuditModel objectWithKeyValues:_dic];
    
    _nameL.text = model.username.length<=2?model.username:[model.username substringWithRange:NSMakeRange(model.username.length-2, 2)];
    _titleL.text = [NSString stringWithFormat:@"%@的%@",model.username,model.typeName];
    _timeL.text = [model.regdate substringToIndex:16];
    _typeL.text = model.typeName;
    if ([model.type intValue] == 9) {
        _beginTime.text = [NSString stringWithFormat:@"补卡时间:%@",[model.datenumber substringToIndex:10]];
    }else{
          _beginTime.text = [NSString stringWithFormat:@"开始时间:%@",[model.begintime substringToIndex:16]];
    }
    
    switch ([model.status intValue]) {
        case 0:
            _statusL.text = @"待审核";
            _statusL.textColor = [UIColor redColor];
            break;
        case 1:
            _statusL.text = @"审核通过";
            _statusL.textColor = GreenStatusColor;
            break;
        case 2:
             _statusL.text = @"审核驳回";
            _statusL.textColor = [UIColor redColor];
            break;
   
        default:
            break;
    }
}

@end
