//
//  IssuedAuditViewController.m
//  OA
//
//  Created by yy on 17/7/13.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "IssuedAuditViewController.h"
#import "WaitAuditTableViewCell.h"
@interface IssuedAuditViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong)NSMutableArray *arrOfDataSource;
@property (nonatomic,assign)int pageNo;
@end

@implementation IssuedAuditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我发出的";
    Y_tableViewMj_header([weakSelf initOfDataSouce];)
    Y_tableViewMj_footer([weakSelf getNewOfDataSouce];)
    [self initOfDataSouce];
    [self initializeUserInterface];
}

- (void)initOfDataSouce{
    _pageNo = 1;
    _arrOfDataSource = [NSMutableArray array];
    NSMutableDictionary *parm = [NSMutableDictionary dictionary];
    [parm setObject:[NSString stringWithFormat:@"%d",_pageNo] forKey:@"pageNo"];
    [parm setObject:Y_pageSizeStr forKey:@"pageSize"];
    [[ToolOfNetWork sharedTools]YrequestURL:oa_getMySaveAuditList withParams:parm finished:^(id responsObject, NSError *error) {
        Y_headerEndRefreshing
        Y_footerEndRefreshing
        if (_Success) {
            Y_MBP_Success
            _arrOfDataSource = [NSMutableArray arrayWithArray:responsObject[@"data"]];
            if (_arrOfDataSource.count>0) {
                _pageNo+=1;
            }
            [_tableView reloadData];
        }else{
            Y_MBP_ErrMessage
        }
    }];
    
}

- (void)getNewOfDataSouce{
    NSMutableDictionary *parm = [NSMutableDictionary dictionary];
    [parm setObject:[NSString stringWithFormat:@"%d",_pageNo] forKey:@"pageNo"];
    [parm setObject:Y_pageSizeStr forKey:@"pageSize"];
    [[ToolOfNetWork sharedTools]YrequestURL:oa_getMySaveAuditList withParams:parm finished:^(id responsObject, NSError *error) {
        Y_headerEndRefreshing
        Y_footerEndRefreshing
        if (_Success) {
            Y_MBP_Success
            NSArray *arr = [NSArray arrayWithArray:responsObject[@"data"]];
            [_arrOfDataSource addObjectsFromArray:arr];
            if (arr.count>0) {
                _pageNo+=1;
            }
            [_tableView reloadData];
        }else{
            Y_MBP_ErrMessage
        }
    }];
    
}

- (void)initializeUserInterface{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _arrOfDataSource.count;
}
 - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
     return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WaitAuditTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WaitAuditTableViewCell"];
    
    if (!cell) {
        cell = [[WaitAuditTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"WaitAuditTableViewCell"];
    }
    cell.dic = _arrOfDataSource[indexPath.section];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]init];
    view.frame = CGRectMake(0, 0, Y_mainW, 12);
    view.backgroundColor =[UIColor lightGrayColor];
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([_arrOfDataSource[indexPath.section][@"status"] isEqualToString:@"0"]) {//未审核
        AuditViewController *audiVc  = VCInSB(@"AuditViewController",  @"SecondViewControllers");
        audiVc.isOperationer = NO;
        audiVc.idOfAudit = _arrOfDataSource[indexPath.section][@"id"];
        audiVc.typeStr = _arrOfDataSource[indexPath.section][@"type"];
        [self.navigationController pushViewController:audiVc animated:YES];
        
    }else{//已审核
       
        AuditTwoViewController *auditTwoVc  = VCInSB(@"AuditTwoViewController",  @"SecondViewControllers");
        auditTwoVc.idOfAudit = _arrOfDataSource[indexPath.section][@"id"];
        [self.navigationController pushViewController:auditTwoVc animated:YES];
    }
}
@end
