//
//  AuditModel.h
//  OA
//
//  Created by yy on 17/7/24.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AuditModel : NSObject
@property (nonatomic,copy)NSString *address;
@property (nonatomic,copy)NSString *auditdate;
@property (nonatomic,copy)NSString *auditor;
@property (nonatomic,copy)NSString *auditors;
@property (nonatomic,copy)NSString *auditorsName;
@property (nonatomic,copy)NSString *begintime;
@property (nonatomic,copy)NSString *content;
@property (nonatomic,copy)NSString *datenumber;
@property (nonatomic,copy)NSString *endtime;
@property (nonatomic,copy)NSString *id;
@property (nonatomic,copy)NSString *isholiday;
@property (nonatomic,copy)NSString *leaveType;
@property (nonatomic,copy)NSString *loginname;
@property (nonatomic,copy)NSString *lookers;
@property (nonatomic,copy)NSString *lookersName;
@property (nonatomic,copy)NSString *myUnitId;
@property (nonatomic,copy)NSString *num;
@property (nonatomic,copy)NSString *numname;
@property (nonatomic,copy)NSString *reason;
@property (nonatomic,copy)NSString *reasonBack;
@property (nonatomic,copy)NSString *reissuecardTime;
@property (nonatomic,copy)NSString *reissuecardType;
@property (nonatomic,copy)NSString *status;
@property (nonatomic,copy)NSString *statusName;
@property (nonatomic,copy)NSString *type;
@property (nonatomic,copy)NSString *typeName;
@property (nonatomic,copy)NSString *username;
@property (nonatomic,copy)NSString *deptName;//部门
@property (nonatomic,copy)NSString *regdate;


/**address = "";
 auditdate = "<null>";
 auditor = "";
 auditors = ";admin;00000009;";
 auditorsName = ";管理员;超级管理员;";
 begintime = "2017-07-24 18:00:00.0";
 content = "重装电脑";
 datenumber = "2017-07-24 00:00:00.0";
 endtime = "2017-07-24 20:00:00.0";
 id = "9b334343-7046-11e7-b580-00163e0431ce";
 isholiday = "否";
 leaveType = "";
 loginname = dddddd;
 lookers = ";admin;00000009;";
 lookersName = ";管理员;超级管理员;";
 myUnitId = 00000000;
 num = "2.0";
 numname = "小时";
 reason = "加班两小时";
 reasonBack = "";
 reissuecardTime = "<null>";
 reissuecardType = "";
 status = 0;
 statusName = "未审批";
 type = 1;
 typeName = "加班";
 username = test2;
 */
@end
