//
//  AuditModel.m
//  OA
//
//  Created by yy on 17/7/24.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "AuditModel.h"

@implementation AuditModel





/**      {
 address = "";
 auditdate = "<null>";
 auditor = "";
 auditors = ";admin;00000009;";
 auditorsName = ";管理员;超级管理员;";
 begintime = "2017-07-24 18:00:00.0";
 content = "重装电脑";
 datenumber = "2017-07-24 00:00:00.0";
 endtime = "2017-07-24 20:00:00.0";
 id = "9b334343-7046-11e7-b580-00163e0431ce";
 isholiday = "否";
 leaveType = "";
 loginname = dddddd;
 lookers = ";admin;00000009;";
 lookersName = ";管理员;超级管理员;";
 myUnitId = 00000000;
 num = "2.0";
 numname = "小时";
 reason = "加班两小时";
 reasonBack = "";
 reissuecardTime = "<null>";
 reissuecardType = "";
 status = 0;
 statusName = "未审批";
 type = 1;
 typeName = "加班";
 username = test2;
 },
 {
 address = "";
 auditdate = "<null>";
 auditor = "";
 auditors = ";admin;00000009;";
 auditorsName = ";管理员;超级管理员;";
 begintime = "2017-07-25 18:00:00.0";
 content = "义务加班学习";
 datenumber = "2017-07-24 00:00:00.0";
 endtime = "2017-07-25 19:00:00.0";
 id = "c724a1d0-7046-11e7-b580-00163e0431ce";
 isholiday = "否";
 leaveType = "";
 loginname = dddddd;
 lookers = ";admin;00000009;";
 lookersName = ";管理员;超级管理员;";
 myUnitId = 00000000;
 num = "1.0";
 numname = "小时";
 reason = "加班1小时";
 reasonBack = "";
 reissuecardTime = "<null>";
 reissuecardType = "";
 status = 0;
 statusName = "未审批";
 type = 1;
 typeName = "加班";
 username = test2;
 },
 {
 address = "";
 auditdate = "<null>";
 auditor = "";
 auditors = ";admin;00000009;";
 auditorsName = ";管理员;超级管理员;";
 begintime = "<null>";
 content = "";
 datenumber = "2017-07-24 00:00:00.0";
 endtime = "<null>";
 id = "1691a835-7047-11e7-b580-00163e0431ce";
 isholiday = "";
 leaveType = "";
 loginname = dddddd;
 lookers = ";admin;00000009;";
 lookersName = ";管理员;超级管理员;";
 myUnitId = 00000000;
 num = "0.0";
 numname = "";
 reason = "忘记打卡了";
 reasonBack = "";
 reissuecardTime = "2017-07-24 08:29:00.0";
 reissuecardType = 0;
 status = 0;
 statusName = "未审批";
 type = 9;
 typeName = "补卡";
 username = test2;
 },
 {
 address = "";
 auditdate = "<null>";
 auditor = "";
 auditors = ";admin;00000009;";
 auditorsName = ";管理员;超级管理员;";
 begintime = "<null>";
 content = "";
 datenumber = "2017-07-13 00:00:00.0";
 endtime = "<null>";
 id = "2866a3ed-7047-11e7-b580-00163e0431ce";
 isholiday = "";
 leaveType = "";
 loginname = dddddd;
 lookers = ";admin;00000009;";
 lookersName = ";管理员;超级管理员;";
 myUnitId = 00000000;
 num = "0.0";
 numname = "";
 reason = "原因";
 reasonBack = "";
 reissuecardTime = "2017-07-13 08:29:00.0";
 reissuecardType = 0;
 status = 0;
 statusName = "未审批";
 type = 9;
 typeName = "补卡";
 username = test2;
 },
 {
 address = "";
 auditdate = "<null>";
 auditor = "";
 auditors = ";admin;00000009;";
 auditorsName = ";管理员;超级管理员;";
 begintime = "2017-07-14 08:29:00.0";
 content = "";
 datenumber = "<null>";
 endtime = "2017-07-15 18:00:00.0";
 id = "391e7cb1-7047-11e7-b580-00163e0431ce";
 isholiday = "";
 leaveType = "请假类型";
 loginname = dddddd;
 lookers = ";admin;00000009;";
 lookersName = ";管理员;超级管理员;";
 myUnitId = 00000000;
 num = "2.0";
 numname = "天";
 reason = "请假原因";
 reasonBack = "";
 reissuecardTime = "<null>";
 reissuecardType = "";
 status = 0;
 statusName = "未审批";
 type = 3;
 typeName = "请假";
 username = test2;
 },
 {
 address = "";
 auditdate = "<null>";
 auditor = "";
 auditors = ";admin;00000009;";
 auditorsName = ";管理员;超级管理员;";
 begintime = "2017-07-25 08:30:00.0";
 content = "";
 datenumber = "<null>";
 endtime = "2017-07-25 18:00:00.0";
 id = "5e4ada48-7047-11e7-b580-00163e0431ce";
 isholiday = "";
 leaveType = "事假";
 loginname = dddddd;
 lookers = ";admin;00000009;";
 lookersName = ";管理员;超级管理员;";
 myUnitId = 00000000;
 num = "1.0";
 numname = "天";
 reason = "个人原因请假";
 reasonBack = "";
 reissuecardTime = "<null>";
 reissuecardType = "";
 status = 0;
 statusName = "未审批";
 type = 3;
 typeName = "请假";
 username = test2;
 },
 {
 address = "地点";
 auditdate = "<null>";
 auditor = "";
 auditors = ";admin;00000009;";
 auditorsName = ";管理员;超级管理员;";
 begintime = "2017-07-14 08:29:00.0";
 content = "";
 datenumber = "<null>";
 endtime = "2017-07-15 18:00:00.0";
 id = "68fdb5f3-7047-11e7-b580-00163e0431ce";
 isholiday = "";
 leaveType = "";
 loginname = dddddd;
 lookers = ";admin;00000009;";
 lookersName = ";管理员;超级管理员;";
 myUnitId = 00000000;
 num = "2.0";
 numname = "天";
 reason = "出差原因";
 reasonBack = "";
 reissuecardTime = "<null>";
 reissuecardType = "";
 status = 0;
 statusName = "未审批";
 type = 5;
 typeName = "出差";
 username = test2;
 },
 {
 address = "北碚";
 auditdate = "<null>";
 auditor = "";
 auditors = ";admin;00000009;";
 auditorsName = ";管理员;超级管理员;";
 begintime = "2017-07-26 08:30:00.0";
 content = "";
 datenumber = "<null>";
 endtime = "2017-07-26 18:00:00.0";
 id = "96329af4-7047-11e7-b580-00163e0431ce";
 isholiday = "";
 leaveType = "";
 loginname = dddddd;
 lookers = ";admin;00000009;";
 lookersName = ";管理员;超级管理员;";
 myUnitId = 00000000;
 num = "1.0";
 numname = "天";
 reason = "出差到北碚博物馆";
 reasonBack = "";
 reissuecardTime = "<null>";
 reissuecardType = "";
 status = 0;
 statusName = "未审批";
 type = 5;
 typeName = "出差";
 username = test2;
 }
 ); totalNum = 8;*/
@end
