//
//  WaitAuditViewController.m
//  OA
//
//  Created by yy on 17/7/4.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "WaitAuditViewController.h"
#import "WaitAuditTableViewCell.h"
@interface WaitAuditViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *waitAuditedBtn;
@property (weak, nonatomic) IBOutlet UIButton *auditedBtn;
@property (weak, nonatomic) IBOutlet UIImageView *headerBackImg;

@property (nonatomic,strong)NSMutableArray *arrOfdataSource;
@property (nonatomic,assign) int pageNo;
@end

@implementation WaitAuditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"审批";
    Y_tableViewMj_header([weakSelf initOfDataSouce];)
    Y_tableViewMj_footer([weakSelf getNewData];)
    [self initializeUserInterface];
}
-(void)viewWillAppear: (BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
}
- (IBAction)auditedAction:(UIButton *)sender {
    _auditedBtn.selected = YES;
    _waitAuditedBtn.selected = !_auditedBtn.selected;
     [self backImg];
}
- (IBAction)waitAudit:(UIButton *)sender {
    _waitAuditedBtn.selected = YES;
    _auditedBtn.selected = !_waitAuditedBtn.selected;
     [self backImg];
    
}

#pragma mark ——————
- (void)initializeUserInterface{
    //红角标消失
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    _waitAuditedBtn.selected = YES;//待审批
    _auditedBtn.selected = !_waitAuditedBtn.selected;
    [self backImg];
    _tableView.delegate = self;
    _tableView.tableFooterView = [UIView new];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(notificationOfRefresh:) name:@"RefreshAudit" object:nil];
    
}
- (void)notificationOfRefresh:(NSNotification *)notification{
     NSLog(@"%@",notification);
    [_tableView.mj_header beginRefreshing];
}

- (void)backImg{
    if (_auditedBtn.selected) {
        _headerBackImg.image = Y_IMAGE(@"已读背景");
    }else{
        _headerBackImg.image = Y_IMAGE(@"未读背景");
    }
    [self initOfDataSouce];
}
- (void)initOfDataSouce{
    _pageNo = 1;
    if (_arrOfdataSource.count>0) {
        [_arrOfdataSource removeAllObjects];
    }
    _arrOfdataSource = [NSMutableArray array];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:[NSString stringWithFormat:@"%d",_pageNo] forKey:@"pageNo"];
    [dic setObject:Y_pageSizeStr forKey:@"pageSize"];
    if (_waitAuditedBtn.selected == YES) {//待审批
        [dic setObject:@"0" forKey:@"status"];
    }else{
        [dic setObject:@"1" forKey:@"status"];
    }
    [self initWithDataOfparm:dic];
    
}

- (void)getNewData{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:[NSString stringWithFormat:@"%d",_pageNo] forKey:@"pageNo"];
    [dic setObject:Y_pageSizeStr forKey:@"pageSize"];
    if (_waitAuditedBtn.selected == YES) {//待审批
        [dic setObject:@"0" forKey:@"status"];
    }else{
        [dic setObject:@"1" forKey:@"status"];
    }
    [self initWithDataOfparm:dic];
}

- (void)initWithDataOfparm:(NSMutableDictionary *)parm{
    [[ToolOfNetWork sharedTools]YrequestURL:oa_getWeiShenHeYiShenHeAuditList withParams:parm finished:^(id responsObject, NSError *error) {
        Y_headerEndRefreshing
        Y_footerEndRefreshing
        if (_Success) {
            NSArray *arr = [NSArray arrayWithArray:responsObject[@"data"]];
            if (_arrOfdataSource.count==0) {
                _arrOfdataSource = [NSMutableArray arrayWithArray:arr];
                 _tableView.mj_footer.hidden = (_arrOfdataSource.count == 0);
            }else{
                [_arrOfdataSource addObjectsFromArray:arr];
            }
            if (arr.count>0) {
                _pageNo+=1;
            }
            [_tableView reloadData];
            Y_MBP_Success
        }else{
            Y_MBP_ErrMessage
            [_tableView reloadData];
            
        }
    }];
 
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ——————
#pragma mark —————— UITableViewDataSource,UITableViewDelegate>
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arrOfdataSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WaitAuditTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WaitAuditTableViewCell"];
    if (!cell) {
        cell = [[WaitAuditTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"WaitAuditTableViewCell"];
    }
    if (_arrOfdataSource.count!=0) {
         cell.dic = _arrOfdataSource[indexPath.row];
    }//标签切换频率过快的崩溃预防
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 140;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//类型:1加班3请假5出差7外勤9补卡
    if (_auditedBtn.selected) {//已审批详情
        AuditTwoViewController *auditTwoVc  = VCInSB(@"AuditTwoViewController",  @"SecondViewControllers");
        auditTwoVc.typeStr = _arrOfdataSource[indexPath.row][@"type"];
        auditTwoVc.idOfAudit = _arrOfdataSource[indexPath.row][@"id"];
        [self.navigationController pushViewController:auditTwoVc animated:YES];
    }else{//未审批详情
        AuditViewController *audiVc  = VCInSB(@"AuditViewController",  @"SecondViewControllers");
        
        audiVc.isOperationer = YES;
        audiVc.typeStr = _arrOfdataSource[indexPath.row][@"type"];
        audiVc.idOfAudit = _arrOfdataSource[indexPath.row][@"id"];
        [self.navigationController pushViewController:audiVc animated:YES];
        
        
        
    }
}





@end
