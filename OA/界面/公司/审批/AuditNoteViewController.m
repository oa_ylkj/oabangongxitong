//
//  AuditNoteViewController.m
//  OA
//
//  Created by yy on 17/7/28.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "AuditNoteViewController.h"

@interface AuditNoteViewController ()<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *signL;

@end

@implementation AuditNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _textView.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)textViewDidChange:(UITextView *)textView{
        _signL.hidden = (textView.text.length>0);
}

//发送
- (IBAction)okAction:(UIBarButtonItem *)sender {

    [_dicOfParm setObject:_textView.text forKey:@"reasonBack"];
    [[ToolOfNetWork sharedTools]YrequestURL:oa_changeAuditType withParams:_dicOfParm finished:^(id responsObject, NSError *error) {
        if (_Success) {
            Y_MBP_Save_Success
            [self sendNotice];
            NSMutableArray *array = [self.navigationController.viewControllers mutableCopy];
            [array removeObjectsInRange:NSMakeRange(array.count-2, 1)];
            self.navigationController.viewControllers  = array;
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            Y_MBP_ErrMessage
        }
    }];
    
}
- (void)sendNotice{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"RefreshAudit" object:nil];
    
}



@end
