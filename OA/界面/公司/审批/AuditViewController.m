//
//  AuditViewController.m
//  OA
//
//  Created by yy on 17/6/30.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "AuditViewController.h"
#import "AuditDetilTableViewCell.h"
#import "AuditNoteViewController.h"//同意拒绝审批备注
@interface AuditViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *agreeBtn;
@property (weak, nonatomic) IBOutlet UIButton *refusedBtn;

@property (weak, nonatomic) IBOutlet UIView *backViewOfOperation;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backViewOfOperationConstraint;
@property (nonatomic,strong) NSMutableDictionary *dicOfdataSource;

@property (nonatomic,strong)AuditModel *model;
@end

@implementation AuditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initOfDataSouce];
    
}
- (void)initOfDataSouce{
//类型:1加班3请假5出差7外勤9补卡
    
    switch ([_typeStr intValue]) {
        case 1:
            self.title = @"加班审核";
            break;
      
        case 3:
             self.title = @"请假审核";
            break;
       
        case 5:
             self.title = @"出差审核";
            break;
            
        case 7:
            self.title = @"外勤审核";
            break;
            
        case 9:
             self.title = @"补卡审核";
            break;
            
        default:
            break;
    }
    _dicOfdataSource = [NSMutableDictionary dictionary];
    [self getData];
    
}

- (void)getData{
//    oa_getDetailAuditById
    NSMutableDictionary *parm = [NSMutableDictionary dictionaryWithObject:_idOfAudit forKey:@"id"];
    [[ToolOfNetWork sharedTools]YrequestURL:oa_getDetailAuditById withParams:parm finished:^(id responsObject, NSError *error) {
        if (_Success) {
            Y_MBP_Success
            _dicOfdataSource = [NSMutableDictionary dictionaryWithDictionary:responsObject[@"data"]];
            _model = [AuditModel objectWithKeyValues:_dicOfdataSource];
            [self initializeUserInterface];
            [_tableView reloadData];
        }else{
            Y_MBP_ErrMessage
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}
- (void)initializeUserInterface{
    //操作人
    if (_isOperationer == YES) {
        _backViewOfOperation.hidden = NO;
    }else{
        //判断是否为审批人
        if ( [_model.auditorsName isEqualToString:[ShareUserInfo sharedUserInfo].userModel.name]) {
            _isOperationer = YES;
        }else{
            _isOperationer = NO;
        }
        
        //界面修改
        if (_isOperationer == YES) {
            _backViewOfOperation.hidden = NO;
        }else{
            _backViewOfOperation.hidden = YES;
            _backViewOfOperationConstraint.constant = 0;
        }
  
    }
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [UIView new];
    self.tableView.estimatedRowHeight = 350;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    _agreeBtn.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    _agreeBtn.layer.borderWidth = 0.4;
    _refusedBtn.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    _refusedBtn.layer.borderWidth = 0.4;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark —————— btn
- (IBAction)agreeBtnAction:(UIButton *)sender {
     NSLog(@"同意");
    [self changeType:YES];
}

- (IBAction)refusedBtnAction:(UIButton *)sender {
     NSLog(@"拒绝");
    [self changeType:NO];
}


- (void)changeType:(BOOL)agreeType{
    NSMutableDictionary *parm = [NSMutableDictionary dictionary];
     [parm setObject:_idOfAudit forKey:@"id"];
     [parm setObject:@"" forKey:@"reasonBack"];
    if (agreeType == YES) {
        [parm setObject:@"1" forKey:@"status"];
        [parm setObject:@"已审批" forKey:@"statusName"];//（0未审批；1已审批；2已驳回）
    }else{
        [parm setObject:@"2" forKey:@"status"];
        [parm setObject:@"已驳回" forKey:@"statusName"];//（0未审批；1已审批；2已驳回）
    }
    
    [self noteBzVc:parm];
}

-(void)noteBzVc:(NSMutableDictionary *)dic{
    AuditNoteViewController *noteVc = VCInSB(@"AuditNoteViewController", @"SecondViewControllers");
    noteVc.dicOfParm = dic;
    [self.navigationController pushViewController:noteVc animated:YES];
}

#pragma mark —————— tableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
            return [self cellOftableView:tableView IndexPath:indexPath];
            break;
        case 1:
            return [self cellTwoOftableView:tableView IndexPath:indexPath];
            break;
        case 2:
            return [self cellThrOftableView:tableView IndexPath:indexPath];
            break;
            
        default:
            return [self cellFourOftableView:tableView IndexPath:indexPath];
            break;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 1:
            return UITableViewAutomaticDimension;
            break;
        case 2:
            return UITableViewAutomaticDimension;
            break;
            
      default :
            
            return 109;
            break;
    }
}



#pragma mark —————— cell{
- (AuditDetilTableViewCell *)cellOftableView:(UITableView *)tableView IndexPath:(NSIndexPath *)indexPath{
    AuditDetilTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AuditDetilTableViewCell"];
    if (!cell) {
        cell = [[AuditDetilTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AuditDetilTableViewCell"];
    }
    cell.personL.text = _model.username;
    cell.imgL.text = _model.username.length>2?[_model.username substringWithRange:NSMakeRange(_model.username.length-2, 2)]:_model.username;
//    switch ([_model.status intValue]) {
//        case 0:
//            
//            break;
//            
//        default:
//            break;
//    }
//    cell.typeL.text = _model.statusName;
    return cell;
}


- (AuditDetilTableViewCellTwo *)cellTwoOftableView:(UITableView *)tableView IndexPath:(NSIndexPath *)indexPath{
    AuditDetilTableViewCellTwo *cell = [tableView dequeueReusableCellWithIdentifier:@"AuditDetilTableViewCellTwo"];
    if (!cell) {
        cell = [[AuditDetilTableViewCellTwo alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AuditDetilTableViewCellTwo"];
    }
    
    
    
    switch ([_typeStr intValue]) {
       
        case 1:
            cell.resonTitleL.text = @"加班原因:";
            cell.beginTimeL.text = [_model.begintime substringToIndex:16];
            cell.endTimeL.text = [_model.endtime substringToIndex:16];
            break;
            
        case 3:
            cell.resonTitleL.text = @"请假理由:";
            cell.beginTimeL.text = [_model.begintime substringToIndex:16];
            cell.endTimeL.text = [_model.endtime substringToIndex:16];
            break;
            
        case 5:
            cell.resonTitleL.text = @"出差目的:";
            cell.beginTimeL.text = [_model.begintime substringToIndex:16];
            cell.endTimeL.text = [_model.endtime substringToIndex:16];
            break;
            
        case 7:
            cell.resonTitleL.text = @"外勤目的:";
            cell.beginTimeL.text = [_model.begintime substringToIndex:16];
            cell.endTimeL.text = [_model.endtime substringToIndex:16];
            break;
       
        case 9:
            cell.resonTitleL.text = @"缺卡原因:";
            cell.beginTimeL.text = @"";
            cell.endTimeL.text = @"";
            break;
            
        default:
            break;
    }
    
//    cell.reasonL.text = _model.reason.length>0 ?_model.reason:_model.content;
    cell.reasonL.text = _model.reason;
    
    
    return cell;
    
}

- (AuditDetilTableViewCellThr *)cellThrOftableView:(UITableView *)tableView IndexPath:(NSIndexPath *)indexPath{
    AuditDetilTableViewCellThr *cell = [tableView dequeueReusableCellWithIdentifier:@"AuditDetilTableViewCellThr"];
    if (!cell) {
        cell = [[AuditDetilTableViewCellThr alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AuditDetilTableViewCellThr"];
    }
    cell.typeL.text = @"";
    switch ([_typeStr intValue]) {
        case 1:
            cell.typeTitleL.text = @"加班内容:";
            cell.dayTitleL.text = @"加班天数:";
            cell.typeL.text = _model.content;
            break;
            
        case 3:
            cell.typeTitleL.text = @"请假类型:";
            cell.dayTitleL.text = @"请假天数:";
            cell.typeL.text = _model.leaveType;
            break;
            
        case 5:
            cell.typeTitleL.text = @"出差地点:";
            cell.dayTitleL.text = @"出差天数:";
            cell.typeL.text = _model.address;
            break;
            
        case 7:
            cell.typeTitleL.text = @"外勤地点:";
            cell.dayTitleL.text = @"外勤天数:";
            cell.typeL.text = _model.address;
            break;
       
        case 9:
            cell.typeTitleL.text = @"缺卡原因:";
            cell.dayTitleL.text = @"缺卡次数:";
            cell.typeL.text = _model.leaveType;
            break;
            
        default:
            break;
    }
    
    cell.departmentL.text = _model.deptName;
    cell.dayNumL.text = [NSString stringWithFormat:@"%@%@",_model.num,_model.numname];
    return cell;
}

- (AuditDetilTableViewCellFour *)cellFourOftableView:(UITableView *)tableView IndexPath:(NSIndexPath *)indexPath{
    AuditDetilTableViewCellFour *cell = [tableView dequeueReusableCellWithIdentifier:@"AuditDetilTableViewCellFour"];
    if (!cell) {
        cell = [[AuditDetilTableViewCellFour alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AuditDetilTableViewCellFour"];
    }
    if (indexPath.row == 3) {
        cell.typeL.text = @"发起申请";
        cell.typeL.textColor = GreenStatusColor;
        [cell.timeBtn setTitle:@"发起申请" forState:UIControlStateNormal];
        [cell.timeBtn setTitleColor:[UIColor darkGrayColor]  forState:UIControlStateNormal];
        [cell.timeBtn setTitle:[_model.regdate substringToIndex:16] forState:UIControlStateNormal];
         cell.personL.text = _model.username;
        cell.imgL.text = _model.username.length>2?[_model.username substringWithRange:NSMakeRange(_model.username.length-2, 2)]:_model.username;
    }else{
        cell.typeL.text = @"审核中";
        cell.typeL.textColor = OrageStatusColor;
//        [cell.timeBtn setTitle:[_model.datenumber substringToIndex:10] forState:UIControlStateNormal];
        [cell.timeBtn setTitle:@"等待审核" forState:UIControlStateNormal];
        [cell.timeBtn setTitleColor:OrageStatusColor   forState:UIControlStateNormal];
         cell.personL.text = _model.auditorsName;
         cell.imgL.text = _model.auditorsName.length>2?[_model.auditorsName substringWithRange:NSMakeRange(_model.auditorsName.length-2, 2)]:_model.auditorsName;
    }
   
    return cell;
}

@end
