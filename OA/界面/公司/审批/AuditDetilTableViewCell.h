//
//  AuditDetilTableViewCell.h
//  OA
//
//  Created by yy on 17/6/30.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AuditDetilTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *personL;
@property (weak, nonatomic) IBOutlet UILabel *typeL;
@property (weak, nonatomic) IBOutlet UILabel *imgL;

@end


@interface AuditDetilTableViewCellTwo : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *resonTitleL;
@property (weak, nonatomic) IBOutlet UILabel *reasonL;
@property (weak, nonatomic) IBOutlet UILabel *beginTimeL;
@property (weak, nonatomic) IBOutlet UILabel *endTimeL;

@end


@interface AuditDetilTableViewCellThr : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *typeTitleL;
@property (weak, nonatomic) IBOutlet UILabel *typeL;
@property (weak, nonatomic) IBOutlet UILabel *dayTitleL;
@property (weak, nonatomic) IBOutlet UILabel *dayNumL;
@property (weak, nonatomic) IBOutlet UILabel *departmentL;//部门

@end


@interface AuditDetilTableViewCellFour : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *personL;
@property (weak, nonatomic) IBOutlet UILabel *typeL;
@property (weak, nonatomic) IBOutlet UIButton *timeBtn;
@property (weak, nonatomic) IBOutlet UILabel *imgL;

@end
