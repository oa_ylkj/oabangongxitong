//
//  WorkPlanViewController.m
//  OA
//
//  Created by yy on 17/6/19.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "WorkPlanViewController.h"
#import "WorkPlanTableViewCell.h"
#import "WorkPlanDetailViewController.h"

@interface WorkPlanViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong)NSMutableArray *arrOfData;
@property (nonatomic,assign)int pageNo;
@end

@implementation WorkPlanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"工作计划";
    _pageNo = 1;
    Y_tableViewMj_header([weakSelf initOfDataSouce];)
    Y_tableViewMj_footer([weakSelf getNewData];)
     _arrOfData = [NSMutableArray array];
    [self initOfDataSouce];
    [self initializeUserInterface];
    [self addNotice];

}


//子计划状态改变的通知
- (void)addNotice{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshOfWorkPlan:) name:@"refreshOfWorkPlanList" object:nil];
}
- (void)refreshOfWorkPlan:(NSNotification*)notice{

    [_tableView.mj_header beginRefreshing];
         NSLog(@"%@",notice);
}
-(void)viewWillAppear: (BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
}
- (void)initOfDataSouce{
    _pageNo = 1;
    NSString *pageNo = [NSString stringWithFormat:@"%d",_pageNo];
    NSString *pageSize = [NSString stringWithFormat:@"%d",Y_pageSize];
    NSMutableDictionary *parm = [NSMutableDictionary dictionaryWithObjectsAndKeys: pageNo,@"pageNo",pageSize, @"pageSize",nil];
    if (_isMyListVc==YES) {
         [parm setObject:@"1" forKey:@"status"];//1==我的列表，0=＝所有
    }else{
         [parm setObject:@"0" forKey:@"status"];//1==我的列表，0=＝所有
    }
   
    [[ToolOfNetWork sharedTools]YrequestURL:oa_workplanList withParams: parm finished:^(id responsObject, NSError *error) {
        Y_headerEndRefreshing;
        Y_footerEndRefreshing;
        if (_Success) {
            NSArray *arr = [NSArray arrayWithArray:responsObject[@"data"]];
            
            if ( arr.count>0) {
                 _pageNo+=1;
            }
            _arrOfData = [NSMutableArray arrayWithArray:arr];
            _tableView.mj_footer.hidden = (_arrOfData.count == 0);
            [_tableView reloadData];
            Y_MBP_Success;
        }else{
            Y_MBP_Error;
            
        }
    }];
}

- (void)getNewData{
    NSString *pageNo = [NSString stringWithFormat:@"%d",_pageNo];
    NSString *pageSize = [NSString stringWithFormat:@"%d",Y_pageSize];
    NSMutableDictionary *parm = [NSMutableDictionary dictionaryWithObjectsAndKeys: pageNo,@"pageNo",pageSize, @"pageSize",nil];
    [parm setObject:@"1" forKey:@"status"];//1==我的列表，0=＝所有
    [[ToolOfNetWork sharedTools]YrequestURL:oa_workplanList withParams: parm finished:^(id responsObject, NSError *error) {
        Y_headerEndRefreshing;
        Y_footerEndRefreshing;
        if (_Success) {
            NSArray *arr = [NSArray arrayWithArray:responsObject[@"data"]];
            
            if ( arr.count>0) {
                _pageNo+=1;
            }
            [_arrOfData addObjectsFromArray:arr];
          
            _tableView.mj_footer.hidden = (_arrOfData.count == 0);
            [_tableView reloadData];
            Y_MBP_Success;
        }else{
            Y_MBP_Error;
            
        }
    }];

}
- (void)initializeUserInterface{
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [UIView new];
    
    self.tableView.estimatedRowHeight = 120.0f;
    self.tableView.rowHeight =UITableViewAutomaticDimension;
//    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        if ([self currentNetworkStatus]) {
//            Y_WEAKSELF
//            [weakSelf initOfDataSouce];;
//        }else{
//            [MBProgressHUD showError:@"网络异常,请检查您的网络设置!"];
//            [_tableView.mj_header endRefreshing];
//        }
//    }];
//    self.tableView.mj_footer = [MJRefreshFooter footerWithRefreshingBlock:^{
//        if ([self currentNetworkStatus]) {
//            Y_WEAKSELF
//            [weakSelf initOfDataSouce];
//
//        }else{
//            [MBProgressHUD showError:@"网络异常,请检查您的网络设置!"];
//            [_tableView.mj_footer endRefreshing];
//        }
//
//    }];
 
    
}
- (IBAction)addWorkPlanAction:(UIBarButtonItem *)sender {
    WorkPlanDetailViewController *detail = VCInSB(@"WorkPlanDetailViewController", @"ThirdViewControllers");
    detail.isAddWorkPlanType = YES;
    
    [detail refreshAction:^{
        [_tableView.mj_header beginRefreshing];
    }];
    [self.navigationController pushViewController:detail animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark —————— 
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _arrOfData.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *seqView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Y_mainW, 12)];
    seqView.backgroundColor = [UIColor clearColor];
    return seqView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 12;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WorkPlanTableViewCell *cell = [tableView  dequeueReusableCellWithIdentifier:@"WorkPlanTableViewCell"];
    if (!cell) {
        cell = [[WorkPlanTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"WorkPlanTableViewCell"];
    }
    cell.dic = _arrOfData[indexPath.section];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    WorkPlanDetailViewController *detail = VCInSB(@"WorkPlanDetailViewController", @"ThirdViewControllers");
    detail.isAddWorkPlanType = NO;
    detail.idOfWorkPlan = _arrOfData[indexPath.section][@"id"];
    [self.navigationController pushViewController:detail animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 72;
}
#pragma mark —————— 

- (IBAction)addPlanAction:(UIBarButtonItem *)sender {
}

@end
