//
//  WorkHeaderView.h
//  OA
//
//  Created by yy on 17/6/20.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WorkHeaderView : UIView
@property (weak, nonatomic) IBOutlet UITextView *titleTextView;
@property (weak, nonatomic) IBOutlet UITextField *beginTimeF;
@property (weak, nonatomic) IBOutlet UITextField *endTiemF;
@property (weak, nonatomic) IBOutlet UIButton *beginBtn;
@property (weak, nonatomic) IBOutlet UIButton *endtimeBtn;

@end
