//
//  AddWorkPlanTableViewCell.h
//  OA
//
//  Created by yy on 17/6/20.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddWorkPlanTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *contentTextView;
@property (weak, nonatomic) IBOutlet UITextField *dayTextField;
@property (weak, nonatomic) IBOutlet UITextView *noteTextView;
@property (weak, nonatomic) IBOutlet UIButton *typeBtn;

@property (nonatomic,strong)NSDictionary *dic;
@end

@interface EndWorkPlanTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *typeTitleL;
@property (weak, nonatomic) IBOutlet UITextView *endWorkTextView;
@property (nonatomic,strong)NSDictionary *dic;
@end

