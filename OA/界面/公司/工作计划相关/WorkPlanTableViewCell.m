//
//  WorkPlanTableViewCell.m
//  OA
//
//  Created by yy on 17/6/19.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "WorkPlanTableViewCell.h"

@implementation WorkPlanTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
        
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setDic:(NSDictionary *)dic{
    _dic = dic;
    
    WorkPlanModel *model = [WorkPlanModel objectWithKeyValues:dic];
    _titleL.text = [NSString stringWithFormat:@"标题：%@",model.name];
    _imgL.text = model.recorder.length>2?[model.recorder substringWithRange:NSMakeRange(model.recorder.length-2, 2)]:model.recorder;
    _dateL.text = [model.record_date substringToIndex:16];
    _personL.text = model.recorder;
    _typeImg.image = [self imgWithType: model.status];
    
}

- (UIImage *)imgWithType:(NSString *)type{
    NSString *strImg = @"";
    switch ([type intValue]) {
        case 0:
            strImg = @"进行中图标";
            break;
        case 1:
            strImg = @"延期完成图标";
            break;
        case 2:
            strImg = @"按时完成图标";
            break;
        case 3:
            strImg = @"按时完成图标";
            break;
        default:
            break;
    }
    return Y_IMAGE(strImg);
}
@end
