//
//  ChooseFinishTypeViewController.m
//  OA
//
//  Created by yy on 17/7/13.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "ChooseFinishTypeViewController.h"

@interface ChooseFinishTypeViewController ()<RegionDelegate>

@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UITextField *delayDayTextField;
@property (weak, nonatomic) IBOutlet UITextView *noteTextView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintOfNoteBackView;
@property (nonatomic,assign)CGFloat constraintFloat;

@property (nonatomic,strong)BasisChooseRegionView *basisOfTypeCooseView;
@end

@implementation ChooseFinishTypeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initOfDataSouce];
    [self initializeUserInterface];
  
}
- (void)initOfDataSouce{
    _constraintFloat = _constraintOfNoteBackView.constant;
    _noteTextView.text = @"";
}

- (void)initializeUserInterface{
    [self changeConstraint];
}

- (void)begingConstraint{
    _constraintOfNoteBackView.constant = _constraintFloat;
}
- (void)changeConstraint{
    _constraintOfNoteBackView.constant = _constraintFloat - 50;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)chooseTypeBtnAction:(UIButton *)sender {
    [self.view addSubview:self.basisOfTypeCooseView];
}


- (BasisChooseRegionView *)basisOfTypeCooseView{
    if (!_basisOfTypeCooseView) {
        _basisOfTypeCooseView = [[[NSBundle mainBundle]loadNibNamed:@"BasisChooseRegionView" owner:self options:nil]objectAtIndex:0];
        _basisOfTypeCooseView.frame = CGRectMake(30, 30, Y_mainW-60, 280);
        _basisOfTypeCooseView.center = self.view.center;
        _basisOfTypeCooseView.layer.cornerRadius = 20;
        _basisOfTypeCooseView.layer.borderWidth = 0.2;
        _basisOfTypeCooseView.delegateOfRegion = self;
        _basisOfTypeCooseView.arrOfData = [NSArray arrayWithObjects:@"中止",@"按时完成",@"延期完成", nil];
    }
    return _basisOfTypeCooseView;
}
- (void)removeRegionView{
    [self.basisOfTypeCooseView removeFromSuperview];
}

- (NSString *)regionOfdidSelect:(NSString *)strOfArea{
    
    _typeLabel.text = strOfArea;
    if ([strOfArea isEqualToString:@"延期完成"]) {
        [self begingConstraint];
    }else{
        [self changeConstraint];
    }
    [self.basisOfTypeCooseView removeFromSuperview];
    return strOfArea;
}

- (IBAction)saveBtnAction:(UIBarButtonItem *)sender {
    NSMutableDictionary *parm = [NSMutableDictionary dictionary];
    [parm setObject:_idStr forKey:@"id"];
    [parm setObject:_workplanId forKey:@"workplanId"];
    if ([_typeLabel.text isEqualToString:@"延期完成"]) {
         [parm setObject:@"1" forKey:@"status"];
         [parm setObject:_delayDayTextField.text forKey:@"delayDays"];
        
    }
    if ([_typeLabel.text isEqualToString:@"按时完成"]) {
         [parm setObject:@"2" forKey:@"status"];
         [parm setObject:@"0" forKey:@"delayDays"];
    }
    if ([_typeLabel.text isEqualToString:@"中止"]) {
         [parm setObject:@"3" forKey:@"status"];
         [parm setObject:@"0" forKey:@"delayDays"];
    }
    [parm setObject:_noteTextView.text forKey:@"finishedNotes"];
    [[ToolOfNetWork sharedTools]YrequestURL:oa_updateStatusWorkPlan withParams:parm finished:^(id responsObject, NSError *error) {
        if (_Success) {
            Y_MBP_Save_Success
//            [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshOfWorkPlanList" object:@"refreshOfWorkPlanList"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshOfWorkPlanList" object:self userInfo:@{@"action":@"refreshOfWorkPlanList"}];
            [self.navigationController popViewControllerAnimated:YES];
  
        }else{
            Y_MBP_Save_Error
        }
    }];
    
    
}


@end
