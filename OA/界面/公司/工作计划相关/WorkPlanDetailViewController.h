//
//  WorkPlanDetailViewController.h
//  OA
//
//  Created by yy on 17/6/19.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^RefreshWorkPlanListVc) ();
@interface WorkPlanDetailViewController : UIViewController
@property(nonatomic,assign)BOOL isAddWorkPlanType;
@property (nonatomic,copy)NSString *idOfWorkPlan;

@property (nonatomic,copy)RefreshWorkPlanListVc ref;
- (void)refreshAction:(RefreshWorkPlanListVc)refresh;
@end
