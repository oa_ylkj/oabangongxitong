//
//  AddWorkPlanTableViewCell.m
//  OA
//
//  Created by yy on 17/6/20.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "AddWorkPlanTableViewCell.h"

@implementation AddWorkPlanTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setDic:(NSDictionary *)dic{
    _dic = dic;
    WorkPlanDetailMapModel *detailMapModel = [WorkPlanDetailMapModel objectWithKeyValues:dic];

    _contentTextView.text = detailMapModel.content;
    _dayTextField.text = detailMapModel.days;
    _noteTextView.text = detailMapModel.notes;
    
 
 
    switch ([ detailMapModel.status intValue]) {//状态整型//0是未完成  1:是延期完成 2是按时完成  3是 中止
       
        case 0:
            [_typeBtn setTitle:@"未完成" forState:UIControlStateNormal];
//            [_typeBtn addTarget:self action:@selector(notFinishBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            //未完成icon
            [_typeBtn setImage:Y_IMAGE(@"未完成icon") forState:UIControlStateNormal];
            [_typeBtn setTitleColor:BlueColor forState:UIControlStateNormal];
            
            break;
        case 1:
            [_typeBtn setImage:nil forState:UIControlStateNormal];
            [_typeBtn setTitle:@"延期完成" forState:UIControlStateNormal];
            [_typeBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
            break;
        case 2:
            [_typeBtn setImage:nil forState:UIControlStateNormal];
            [_typeBtn setTitle:@"按时完成" forState:UIControlStateNormal];
            [_typeBtn setTitleColor:BlueColor forState:UIControlStateNormal];
            break;
        case 3:
            [_typeBtn setImage:nil forState:UIControlStateNormal];
            [_typeBtn setTitle:@"中止" forState:UIControlStateNormal];
            [_typeBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            break;
            
        default:
            break;
    }

}

@end

@implementation EndWorkPlanTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (void)setDic:(NSDictionary *)dic{
    _dic = dic;
     WorkPlanDetailMapModel *detailMapModel = [WorkPlanDetailMapModel objectWithKeyValues:dic];
    _endWorkTextView.text = detailMapModel.finishedNotes;
    [self typeLabel:detailMapModel.status];
 
}
- (void)typeLabel:(NSString *)status{//0是未完成  1:是延期完成 2是按时完成  3是 中止
    switch ([status intValue]) {
        case 1:
            _typeTitleL.text = @"延期完成";
            _typeTitleL.textColor = [UIColor orangeColor];
            break;
        case 2:
            _typeTitleL.text = @"按时完成";
            _typeTitleL.textColor = BlueColor;
            break;
        case 3:
            _typeTitleL.text = @"中止";
            _typeTitleL.textColor = [UIColor redColor];
            break;
            
        default:
            break;
    }
}
@end



