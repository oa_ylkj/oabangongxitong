//
//  WorkPlanModel.h
//  OA
//
//  Created by yy on 17/7/20.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WorkPlanModel : NSObject
@property (nonatomic,copy)NSString *begin_date;
@property (nonatomic,copy)NSString *end_date;
@property (nonatomic,copy)NSString *filename;
@property (nonatomic,copy)NSString *id;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *participator;
@property (nonatomic,copy)NSArray *detailMap;
@property (nonatomic,copy)NSString *principal;
@property (nonatomic,copy)NSString *status;
@property (nonatomic,copy)NSString *record_date;
/**    {
 "begin_date" = "2017-06-15 00:00:00.0";
 detailMap =             {
 };
 "end_date" = "2017-06-30 00:00:00.0";
 filename = "";
 id = 00000002;
 name = "项目测试计划";
 participator = "系统管理员;犀利;西秀;";
 principal = "";
 status = 0;
 },*/


@property (nonatomic,copy)NSString *content;
@property (nonatomic,copy)NSString *opinion_leader;
@property (nonatomic,copy)NSString *opinion_leaderid;
@property (nonatomic,copy)NSString *participatorid;
@property (nonatomic,copy)NSString *principalid;
@property (nonatomic,copy)NSString *recorder;
@property (nonatomic,copy)NSString *serverFilename;
@property (nonatomic,copy)NSString *type;
@property (nonatomic,copy)NSString *viewdepart;
@property (nonatomic,copy)NSString *viewdepartid;
@property (nonatomic,copy)NSString *viewrole;
@property (nonatomic,copy)NSString *viewroleid;
@property (nonatomic,copy)NSString *viewstaff;
@property (nonatomic,copy)NSString *viewstaffid;
@property (nonatomic,copy)NSString *warn1;
@property (nonatomic,copy)NSString *warn2;
@property (nonatomic,copy)NSArray *detailList;

/**code = 200;
 data =     {
 "begin_date" = "2017-07-04 00:00:00.0";
 content = "测试";
 detailList =         (
 );
 detailMap =         {
 };
 "end_date" = "2017-07-30 00:00:00.0";
 filename = "";
 id = 00000015;
 name = "oa计划";
 "opinion_leader" = "凤飞飞;";
 "opinion_leaderid" = "fff;";
 participator = "犀利;";
 participatorid = "xili;";
 principal = "西秀;";
 principalid = "xx;";
 recorder = admin;
 serverFilename = "";
 status = 0;
 type = 00000227;
 viewdepart = "";
 viewdepartid = "";
 viewrole = "";
 viewroleid = "";
 viewstaff = "系统管理员;";
 viewstaffid = "admin;";
 warn1 = 1;
 warn2 = 1;
 };*/
@end
