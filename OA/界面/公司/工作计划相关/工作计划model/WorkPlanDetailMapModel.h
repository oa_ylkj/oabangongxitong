//
//  WorkPlanDetailMapModel.h
//  OA
//
//  Created by yy on 17/7/21.
//  Copyright © 2017年 yy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WorkPlanDetailMapModel : NSObject
@property  (nonatomic,copy)NSString *content;
@property  (nonatomic,copy)NSString *createtime;
@property  (nonatomic,copy)NSString *creator;
@property  (nonatomic,copy)NSString *days;
@property  (nonatomic,copy)NSString *finishedNotes;
@property  (nonatomic,copy)NSString *id;
@property  (nonatomic,copy)NSString *myUnitId;
@property  (nonatomic,copy)NSString *notes;
@property  (nonatomic,copy)NSString *status;//0是未完成  1:是延期完成 2是按时完成  3是 中止
@property  (nonatomic,copy)NSString *updator;
@property  (nonatomic,copy)NSString *workplanId;

/**detailList =         (
 {
 content = "项目1";
 createtime = "2017-07-20 19:26:17.0";
 creator = admin;
 days = 1;
 finishedNotes = "";
 id = 00000001;
 myUnitId = 00000001;
 notes = "备注1";
 status = 0;
 updator = "";
 workplanId = 00000001;
 },
 {
 content = "项目2";
 createtime = "2017-07-20 19:26:18.0";
 creator = admin;
 days = 2;
 finishedNotes = "";
 id = 00000002;
 myUnitId = 00000001;
 notes = "备注2";
 status = 0;
 updator = "";
 workplanId = 00000001;
 },
 {
 content = "项目3";
 createtime = "2017-07-20 19:26:19.0";
 creator = admin;
 days = 3;
 finishedNotes = "";
 id = 00000003;
 myUnitId = 00000001;
 notes = "备注3";
 status = 0;
 updator = "";
 workplanId = 00000001;
 }
 );
 detailMap =         {
 00000001 =             {
 content = "项目1";
 createtime = "2017-07-20 19:26:17.0";
 creator = admin;
 days = 1;
 finishedNotes = "";
 id = 00000001;
 myUnitId = 00000001;
 notes = "备注1";
 status = 0;
 updator = "";
 workplanId = 00000001;
 };
 00000002 =             {
 content = "项目2";
 createtime = "2017-07-20 19:26:18.0";
 creator = admin;
 days = 2;
 finishedNotes = "";
 id = 00000002;
 myUnitId = 00000001;
 notes = "备注2";
 status = 0;
 updator = "";
 workplanId = 00000001;
 };
 00000003 =             {
 content = "项目3";
 createtime = "2017-07-20 19:26:19.0";
 creator = admin;
 days = 3;
 finishedNotes = "";
 id = 00000003;
 myUnitId = 00000001;
 notes = "备注3";
 status = 0;
 updator = "";
 workplanId = 00000001;
 };
 };
*/
@end
