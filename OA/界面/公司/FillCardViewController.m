//
//  FillCardViewController.m
//  OA
//
//  Created by Admin on 2017/7/18.
//  Copyright © 2017年 yy. All rights reserved.
//

#import "FillCardViewController.h"
#import "csCell.h"
#import "csAddCell.h"
@interface FillCardViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UITextView *reason;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *bukaTime;
@property (nonatomic ,strong) NSMutableArray *csArr;
@end

@implementation FillCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.csArr = @[].mutableCopy;
    self.date.text = self.dateStr;
    
    if ([[self.dateStr substringFromIndex:5] compare:@"05-01"] > 0 && [[self.dateStr substringFromIndex:5] compare:@"10-01"] < 0) {
        self.bukaTime.text = [NSString stringWithFormat:@"夏季作息时间 %@",self.bkTimeStr];
    } else {
        self.bukaTime.text = [NSString stringWithFormat:@"秋季作息时间 %@",self.bkTimeStr];
    }
    self.title = @"补卡申请";
}
- (IBAction)sen:(id)sender {
    if (self.reason.text.length == 0) {
        [MBProgressHUD showError:@"请输入补卡原因"];
        return;
    }
    if (self.csArr.count == 0) {
        [MBProgressHUD showError:@"请选择审核人"];
        return;
    }
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //设置格式：
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    //NSDate转NSString
    NSString *now = [dateFormatter stringFromDate:date];
    NSMutableDictionary *dic = @{}.mutableCopy;
    [dic setObject:self.dateStr forKey:@"datenumber"];
    [dic setObject:self.reason.text forKey:@"reason"];
    [dic setObject:self.type forKey:@"reissuecardType"];
    [dic setObject:now forKey:@"reissuecardTime"];
    [dic setObject:[NSString stringWithFormat:@"%@;",self.csArr[0][@"id"]] forKey:@"auditors"];
    [dic setObject:[NSString stringWithFormat:@"%@;",self.csArr[0][@"name"]] forKey:@"auditorsName"];
    Y_Wait
    [[ToolOfNetWork sharedTools] YrequestURL:oa_insertAttendanceAuditByReissuecard withParams:dic.mutableCopy finished:^(id responsObject, NSError *error) {
        Y_Hide
        if (_Success) {
            [MBProgressHUD showSuccess:@"申请成功"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        } else {
            [MBProgressHUD showError:responsObject[@"message"]];
        }
        NSLog(@"%@",[[responsObject description]kdtk_stringByReplaceingUnicode]);
        NSLog(@"%@",error.localizedDescription);
    }];

}

#pragma mark --- UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.csArr.count + 1;
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < self.csArr.count) {
        csCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"csCell" forIndexPath:indexPath];
        cell.image.backgroundColor = [UIColor getColorWithHexString:@"FEA100"];
        cell.image.layer.cornerRadius = 20;
        cell.image.clipsToBounds = YES;
        
        NSString *nameStr = self.csArr[indexPath.row][@"name"];
        
        if (nameStr.length <3) {
            cell.name.text = nameStr;
        } else {
            cell.name.text = [nameStr substringWithRange:NSMakeRange(nameStr.length - 2, 2)];
        }
        return cell;
    } else {
        csAddCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"csAddCell" forIndexPath:indexPath];
        return cell;
    }
    
    
}

#pragma mark --- UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < self.csArr.count) {
        [self.csArr removeObjectAtIndex:indexPath.row];
        [self.collectionView reloadData];
    } else {
        BumenChoiceViewController *vc = VCInSB(@"BumenChoiceViewController", @"BumenViewController");
        vc.type = @"1";
        vc.vc = self;
        vc.csshArr = self.csArr;
        vc.back = ^() {
            if ([ShareUserInfo sharedUserInfo].csArr.count > 0) {
                [self.csArr removeAllObjects];
                [self.csArr addObject:[ShareUserInfo sharedUserInfo].csArr[0]];
                [self.collectionView reloadData];
            }
        };
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    
    
    
}

@end
